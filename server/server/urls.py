"""server URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.static import serve

from edec_account.views import index, error
from .settings import MEDIA_ROOT
from .views import base_html_view


urlpatterns = [
    # already configured urls
    url(r'^admin/', admin.site.urls),
    url(r'^avatar/', include('avatar.urls')),
    url(r'^media/(?P<path>.*)$', serve, {'document_root': MEDIA_ROOT}),

    # custom urls
    url(r'^$', index, name='index'),
    url(r'^index$', index, name='index'),
    url(r'^index.php$', index, name='index'),
    url(r'^index.html$', index, name='index'),
    url(r'^error$', error, name='error'),
    url(r'^error/(?P<code>\d{3})/$', error, name='error'),
    url(r'^api/', include('api.urls', namespace='api')),
    url(r'^accounts/', include('edec_account.urls')),
    url(r'^products/', include('product.urls', namespace='product')),
    url(r'^companies/', include('company.urls', namespace='company')),

    # test urls
    url(r'^test$', base_html_view, name='test')
]

from django.db import models
from edec_account.models import UserProfile
from product.models import Product
from company.models import Company


class UserCompany(models.Model):
    class Meta:
        unique_together = ('user', 'company')

    class Manager(models.Manager):
        def suggested_companies(self, user_id):
            return [(rel.company, rel.get_score() * 2 - 1) for rel in self.filter(user_id=user_id)
                    if rel.get_score() > 0.5]

        def unsuggested_companies(self, user_id):
            return [(rel.company, 1 - rel.get_score() * 2)for rel in self.filter(user_id=user_id)
                    if rel.get_score() < 0.5]

    user = models.ForeignKey(UserProfile)
    company = models.ForeignKey(Company)
    positive_score = models.IntegerField()
    negative_score = models.IntegerField()

    objects = Manager()

    def get_negative_score(self):
        return float(self.negative_score)/10000

    def get_positive_score(self):
        return float(self.positive_score)/10000

    def get_score(self):
        return float(self.positive_score - self.negative_score + 10000)/20000


class UserProduct(models.Model):
    class Meta:
        unique_together = ('user', 'product')

    class Manager(models.Manager):
        def suggested_products(self, user_id):
            return [(rel.product, rel.get_score() * 2) for rel in self.filter(user_id=user_id)
                    if rel.get_score() > 0.5]

        def unsuggested_products(self, user_id):
            return [(rel.product, 1 - rel.get_score() * 2)for rel in self.filter(user_id=user_id)
                    if rel.get_score() < 0.5]

    user = models.ForeignKey(UserProfile)
    product = models.ForeignKey(Product)
    positive_score = models.IntegerField()
    negative_score = models.IntegerField()

    objects = Manager()

    def get_negative_score(self):
        return float(self.negative_score)/10000

    def get_positive_score(self):
        return float(self.positive_score)/10000

    def get_score(self):
        return float(self.positive_score - self.negative_score + 10000)/20000


class UserUser(models.Model):

    class Meta:
        unique_together = ('user1', 'user2')

    class Manager(models.Manager):
        def similar_users(self, user_id):
            result = []
            for rel in self.filter(user2_id=user_id):
                result.append((rel.user1, rel.get_score()))
            for rel in self.filter(user1_id=user_id):
                result.append((rel.user2, rel.get_score()))
            return result

    user1 = models.ForeignKey(UserProfile, related_name='metrics_first')
    user2 = models.ForeignKey(UserProfile, related_name='metrics_second')
    score = models.IntegerField()
    approval_score = models.IntegerField()
    preference_score = models.IntegerField()

    objects = Manager()

    def get_approval_score(self):
        return float(10000 + self.approval_score)/20000

    def get_preference_score(self):
        return float(10000 + self.preference_score)/20000

    def get_score(self):
        return float(self.score + 10000 + self.approval_score + 10000 + self.preference_score)/50000


class CompanyCompany(models.Model):
    class Meta:
        unique_together = ('company1', 'company2')

    class Manager(models.Manager):
        def similar_companies(self, company_id):
            result = []
            for rel in self.filter(company1_id=company_id):
                result.append((rel.company2, rel.get_score()))
            for rel in self.filter(company2_id=company_id):
                result.append((rel.company1, rel.get_score()))
            return result

    company1 = models.ForeignKey(Company, related_name='metrics_first')
    company2 = models.ForeignKey(Company, related_name='metrics_second')
    score = models.IntegerField()

    objects = Manager()

    def get_score(self):
        return float(self.score)/10000


class ProductProduct(models.Model):
    class Meta:
        unique_together = ('product1', 'product2')

    class Manager(models.Manager):
        def similar_products(self, product_id):
            result = []
            for rel in self.filter(product1_id=product_id):
                result.append((rel.product2, rel.get_score()))
            for rel in self.filter(product2_id=product_id):
                result.append((rel.product1, rel.get_score()))
            return result

    product1 = models.ForeignKey(Product, related_name='metrics_first')
    product2 = models.ForeignKey(Product, related_name='metrics_second')
    score = models.IntegerField()

    objects = Manager()

    def get_score(self):
        return float(self.score)/10000

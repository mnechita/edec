from django.core.exceptions import *
from django.db.models import Q
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponseNotAllowed, HttpResponseRedirect
from server.settings import BASE_DIR
from product.models import ProductTrait, Preference
from .models import *
from .forms import *

import re
import json
import operator
from functools import reduce
from PIL import Image
from urllib.parse import urlencode


def error(request, code):
    integer_code = int(code)
    messages = {
        400: "Bad request",
        404: "Page not found",
        405: "Request not allowed",
    }
    extended_messages = {
        400: "Request type or parameters are not allowed !",
        404: "The page you are looking for does not exist !",
        405: "The request you made is not allowed !",
    }
    context = {
        'code': integer_code,
        'message': messages.get(integer_code, 'Unknown error'),
        'extended_message': extended_messages.get(integer_code, 'An unknown error occurred. Please contact a site administrator !')
    }
    return render(request, 'edec_account/error.html', context)


@login_required
def search(request):
    # treat a post request on the search page
    if request.method == 'POST':
        return HttpResponseRedirect('/search')

    # treat a get request on the search page
    objects = []
    last_page = 1
    if request.method == 'GET':
        page_nr = request.GET.get('page', 1)
        try:
            page_nr = int(page_nr)
            if page_nr < 0:
                raise Exception('Page number is negative')
        except Exception as exc:
            print('Exception: {}'.format(exc))
            return HttpResponseRedirect('/error/400/')
        first_slice = (page_nr-1) * 10
        second_slice = page_nr * 10
        flag = False
        if len(request.GET) > 1:
            submit = request.GET.get('submit')
            search_form = SearchProduct(request.GET, form_count=submit)
            if (request.GET['token_to'] is None and request.GET['token_from'])\
                or (request.GET['token_from'] is None and request.GET['token_to']):
                print(1)
                return HttpResponseRedirect('/error/400/')
            if search_form.is_valid():
                # rename cleaned data
                query = search_form.cleaned_data
                category = query['category']

                # build word searching queries
                queries = query['query'].split()
                if queries:
                    query_set = reduce(operator.__or__, [Q(name__icontains=query) for query in queries])
                else:
                    query_set = Q(name__icontains='')

                if query['order'] == 'Relevance Descending':
                    objects = Product.objects.get_preference_ordered(request.user.profile.id, first_slice - 1, second_slice,
                                                                     query['query'], query['token_from'],
                                                                     query['token_to'], 'desc')
                elif query['order'] == 'Relevance Ascending':
                    objects = Product.objects.get_preference_ordered(request.user.profile.id, first_slice - 1, second_slice,
                                                                     query['query'], query['token_from'],
                                                                     query['token_to'], 'asc')
                else:
                    # parse order by and form string for order
                    order_by = 'name'
                    if query['order'] == 'Alphabetic Ascending':
                        order_by = 'name'
                    elif query['order'] == 'Alphabetic Descending':
                        order_by = '-name'
                    elif query['order'] == 'Relevance Descending':
                        order_by = 'score'
                    elif query['order'] == 'Relevance Ascending':
                        order_by = '-score'
                    elif query['order'] == 'Price Ascending' or query['order'] == 'Calories Ascending':
                        order_by = 'token'
                    elif query['order'] == 'Price Descending' or query['order'] == 'Calories Descending':
                        order_by = '-token'

                    # get request data according to searched values
                    with_tokens = False
                    if query['token_from'] is not None and query['token_to'] is not None:
                        with_tokens = True
                        if query['token_from'] > query['token_to']:
                            print('400: Query from > query to')
                            return HttpResponseRedirect('/error/400/')
                    if query['subcategory'] == 'All Subcategories':
                        if with_tokens:
                            last_page = Product.objects.filter(
                                query_set,
                                category__pk=query['category'],
                                token__range=(query['token_from'], query['token_to'])).count()
                            objects = Product.objects.filter(
                                query_set,
                                category__pk=query['category'],
                                token__range=(query['token_from'], query['token_to'])
                            ).order_by(order_by)[first_slice:second_slice]
                        else:
                            last_page = Product.objects.filter(
                                query_set,
                                category__pk=query['category']).count()
                            objects = Product.objects.filter(
                                query_set,
                                category__pk=query['category']).order_by(order_by)[first_slice:second_slice]
                    else:
                        if with_tokens:
                            last_page = Product.objects.filter(
                                query_set,
                                category__pk=query['category'],
                                subcategory__name=query['subcategory'],
                                token__range=(query['token_from'], query['token_to'])).count()
                            objects = Product.objects.filter(
                                query_set,
                                category__pk=query['category'],
                                subcategory__name=query['subcategory'],
                                token__range=(query['token_from'], query['token_to'])
                            ).order_by(order_by)[first_slice:second_slice]
                        else:
                            last_page = Product.objects.filter(
                                query_set,
                                category__pk=query['category'],
                                subcategory__name=query['subcategory']).count()
                            objects = Product.objects.filter(
                                query_set,
                                category__pk=query['category'],
                                subcategory__name=query['subcategory']).order_by(order_by)[first_slice:second_slice]
            else:
                print('400:Search form not valid')
                return HttpResponseRedirect('/error/400/')
        else:
            flag = True
            page_nr = request.GET.get('page', 1)
            try:
                page_nr = int(page_nr)
                if page_nr < 0:
                    raise Exception('Page number is negative')
            except Exception as exc:
                print('Exception: {}'.format(exc))
                return HttpResponseRedirect('/error/400/')
            first_slice = (page_nr-1) * 10
            second_slice = page_nr * 10
            order_by = 'name'
            category = 1
            last_page = Product.objects.filter(
                category__pk=category).count()
            objects = Product.objects.filter(
                category__pk=category).order_by(order_by)[first_slice:second_slice]
            copy_params = {
                'category': 1,
                'query': '',
                'submit': 1,
                'token_from': '',
                'token_to': '',
                'subcategory': 'All Subcategories',
                'order': 'Alphabetic Ascending'
            }

        # get subcategories for all categories
        subcategories_1 = [{'name': item.name.replace("''", "'")} for item in Subcategory.objects.filter(category__pk=1)]
        subcategories_2 = [{'name': item.name.replace("''", "'")} for item in Subcategory.objects.filter(category__pk=2)]
        subcategories_3 = [{'name': item.name.replace("''", "'")} for item in Subcategory.objects.filter(category__pk=3)]
        subcategories_4 = [{'name': item.name.replace("''", "'")} for item in Subcategory.objects.filter(category__pk=4)]

        # append subcategories in order to pass to template
        subcategories = {
            'food': subcategories_1,
            'clothing': subcategories_2,
            'electronics': subcategories_3,
            'home': subcategories_4
        }

        # append results in order to pass to template
        products = []
        for prod in objects:
            if prod.image != 'nophoto':
                image = Image.open(BASE_DIR + '\\static\\img\\product\\'
                                   + '{:02d}'.format(prod.id)[-2:] + '\\{}.'.format(prod.id) + prod.image)
            else:
                image = Image.open(BASE_DIR + '\\static\\images\\slider\\no_photo.png')
            image_ratio = image.height / image.width
            if image_ratio > 170/140:
                style = 'height: 100%;'
            else:
                style = 'width: 100%;'
            if category != 1:
                description = json.loads(prod.description)['description'].replace("''", "'")
                replaceable = re.search(r'</?div[^>]+>', description)
                while replaceable:
                    description = description.replace(replaceable.group(0), '')
                    replaceable = re.search(r'</?div[^>]+>', description)
            else:
                info = json.loads(prod.description)
                description = '<b>Information for {}</b><br />'.format(info['info_for'])
                description += '<table class="table table-striped"><col width="200"><col width="200"><col width="200">'
                description += '<thead class="tables" style="font-size: 15px; color: #666;"><tr><th>Nutrients:' \
                               '</th><th>Quantity:</th><th>Daily recommended:</th></tr></thead><tbody>'
                for key in info:
                    if key != 'info_for':
                        first = key.replace('_', ' ').title()
                        absolute = str(info[key][0]) + ' g'
                        relative = info[key][1]
                        if relative is None:
                            relative = '-'
                        else:
                            relative = str(info[key][1]) + ' %'
                        description += '<tr><td>{}</td><td>{}</td><td>{}</td></tr>'.format(first, absolute, relative)
                description += '</tbody></table'
            if prod.image != 'nophoto':
                image_link = 'global/img/product/' + '{:02d}'.format(prod.id)[-2:] + '/{}.'.format(prod.id) + prod.image
            else:
                image_link = 'global/images/slider/no_photo.png'
            if category != 1:
                token = 'Price: {} $'.format(prod.token)
            else:
                token = 'Calories: {}'.format(prod.token)
            products.append({
                'name': prod.name.replace("''", "'"),
                'style': style,
                'token': token,
                'description' : description,
                'image_link': image_link,
                'prod_id': prod.id
            })
        search_form_1 = SearchProduct(initial={'category': 1}, form_count=1)
        search_form_2 = SearchProduct(initial={'category': 2}, form_count=2)
        search_form_3 = SearchProduct(initial={'category': 3}, form_count=3)
        search_form_4 = SearchProduct(initial={'category': 4}, form_count=4)
        if page_nr <= 2:
            page_token_prev = 1
            page_token_1 = 1
            page_token_2 = 2
            page_token_3 = 3
            page_token_4 = 4
            page_token_5 = 5
            page_token_next = page_nr + 1
        else:
            page_token_prev = page_nr - 1
            page_token_1 = page_nr - 2
            page_token_2 = page_nr - 1
            page_token_3 = page_nr
            page_token_4 = page_nr + 1
            page_token_5 = page_nr + 2
            page_token_next = page_nr + 1
        if not flag:
            copy_params = {}
            for key in request.GET:
                if key != 'page':
                    copy_params[key] = request.GET[key]
        if last_page % 10 != 0:
            last_page = int(last_page / 10) + 1
        else:
            last_page = int(last_page / 10)
        if page_token_next > last_page:
            page_token_next = last_page
        context = {
            'subcategories': subcategories,
            'current_category': category,
            'products': products,
            'current_page': page_nr,
            'last_page': last_page,
            'page_token_1': page_token_1,
            'page_token_2': page_token_2,
            'page_token_3': page_token_3,
            'page_token_4': page_token_4,
            'page_token_5': page_token_5,
            'page_token_link_first': '/products/search?' + urlencode(copy_params) + '&page=1',
            'page_token_link_prev': '/products/search?' + urlencode(copy_params) + '&page={}'.format(page_token_prev),
            'page_token_link_1': '/products/search?' + urlencode(copy_params) + '&page={}'.format(page_token_1),
            'page_token_link_2': '/products/search?' + urlencode(copy_params) + '&page={}'.format(page_token_2),
            'page_token_link_3': '/products/search?' + urlencode(copy_params) + '&page={}'.format(page_token_3),
            'page_token_link_4': '/products/search?' + urlencode(copy_params) + '&page={}'.format(page_token_4),
            'page_token_link_5': '/products/search?' + urlencode(copy_params) + '&page={}'.format(page_token_5),
            'page_token_link_next': '/products/search?' + urlencode(copy_params) + '&page={}'.format(page_token_next),
            'page_token_link_last': '/products/search?' + urlencode(copy_params) + '&page={}'.format(last_page),
            'search_form_1': search_form_1,
            'search_form_2': search_form_2,
            'search_form_3': search_form_3,
            'search_form_4': search_form_4,
        }
        return render(request, 'product/search.html', context)

    # treat other types of requests on the search page
    return HttpResponseRedirect('/error/405/')


@login_required
def product_details(request, prod_id):
    try:
        prod = Product.objects.get(pk=int(prod_id))
    except ObjectDoesNotExist:
        return HttpResponseRedirect('/error/404/')

    if prod.image != 'nophoto':
        image = Image.open(BASE_DIR + '\\static\\img\\product\\'
                           + '{:02d}'.format(prod.id)[-2:] + '\\{}.'.format(prod.id) + prod.image)
    else:
        image = Image.open(BASE_DIR + '\\static\\images\\slider\\no_photo.png')
    image_ratio = image.height / image.width
    if image_ratio > 684/330:
        style = 'height: 100%;'
    else:
        style = 'width: 100%;'

    if prod.image != 'nophoto':
        image_link = 'global/img/product/' + '{:02d}'.format(prod.id)[-2:] + '/{}.'.format(prod.id) + prod.image
    else:
        image_link = 'global/images/slider/no_photo.png'

    if prod.category.id != 1:
        description = json.loads(prod.description)['description'].replace("''", "'")
        replaceable = re.search(r'</?div[^>]+>', description)
        while replaceable:
            description = description.replace(replaceable.group(0), '')
            replaceable = re.search(r'</?div[^>]+>', description)
    else:
        info = json.loads(prod.description)
        description = '<b>Information for {}</b><br />'.format(info['info_for'])
        description += '<table class="table table-striped"><col width="100"><col width="100"><col width="200">'
        description += '<thead class="tables" style="font-size: 15px; color: #666;"><tr><th>Nutrients:' \
                       '</th><th>Quantity:</th><th>Daily recommended:</th></tr></thead><tbody>'
        for key in info:
            if key != 'info_for':
                first = key.replace('_', ' ').title()
                absolute = str(info[key][0]) + ' g'
                relative = info[key][1]
                if relative is None:
                    relative = '-'
                else:
                    relative = str(info[key][1]) + ' %'
                description += '<tr><td>{}</td><td>{}</td><td>{}</td></tr>'.format(first, absolute, relative)
        description += '</tbody></table'

    all_traits = Trait.objects.exclude(producttrait__product=prod)
    product_traits = ProductTrait.objects.filter(product_id=prod.id)
    user_positive_traits = [pref.trait_id for pref in Preference.objects.filter(user_id=request.user.profile.id, preference=True)]
    user_negative_traits = [pref.trait_id for pref in Preference.objects.filter(user_id=request.user.profile.id, preference=False)]

    context = {
        'positive_traits': [pt.trait.name for pt in product_traits if pt.trait.id in user_positive_traits],
        'negative_traits': [pt.trait.name for pt in product_traits if pt.trait.id in user_negative_traits],
        'neutral_traits': [pt.trait.name for pt in product_traits if pt.trait.id not in user_positive_traits and pt.trait.id not in user_negative_traits],
        'missing_traits': [trait.name for trait in all_traits],
        'prod_name': prod.name,
        'prod_img': image_link,
        'prod_desc': description.replace('Good to Know', '').replace('\\n', '\n'),
        'style': style,
        'comp_name': prod.company.name,
        'comp_description': prod.company.description.replace('\\n', '\n'),
    }
    return render(request, 'product/product_details.html', context)

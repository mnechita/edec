from django import forms


class SearchProduct(forms.Form):
    category = forms.IntegerField(min_value=1, max_value=4)
    query = forms.CharField(max_length=64, required=False)
    token_from = forms.IntegerField(min_value=0, max_value=100000, required=False)
    token_to = forms.IntegerField(min_value=0, max_value=100000, required=False)
    subcategory = forms.CharField(max_length=128, initial='All Subcategories')
    order = forms.CharField(max_length=64, initial='Alphabetic Ascending')

    def __init__(self, *args, **kwargs):
        form_count = kwargs.pop('form_count')
        super(SearchProduct, self).__init__(*args, **kwargs)
        # category
        self.fields['category'].widget = forms.HiddenInput()
        self.fields['category'].widget.attrs.update({
            'id': 'hiddenCategory{}'.format(form_count)
        })
        # query
        self.fields['query'].widget.attrs.update({
            'class': 'form-control'
        })
        # token_from
        self.fields['token_from'].widget.attrs.update({
            'style': "width:100px;",
            'id': "fromvalue{}".format(form_count)
        })
        # token_to
        self.fields['token_to'].widget.attrs.update({
            'style': "width:100px;",
            'id': "tovalue{}".format(form_count)
        })
        # subcategory
        self.fields['subcategory'].widget = forms.HiddenInput()
        self.fields['subcategory'].widget.attrs.update({
            'id': 'hiddenSubcategory{}'.format(form_count)
        })
        # order
        self.fields['order'].widget = forms.HiddenInput()
        self.fields['order'].widget.attrs.update({
            'id': 'hiddenFilter{}'.format(form_count)
        })
from django.conf.urls import url
from django.shortcuts import redirect
from . import views


urlpatterns = [
    url(r'error/(?P<code>\d{3})/$', views.error, name='error'),
    url(r'^search$', views.search, name='search'),
    url(r'^search/$', views.search, name='search'),
    url(r'^details/(?P<prod_id>\d+)/$', views.product_details, name='details'),
]

from django.db import models, connection
from django.db.models import F
from edec_account.models import UserProfile
from company.models import Company
from django.core.urlresolvers import reverse
from os.path import join, isfile
from server.settings import BASE_DIR, MEDIA_URL
from django.contrib.sites.models import Site


class Category(models.Model):
    name = models.CharField(max_length=64, unique=True, verbose_name='Name of category')


class Subcategory(models.Model):
    name = models.CharField(max_length=64, unique=True, verbose_name='Name of subcategory')
    category = models.ForeignKey(Category, verbose_name='Category of subcategory')


class Product(models.Model):

    class Manager(models.Manager):
        def get_preference_ordered(self, user_id, page1, page2, query_string=None, low_value=None, high_value=None, order='desc'):
            where_clause = ''
            if query_string:
                tokens = query_string.lower().split(' ')
                where_clause = 'and (lower(name) like \'%%{}%%\' '.format(tokens[0])
                for token in tokens[1:]:
                    where_clause += 'or lower(name) like \'%%{}%%\' '.format(token)
                where_clause += ') '
            if low_value:
                where_clause += 'and token >= {} '.format(low_value)
            if high_value:
                where_clause += 'and token <= {} '.format(high_value)

            query = \
                'select id from '\
                '(select id, rownum rn from '\
                '(select pp.id id '\
                'from '\
                '(select id '\
                'from product_product ' \
                'where 1 = 1 {}) pp '\
                'left outer join '\
                '(select product_id, positive_score, negative_score '\
                'from metrics_userproduct '\
                'where user_id = %s) up '\
                'on pp.id = up.product_id '\
                'order by NVL(up.positive_score - up.negative_score, 0) {}) '\
                'where rownum <= %s) '\
                'where rn >= %s'.format(where_clause, order)
            return [self.get(id=res.id) for res in self.raw(query, [user_id, page2, page1])]


    name = models.CharField(max_length=128, verbose_name='Product\'s name')
    description = models.TextField(verbose_name='Product\'s description')
    company = models.ForeignKey(Company, verbose_name='Product\'s company')
    category = models.ForeignKey(Category, verbose_name='Product\'s category')
    subcategory = models.ForeignKey(Subcategory, verbose_name='Product\'s subcategory')
    token = models.IntegerField(null=True, blank=True, verbose_name='A value used for nefarious purposes')
    image = models.CharField(max_length=256, null=True, blank=True, verbose_name='Product\'s image extension')

    objects = Manager()

    def get_image_path(self):
        """ Returns '' if none exists """
        if not self.image:
            return ''
        img_path = join(BASE_DIR, 'static', 'img', 'product',
                        '%02d' % (self.id % 100), str(self.id) + '.' + self.image)
        print(img_path)
        if not isfile(img_path):
            return ''
        return img_path

    def get_image_url(self):
        """ Returns '' if none exists """
        if not self.image:
            return ''
        # domain = Site.objects.get_current().domain
        # TODO: momentan e hardcodat
        return r'http://localhost:8000/static/global/img/product/%02d/' \
               r'' % (self.id % 100) + str(self.id) + '.' + self.image

    def get_absolute_url(self):
        return reverse('product.views.product_view', args=[self.id])


class Trait(models.Model):
    name = models.CharField(max_length=64, verbose_name='Trait\'s name')
    description = models.TextField(verbose_name='Trait\'s description')
    category = models.ForeignKey(Category, verbose_name='Trait\'s category')

    class Meta:
        unique_together = ('name', 'category')


class ProductTrait(models.Model):
    product = models.ForeignKey(Product, verbose_name='The product')
    trait = models.ForeignKey(Trait, verbose_name='The trait')

    class Meta:
        unique_together = ('product', 'trait')


class Preference(models.Model):
    PREFERENCE_CHOICE = (
        (None, 'Choose preference'),
        (False, 'I do not prefer'),
        (True, 'I prefer')
    )

    user = models.ForeignKey(UserProfile, verbose_name='The user')
    trait = models.ForeignKey(Trait, verbose_name='The trait')
    preference = models.BooleanField(choices=PREFERENCE_CHOICE, verbose_name='User\'s preference')

    class Meta:
        unique_together = ('user', 'trait')


class ProductReview(models.Model):
    title = models.CharField(max_length=64, verbose_name='The title of the review')
    content = models.TextField(verbose_name='The content of the review')
    creation_time = models.DateTimeField(auto_now_add=True, verbose_name='The moment the product was reviewed')
    modified_time = models.DateTimeField(auto_now=True, verbose_name='The moment the review was last updated')
    user = models.ForeignKey(UserProfile, verbose_name='The user')
    product = models.ForeignKey(Product, verbose_name='The product')


class ProductComment(models.Model):
    content = models.TextField(verbose_name='The content of the comment')
    creation_time = models.DateTimeField(auto_now_add=True, verbose_name='The moment the review was commented')
    modified_time = models.DateTimeField(auto_now=True, verbose_name='The moment the comment was last updated')
    user = models.ForeignKey(UserProfile, verbose_name='The user')
    product_review = models.ForeignKey(ProductReview, verbose_name='The review that is being commented')
from django.apps import AppConfig


class EdecAccountConfig(AppConfig):
    name = 'edec_account'

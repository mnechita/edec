from django.contrib import admin
from .models import City, Country, UserProfile

admin.site.register(City)
admin.site.register(UserProfile)
admin.site.register(Country)

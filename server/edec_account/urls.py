from django.conf.urls import url, include
from django.contrib.auth.views import logout as auth_logout
from . import views

urlpatterns = [
    url(r'^login', views.login),
    url(r'^country-ajax', views.testajax),
    url(r'^profile', views.profile, name="account_profile"),
    url(r'^logout/$', auth_logout, {'next_page': '/'}),
    url(r'^', include('allauth.urls')),
]

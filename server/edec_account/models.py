import os.path
import urllib.request
from datetime import date

from avatar.models import Avatar
from django.contrib.auth.models import User
from django.core.files import File
from django.core.validators import MinValueValidator, MaxValueValidator, RegexValidator
from django.db import models
from django.dispatch import receiver

from allauth.account.signals import user_signed_up


class Country(models.Model):
    code = models.CharField(max_length=2, unique=True, validators=[RegexValidator(r'^[a-zA-Z]{2}$')],
                            verbose_name='Country\'s 2 digit identification code')
    name = models.CharField(max_length=64, validators=[RegexValidator(r'^[a-zA-Z \']&')],
                            verbose_name='Country\'s full name')

    def __str__(self):
        return self.name


class City(models.Model):
    name = models.CharField(max_length=128, validators=[RegexValidator(r'^[a-zA-Z -]+$')],
                            verbose_name='City\'s full name')
    locale_name = models.CharField(null=True, blank=True, max_length=128, verbose_name='City\s full name with dialect')
    population = models.IntegerField(null=True, blank=True, verbose_name='City\'s population')
    country = models.ForeignKey(Country, verbose_name='City\'s Country')
    latitude = models.DecimalField(max_digits=10, decimal_places=7,
                                   validators=[MinValueValidator(-90), MaxValueValidator(90)],
                                   verbose_name='City\'s latitude')
    longitude = models.DecimalField(max_digits=10, decimal_places=7,
                                    validators=[MinValueValidator(-180), MaxValueValidator(180)],
                                    verbose_name='City\'s longitude')

    def __str__(self):
        return self.name


class UserProfile(models.Model):
    GENDER_CHOICE = (
        (False, 'Female'),
        (True, 'Male'),
        (None, 'Choose gender')
    )

    auth = models.OneToOneField(User, related_name='profile', verbose_name='Django user interface')
    birth_date = models.DateField(validators=[MinValueValidator(date(1900, 1, 1),
                                                                message='Date of birth does not exceed year 1900'),
                                              MaxValueValidator(date.today(),
                                                                message='Date of birth exceeds the present')],
                                  null=True, blank=True, verbose_name='Date of birth')
    gender = models.NullBooleanField(null=True, blank=True, choices=GENDER_CHOICE, verbose_name='Gender')
    country = models.ForeignKey(Country, null=True, blank=True, verbose_name='Country')
    city = models.ForeignKey(City, null=True, blank=True, verbose_name='City')
    income = models.IntegerField(validators=[MinValueValidator(0, message='Income can\'t be negative')],
                                 null=True, blank=True, verbose_name='Income')

    def __unicode__(self):
        return "{}'s profile".format(self.auth.username)

    def __str__(self):
        return "{}'s profile".format(self.auth.username)


@receiver(user_signed_up)
def complete_social_signup(sender, **kwargs):
    """
    Receives user_signed_up signal and provides a hook for populating additional user data.
    The user_signed_up signal is sent when a user signs up for an account. This signal is typically
    followed by a user_logged_in, unless e-mail verification prohibits the user to log in.

    You may populate user data collected from social login's extra info, or other user data here.

    """

    def _get_social_picture(url):
        if url is None:
            return
        try:
            result = urllib.request.urlretrieve(url)
            profile_picture = Avatar()
            profile_picture.primary = 1
            profile_picture.user = user
            profile_picture.avatar.save(os.path.basename(result[0] + '.jpg'), File(open(result[0], 'rb')))
            profile_picture.save()
        except Exception as exc:
            print(str(exc))

    user = kwargs.pop('user')
    request = kwargs.pop('request')

    sociallogin = request.session.get('socialaccount_sociallogin')
    if sociallogin is None:
        """ normal register """
        return

    account_details = sociallogin['account']
    data = account_details['extra_data']
    profile = user.profile

    gender = data.get('gender')
    if gender is not None:
        if gender.lower() == 'male':
            profile.gender = True
        else:
            profile.gender = False
        profile.save()

    if account_details['provider'] == 'facebook':
        _get_social_picture('http://graph.facebook.com/{uid}/picture?type=large'.format(uid=account_details['uid']))
    elif account_details['provider'] == 'google':
        _get_social_picture(data.get('picture'))
    elif account_details['provider'] == 'instagram':
        _get_social_picture(data.get('profile_picture'))
    elif account_details['provider'] == 'twitter':
        _get_social_picture(data.get('profile_image_url'))

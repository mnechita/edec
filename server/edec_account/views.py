from django.shortcuts import render

from allauth.account import app_settings
from allauth.account.forms import LoginForm, SignupForm
from allauth.account.views import LoginView, SignupView
from allauth.utils import get_form_class
from .forms import UserAccountForm
from product.models import Preference
from company.models import Approval
from .forms import UserProfileForm


class JointLoginSignupView(LoginView):
    form_class = LoginForm
    signup_form = SignupForm

    def __init(self, **kwargs):
        super(JointLoginSignupView, self).__init__(**kwargs)

    def get_context_data(self, **kwargs):
        ret = super(JointLoginSignupView, self).get_context_data(**kwargs)
        ret['signupform'] = get_form_class(app_settings.FORMS, 'signup', self.signup_form)
        return ret


login = JointLoginSignupView.as_view()


def index(request):
    return render(request, 'edec_account/index.html')


def error(request, code):
    integer_code = int(code)
    messages = {
        400: "Bad request",
        404: "Page not found",
        405: "Request not allowed",
    }
    extended_messages = {
        400: "Request type or parameters are not allowed !",
        404: "The page you are looking for does not exist !",
        405: "The request you made is not allowed !",
    }
    context = {
        'code': integer_code,
        'message': messages.get(integer_code, 'Unknown error'),
        'extended_message': extended_messages.get(integer_code,
                                                  'An unknown error occurred. Please contact a site administrator !')
    }
    return render(request, 'edec_account/error.html', context)


def profile(request):
    positive_traits = [pref.trait for pref in Preference.objects.filter(user_id=request.user.profile, preference=True)]
    negative_traits = [pref.trait for pref in Preference.objects.filter(user_id=request.user.profile, preference=False)]
    form = UserProfileForm(instance=request.user.profile)
    context = {
        'form': form,
        'food_positive_traits': [trait.name for trait in positive_traits if trait.category_id == 1],
        'food_negative_traits': [trait.name for trait in negative_traits if trait.category_id == 1],
        'clothes_positive_traits': [trait.name for trait in positive_traits if trait.category_id == 2],
        'clothes_negative_traits': [trait.name for trait in negative_traits if trait.category_id == 2],
        'electronics_positive_traits': [trait.name for trait in positive_traits if trait.category_id == 3],
        'electronics_negative_traits': [trait.name for trait in negative_traits if trait.category_id == 3],
        'home_positive_traits': [trait.name for trait in positive_traits if trait.category_id == 4],
        'home_negative_traits': [trait.name for trait in negative_traits if trait.category_id == 4],
        'positive_ideologies': [appr.ideology.name for appr in
                                Approval.objects.filter(user_id=request.user.profile, approval=True)],
        'negative_ideologies': [appr.ideology.name for appr in
                                Approval.objects.filter(user_id=request.user.profile, approval=False)]
    }
    return render(request, 'edec_account/profile.html', context)


def testajax(request):
    print(request.POST)

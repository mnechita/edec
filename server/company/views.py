from django.core.exceptions import *
from django.db.models import Q
from django.shortcuts import render
from django.http import HttpResponseNotAllowed, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from server.settings import BASE_DIR
from .models import *
from .forms import *
from company.models import Compliance, Approval, Ideology

import re
import operator
from functools import reduce
from PIL import Image
from urllib.parse import urlencode


def error(request, code):
    integer_code = int(code)
    messages = {
        400: "Bad request",
        404: "Page not found",
        405: "Request not allowed",
    }
    extended_messages = {
        400: "Request type or parameters are not allowed !",
        404: "The page you are looking for does not exist !",
        405: "The request you made is not allowed !",
    }
    context = {
        'code': integer_code,
        'message': messages.get(integer_code, 'Unknown error'),
        'extended_message': extended_messages.get(integer_code, 'An unknown error occurred. Please contact a site administrator !')
    }
    return render(request, 'edec_account/error.html', context)


@login_required
def search(request):
    # treat a post request on the search page
    if request.method == 'POST':
        return HttpResponseRedirect('/search')

    # treat a get request on the search page
    last_page = 1
    if request.method == 'GET':
        print(1)
        page_nr = request.GET.get('page', 1)
        try:
            page_nr = int(page_nr)
            if page_nr < 0:
                raise Exception('Page number is negative')
        except Exception as exc:
            print('Exception: {}'.format(exc))
            return HttpResponseRedirect('/error/400/')
        first_slice = (page_nr-1) * 10
        second_slice = page_nr * 10
        flag = False
        print(2)
        if len(request.GET) > 1:
            search_form = SearchCompany(request.GET)
            if search_form.is_valid():
                # rename cleaned data
                query = search_form.cleaned_data
                print(query)
                category = query['category']
                print(3)
                # build word searching queries
                queries = query['query'].split()
                if queries:
                    query_set = reduce(operator.__or__, [Q(name__icontains=query) for query in queries])
                else:
                    query_set = Q(name__icontains='')

                if query['order'] == 'Relevance Descending':
                    objects = Company.objects.get_preference_ordered(request.user.profile.id, first_slice - 1, second_slice,
                                                                     query['query'], query['token_from'],
                                                                     query['token_to'], 'desc')
                elif query['order'] == 'Relevance Ascending':
                    objects = Company.objects.get_preference_ordered(request.user.profile.id, first_slice - 1, second_slice,
                                                                     query['query'], query['token_from'],
                                                                     query['token_to'], 'asc')
                else:
                    # parse order by and form string for order
                    order_by = 'name'
                    if query['order'] == 'Alphabetic Ascending':
                        order_by = 'name'
                    elif query['order'] == 'Alphabetic Descending':
                        order_by = '-name'
                    elif query['order'] == 'Relevance Descending':
                        order_by = 'score'
                    elif query['order'] == 'Relevance Ascending':
                        order_by = '-score'

                    # get request data according to searched values
                    if category != 'Any Category':
                        last_page = Company.objects.filter(
                            query_set,
                            category__name=category).count()
                        objects = Company.objects.filter(
                            query_set,
                            category__name=category,
                        ).order_by(order_by)[first_slice:second_slice]
                    else:
                        last_page = Company.objects.filter(
                            query_set).count()
                        objects = Company.objects.filter(
                            query_set,
                        ).order_by(order_by)[first_slice:second_slice]
                print(len(objects))
            else:
                return HttpResponseRedirect('/error/400/')
        else:
            flag = True
            page_nr = request.GET.get('page', 1)
            try:
                page_nr = int(page_nr)
                if page_nr < 0:
                    raise Exception('Page number is negative')
            except Exception as exc:
                print('Exception: {}'.format(exc))
                return HttpResponseRedirect('/error/400/')
            first_slice = (page_nr-1) * 10
            second_slice = page_nr * 10
            order_by = 'name'
            last_page = Company.objects.all().count()
            objects = Company.objects.all().order_by(order_by)[first_slice:second_slice]
            copy_params = {
                'category': 'Any Category',
                'query': '',
                'submit': 1,
                'order': 'Alphabetic Ascending'
            }

        # append results in order to pass to template
        companies = []
        for comp in objects:
            if comp.image and comp.image != 'nophoto':
                try:
                    image = Image.open(BASE_DIR + '\\static\\img\\company\\'
                                       + '{:02d}'.format(comp.id)[-2:] + '\\{}.'.format(comp.id) + comp.image)
                except:
                    image = Image.open(BASE_DIR + '\\static\\images\\slider\\no_photo.png')
            else:
                image = Image.open(BASE_DIR + '\\static\\images\\slider\\no_photo.png')
            image_ratio = image.height / image.width
            if image_ratio > 170/140:
                style = 'height: 100%;'
            else:
                style = 'width: 100%;'

            description = comp.description.replace("''", "'")
            replaceable = re.search(r'</?div[^>]+>', description)
            while replaceable:
                description = description.replace(replaceable.group(0), '')
                replaceable = re.search(r'</?div[^>]+>', description)

            if comp.image and comp.image != 'nophoto':
                image_link = 'global/img/company/' + '{:02d}'.format(comp.id)[-2:] + '/{}.'.format(comp.id) + comp.image
            else:
                image_link = 'global/images/slider/no_photo.png'
            companies.append({
                'name': comp.name.replace("''", "'"),
                'style': style,
                'description' : description,
                'image_link': image_link,
                'comp_id': comp.id
            })
        search_form = SearchCompany(initial={'category': 'Any Category'})

        if page_nr <= 2:
            page_token_prev = 1
            page_token_1 = 1
            page_token_2 = 2
            page_token_3 = 3
            page_token_4 = 4
            page_token_5 = 5
            page_token_next = page_nr + 1
        else:
            page_token_prev = page_nr - 1
            page_token_1 = page_nr - 2
            page_token_2 = page_nr - 1
            page_token_3 = page_nr
            page_token_4 = page_nr + 1
            page_token_5 = page_nr + 2
            page_token_next = page_nr + 1
        if not flag:
            copy_params = {}
            for key in request.GET:
                if key != 'page':
                    copy_params[key] = request.GET[key]
        if last_page % 10 != 0:
            last_page = int(last_page / 10) + 1
        else:
            last_page = int(last_page / 10)
        context = {
            'companies': companies,
            'current_page': page_nr,
            'last_page': last_page,
            'page_token_1': page_token_1,
            'page_token_2': page_token_2,
            'page_token_3': page_token_3,
            'page_token_4': page_token_4,
            'page_token_5': page_token_5,
            'page_token_link_first': '/companies/search?' + urlencode(copy_params) + '&page=1',
            'page_token_link_prev': '/companies/search?' + urlencode(copy_params) + '&page={}'.format(page_token_prev),
            'page_token_link_1': '/companies/search?' + urlencode(copy_params) + '&page={}'.format(page_token_1),
            'page_token_link_2': '/companies/search?' + urlencode(copy_params) + '&page={}'.format(page_token_2),
            'page_token_link_3': '/companies/search?' + urlencode(copy_params) + '&page={}'.format(page_token_3),
            'page_token_link_4': '/companies/search?' + urlencode(copy_params) + '&page={}'.format(page_token_4),
            'page_token_link_5': '/companies/search?' + urlencode(copy_params) + '&page={}'.format(page_token_5),
            'page_token_link_next': '/companies/search?' + urlencode(copy_params) + '&page={}'.format(page_token_next),
            'page_token_link_last': '/companies/search?' + urlencode(copy_params) + '&page={}'.format(last_page),
            'search_form': search_form,
        }
        return render(request, 'company/search.html', context)

    # treat other types of requests on the search page
    return HttpResponseRedirect('/error/405/')


@login_required
def company_details(request, comp_id):
    try:
        comp = Company.objects.get(pk=int(comp_id))
    except ObjectDoesNotExist:
        return HttpResponseRedirect('/error/404/')
    if comp.image and comp.image != 'nophoto':
        try:
            image = Image.open(BASE_DIR + '\\static\\img\\company\\'
                               + '{:02d}'.format(comp.id)[-2:] + '\\{}.'.format(comp.id) + comp.image)
        except:
            image = Image.open(BASE_DIR + '\\static\\images\\slider\\no_photo.png')
    else:
        image = Image.open(BASE_DIR + '\\static\\images\\slider\\no_photo.png')
    image_ratio = image.height / image.width
    if image_ratio > 684/330:
        style = 'height: 100%;'
    else:
        style = 'width: 100%;'
    description = comp.description.replace("''", "'")
    replaceable = re.search(r'</?div[^>]+>', description)
    while replaceable:
        description = description.replace(replaceable.group(0), '')
        replaceable = re.search(r'</?div[^>]+>', description)
    if comp.image and comp.image != 'nophoto':
        image_link = 'global/img/company/' + '{:02d}'.format(comp.id)[-2:] + '/{}.'.format(comp.id) + comp.image
    else:
        image_link = 'global/images/slider/no_photo.png'

    all_ideologies = Ideology.objects.exclude(compliance__company=comp)
    company_ideologies = Compliance.objects.filter(company_id=comp.id)
    user_positive_ideologies = [appr.ideology_id for appr in Approval.objects.filter(user_id=request.user.profile.id, approval=True)]
    user_negative_ideologies = [appr.ideology_id for appr in Approval.objects.filter(user_id=request.user.profile.id, approval=False)]

    context = {
        'positive_ideologies': [com.ideology.name for com in company_ideologies if com.ideology.id in user_positive_ideologies],
        'negative_ideologies': [com.ideology.name for com in company_ideologies if com.ideology.id in user_negative_ideologies],
        'neutral_ideologies': [com.ideology.name for com in company_ideologies if com.ideology.id not in user_negative_ideologies and com.ideology.id not in user_positive_ideologies],
        'missing_ideologies': [ide.name for ide in all_ideologies],
        'comp_name': comp.name,
        'comp_img': image_link,
        'comp_desc': description.replace('Good to Know', '').replace('\\n', '\n'),
        'style': style,
    }
    return render(request, 'company/company_details.html', context)

from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'error/(?P<code>\d{3})/$', views.error, name='error'),
    url(r'^search$', views.search, name='search'),
    url(r'^search/$', views.search, name='search'),
    url(r'^details/(?P<comp_id>\d+)/$', views.company_details, name='details'),
]
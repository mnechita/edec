__author__ = 'vlupoaie'

from django import forms


class SearchCompany(forms.Form):
    category = forms.CharField(max_length=64)
    query = forms.CharField(max_length=64, required=False)
    order = forms.CharField(max_length=64, initial='Alphabetic Ascending')

    def __init__(self, *args, **kwargs):
        super(SearchCompany, self).__init__(*args, **kwargs)
        # category
        self.fields['category'].widget = forms.HiddenInput()
        self.fields['category'].widget.attrs.update({
            'id': 'hiddenCategoryCompany'
        })
        # query
        self.fields['query'].widget.attrs.update({
            'class': 'form-control'
        })
        # order
        self.fields['order'].widget = forms.HiddenInput()
        self.fields['order'].widget.attrs.update({
            'id': 'hiddenFilterCompany'
        })
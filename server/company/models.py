from django.db import models
from edec_account.models import UserProfile
from django.core.validators import RegexValidator


class Company(models.Model):

    class Manager(models.Manager):
        def get_preference_ordered(self, user_id, page1, page2, query_string=None, low_value=None, high_value=None,
                                   order='desc'):
            where_clause = ''
            if query_string:
                tokens = query_string.lower().split(' ')
                where_clause = 'and (lower(name) like \'%%{}%%\' '.format(tokens[0])
                for token in tokens[1:]:
                    where_clause += 'or lower(name) like \'%%{}%%\' '.format(token)
                where_clause += ') '
            if low_value:
                where_clause += 'and token >= {} '.format(low_value)
            if high_value:
                where_clause += 'and token <= {} '.format(high_value)

            query = \
                'select id from ' \
                '(select id, rownum rn from ' \
                '(select pp.id id ' \
                'from ' \
                '(select id ' \
                'from company_company ' \
                'where 1 = 1 {}) pp ' \
                'left outer join ' \
                '(select company_id, positive_score, negative_score ' \
                'from metrics_usercompany ' \
                'where user_id = %s) up ' \
                'on pp.id = up.company_id ' \
                'order by NVL(up.positive_score - up.negative_score, 0) {}) ' \
                'where rownum <= %s) ' \
                'where rn >= %s'.format(where_clause, order)
            return [self.get(id=res.id) for res in self.raw(query, [user_id, page2, page1])]

    name = models.CharField(max_length=64, unique=True, validators=[RegexValidator(r'^[\w \'-]+$')],
                            verbose_name='Company\'s full name')
    description = models.TextField(validators=[RegexValidator(r'^[\x20-\xff]+$')], blank=True, null=True,
                                   verbose_name='Company\'s description')
    image = models.CharField(max_length=256, null=True, blank=True, verbose_name='Static path to company image')

    objects = Manager()

    def get_categories(self):
        return [a['category_id'] for a in self.product_set.values('category_id').distinct()]

    def get_subcategories(self):
        return [a['subcategory_id'] for a in self.product_set.values('subcategory_id').distinct()]


class Ideology(models.Model):
    name = models.CharField(max_length=64, unique=True, validators=[RegexValidator(r'^[\w \'-]+$')],
                            verbose_name='Ideology\'s name')
    description = models.TextField(validators=[RegexValidator(r'^[\x20-\xff]+$')], blank=True, null=True,
                                   verbose_name='Ideology\'s description')


class Compliance(models.Model):
    company = models.ForeignKey(Company, verbose_name='The company that complies to the ideology')
    ideology = models.ForeignKey(Ideology, verbose_name='The ideology the company complies to')

    class Meta:
        unique_together = ('company', 'ideology')


class Approval(models.Model):
    APPROVAL_CHOICE = (
        (None, 'Choose approval'),
        (False, 'I disapprove'),
        (True, 'I approve')
    )

    user = models.ForeignKey(UserProfile, verbose_name='The user that approves/disapproves the ideology')
    ideology = models.ForeignKey(Ideology, verbose_name='The ideology that the user approves/disapproves')
    approval = models.BooleanField(choices=APPROVAL_CHOICE, verbose_name='Approve or disapprove')

    class Meta:
        unique_together = ('user', 'ideology')


class CompanyReview(models.Model):
    title = models.CharField(max_length=64, validators=[RegexValidator(r'^[\w \'')],
                             verbose_name='The title of the review')
    content = models.TextField(validators=[RegexValidator(r'^[\x20-\xff]+$')],
                               verbose_name='The content of the review')
    user = models.ForeignKey(UserProfile, verbose_name='The user that makes the review')
    company = models.ForeignKey(Company, verbose_name='The company that is reviewed')


class CompanyComment(models.Model):
    title = models.CharField(max_length=64, validators=[RegexValidator(r'^[\w \'')],
                             verbose_name='The title of the comment')
    content = models.TextField(validators=[RegexValidator(r'^[\x20-\xff]+$')],
                               verbose_name='The content of the review')
    user = models.ForeignKey(UserProfile, verbose_name='The user that writes the comment')
    company_review = models.ForeignKey(CompanyReview, verbose_name='The review that is being commented')

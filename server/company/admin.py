from django.contrib import admin
from .models import Company, Approval, CompanyComment, CompanyReview, Compliance, Ideology

# Register your models here.
admin.site.register(Company)
admin.site.register(Approval)
admin.site.register(CompanyReview)
admin.site.register(CompanyComment)
admin.site.register(Compliance)
admin.site.register(Ideology)

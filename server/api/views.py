from django.shortcuts import HttpResponse, get_object_or_404
from edec_account.models import UserProfile
from product.models import Preference, Product, ProductTrait, Category, Subcategory, Trait
from company.models import Company, Approval, Compliance, Ideology
from metrics.models import UserUser, UserProduct, UserCompany, ProductProduct, CompanyCompany
from server.settings import MEDIA_URL
import json
import datetime

MAXIMUM_OFFSET = 1000
MAXIMUM_SUGGESTIONS_COUNT = 15


def home_view(request):
    return HttpResponse('Api Home')


def users_view(request, start=0, offset=1):
    if request.method != 'GET':
        return HttpResponse(status=405)
    start = int(start)
    offset = int(offset)
    offset = min(offset, MAXIMUM_OFFSET)

    response = {'users': []}
    for user in UserProfile.objects.all()[start:start+offset]:
        info = {
            'id': user.id,
            'first_name': user.auth.first_name,
            'last_name': user.auth.last_name,
            'preferences': Preference.objects.filter(user_id=user.id).count(),
            'approvals': Approval.objects.filter(user_id=user.id).count()
        }
        response['users'].append(info)
    return HttpResponse(json.dumps(response))


def user_view(request, pk):
    if request.method != 'GET':
        return HttpResponse(status=405)

    response = {'user': {}}
    user = get_object_or_404(UserProfile, pk=pk)
    response['user'] = {
        'id': user.id,
        'first_name': user.auth.first_name,
        'last_name': user.auth.last_name,
        'city': user.city.name,
        'country': user.country.name,
        'age': (datetime.date.today() - user.birth_date).days/365,
        'preferred_traits': [
            {
                'id': preference.trait_id,
                'name': preference.trait.name,
                'preference': preference.preference
            } for preference in Preference.objects.filter(user_id=user.id)],
        'approved_ideologies': [
            {
                'id': approval.ideology_id,
                'name': approval.ideology.name,
                'approval': approval.approval
            } for approval in Approval.objects.filter(user_id=user.id)],
        'suggested_products': [
            {
                'id': prod.id,
                'percent': score
            } for prod, score in sorted(UserProduct.objects.suggested_products(user_id=user.id),
                                        key=lambda a: a[1], reverse=True)[:MAXIMUM_SUGGESTIONS_COUNT]
        ],
        'unsuggested_products': [
            {
                'id': prod.id,
                'percent:': score
            } for prod, score in sorted(UserProduct.objects.unsuggested_products(user_id=user.id),
                                        key=lambda a: a[1], reverse=True)[:MAXIMUM_SUGGESTIONS_COUNT]
        ],
        'suggested_companies': [
            {
                'id': comp.id,
                'percent': score
            } for comp, score in sorted(UserCompany.objects.suggested_companies(user_id=user.id),
                                        key=lambda a: a[1], reverse=True)[:MAXIMUM_SUGGESTIONS_COUNT]
        ],
        'unsuggested_companies': [
            {
                'id': comp.id,
                'percent': score
            } for comp, score in sorted(UserCompany.objects.suggested_companies(user_id=user.id),
                                        key=lambda a: a[1], reverse=True)[:MAXIMUM_SUGGESTIONS_COUNT]
        ],
        'similar_users': [
            {
                'id': user.id,
                'percent': score
            } for user, score in sorted(UserUser.objects.similar_users(user_id=user.id),
                                        key=lambda a: a[1], reverse=True)[:MAXIMUM_SUGGESTIONS_COUNT]
        ]
    }
    return HttpResponse(json.dumps(response))


def products_view(request, start=0, offset=1):
    if request.method != 'GET':
        return HttpResponse(status=405)
    start = int(start)
    offset = int(offset)
    offset = min(offset, MAXIMUM_OFFSET)

    response = {'products': []}
    for product in Product.objects.all()[start:start+offset]:
        info = {
            'id': product.id,
            'name': product.name,
            'company': product.company.name,
            'category': product.category.name,
            'subcategory': product.subcategory.name,
            'traits': ProductTrait.objects.filter(product_id=product.id).count()
        }
        response['products'].append(info)
    return HttpResponse(json.dumps(response))


def product_view(request, pk):
    if request.method != 'GET':
        return HttpResponse(status=405)

    response = {'product': {}}
    prod = get_object_or_404(Product, pk=pk)
    response['product'] = {
        'id': prod.id,
        'name': prod.name,
        'company': prod.company.name,
        'category': prod.category.name,
        'subcategory': prod.subcategory.name,
        'image': prod.get_image_url(),
        'similar_products': [
            {
                'id': p.id,
                'percent': score
            } for p, score in sorted(ProductProduct.objects.similar_products(product_id=prod.id),
                                     key=lambda a: a[1], reverse=True)[:MAXIMUM_SUGGESTIONS_COUNT]
        ],
        'traits': [
            {
                'id': pt.trait_id,
                'name': pt.trait.name,
            } for pt in ProductTrait.objects.filter(product_id=prod.id)
        ]
    }
    return HttpResponse(json.dumps(response))


def companies_view(request, start=0, offset=1):
    if request.method != 'GET':
        return HttpResponse(status=405)
    start = int(start)
    offset = int(offset)
    offset = min(offset, MAXIMUM_OFFSET)

    response = {'companies': []}
    for comp in Company.objects.all()[start:start+offset]:
        info = {
            'id': comp.id,
            'name': comp.name,
            'categories': [Category.objects.get(pk=cat).name for cat in comp.get_categories()],
            'ideologies': Compliance.objects.filter(company_id=comp.id).count()
        }
        response['companies'].append(info)
    return HttpResponse(json.dumps(response))


def company_view(request, pk):
    if request.method != 'GET':
        return HttpResponse(status=405)

    response = {'company': {}}
    comp = get_object_or_404(Company, pk=pk)
    response['company'] = {
        'id': comp.id,
        'name': comp.name,
        'categories': [Category.objects.get(pk=cat).name for cat in comp.get_categories()],
        'subcategories': [Subcategory.objects.get(pk=subcat).name for subcat in comp.get_subcategories()],
        'ideologies': [
            {
                'id': comp.ideology_id,
                'name': comp.ideology.name
            } for comp in Compliance.objects.filter(company_id=comp.id)
        ],
        'similar_companies': [
            {
                'id': c.id,
                'percent': score
            } for c, score in sorted(CompanyCompany.objects.similar_companies(company_id=comp.id),
                                     key=lambda a: a[1], reverse=True)[:MAXIMUM_SUGGESTIONS_COUNT]
        ]
    }
    return HttpResponse(json.dumps(response))


def traits_view(request, start=0, offset=1):
    if request.method != 'GET':
        return HttpResponse(status=405)
    start = int(start)
    offset = int(offset)
    offset = min(offset, MAXIMUM_OFFSET)

    response = {'traits': []}
    for trait in Trait.objects.all()[start:start+offset]:
        info = {
            'id': trait.id,
            'name': trait.name,
            'category': trait.category.name,
            'description': trait.description,
            'products': trait.producttrait_set.count(),
            'users': {
                'prefer': trait.preference_set.filter(preference=True).count(),
                'dislike': trait.preference_set.filter(preference=False).count()
            }
        }
        response['traits'].append(info)
    return HttpResponse(json.dumps(response))


def trait_view(request, pk):
    if request.method != 'GET':
        return HttpResponse(status=405)

    response = {'trait': {}}
    trait = get_object_or_404(Trait, pk=pk)
    response['trait'] = {
        'id': trait.id,
        'name': trait.name,
        'category': trait.category.name,
        'description': trait.description,
        'products': [
            {
                'id': pt.product_id,
                'name': pt.product.name
            } for pt in trait.producttrait_set.all()
        ],
        'users': {
            'prefer': [pref.user_id for pref in trait.preference_set.filter(preference=True)],
            'dislike': [pref.user_id for pref in trait.preference_set.filter(preference=False)]
        }
    }
    return HttpResponse(json.dumps(response))


def ideologies_view(request, start=0, offset=1):
    if request.method != 'GET':
        return HttpResponse(status=405)
    start = int(start)
    offset = int(offset)
    offset = min(offset, MAXIMUM_OFFSET)

    response = {'ideologies': []}
    for ideol in Ideology.objects.all()[start:start+offset]:
        info = {
            'id': ideol.id,
            'name': ideol.name,
            'description': ideol.description,
            'companies': ideol.compliance_set.count(),
            'users': {
                'prefer': ideol.approval_set.filter(approval=True).count(),
                'dislike': ideol.approval_set.filter(approval=False).count()
            }
        }
        response['ideologies'].append(info)
    return HttpResponse(json.dumps(response))


def ideology_view(request, pk):
    if request.method != 'GET':
        return HttpResponse(status=405)

    response = {'ideology': {}}
    ideol = get_object_or_404(Ideology, pk=pk)
    response['ideology'] = {
        'id': ideol.id,
        'name': ideol.name,
        'description': ideol.description,
        'companies': [
            {
                'id': compl.company_id,
                'name': compl.company.name
            } for compl in ideol.compliance_set.all()
        ],
        'users': {
            'prefer': [approval.user_id for approval in ideol.approval_set.filter(approval=True)],
            'dislike': [approval.user_id for approval in ideol.approval_set.filter(approval=False)]
        }
    }
    return HttpResponse(json.dumps(response))
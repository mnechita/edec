from django.conf.urls import url
from api.views import home_view
from api.views import users_view, user_view
from api.views import products_view, product_view
from api.views import companies_view, company_view
from api.views import traits_view, trait_view
from api.views import ideologies_view, ideology_view

urlpatterns = [
    url(r'^$', home_view, name='api_home'),
]

urlpatterns += [
    url(r'^users/all/(?P<start>\d+)-(?P<offset>\d+)$', users_view),
    url(r'^users/all/(?P<start>\d+)$', users_view),
    url(r'^users/all/$', users_view),

    url(r'^users/(?P<start>\d+)-(?P<offset>\d+)$', users_view),
    url(r'^users/(?P<pk>\d+)$', user_view),
    url(r'^users/$', users_view)
]

urlpatterns += [
    # url(r'^products/$', )
    url(r'^products/all/(?P<start>\d+)-(?P<offset>\d+)$', products_view),
    url(r'^products/all/(?P<start>\d+)$', products_view),
    url(r'^products/all/$', products_view),

    url(r'^products/(?P<start>\d+)-(?P<offset>\d+)$', products_view),
    url(r'^products/(?P<pk>\d+)$', product_view),
    url(r'^products/$', products_view)
]

urlpatterns += [
    url(r'^companies/all/(?P<start>\d+)-(?P<offset>\d+)$', companies_view),
    url(r'^companies/all/(?P<start>\d+)$', companies_view),
    url(r'^companies/all/$', companies_view),

    url(r'^companies/(?P<start>\d+)-(?P<offset>\d+)$', companies_view),
    url(r'^companies/(?P<pk>\d+)$', company_view),
    url(r'^companies/$', companies_view)
]

urlpatterns += [
    url(r'^traits/all/(?P<start>\d+)-(?P<offset>\d+)$', traits_view),
    url(r'^traits/all/(?P<start>\d+)$', traits_view),
    url(r'^traits/all/$', traits_view),

    url(r'^traits/(?P<start>\d+)-(?P<offset>\d+)$', traits_view),
    url(r'^traits/(?P<pk>\d+)$', trait_view),
    url(r'^traits/$', traits_view)
]

urlpatterns += [
    url(r'^ideologies/all/(?P<start>\d+)-(?P<offset>\d+)$', ideologies_view),
    url(r'^ideologies/all/(?P<start>\d+)$', ideologies_view),
    url(r'^ideologies/all/$', ideologies_view),

    url(r'^ideologies/(?P<start>\d+)-(?P<offset>\d+)$', ideologies_view),
    url(r'^ideologies/(?P<pk>\d+)$', ideology_view),
    url(r'^ideologies/$', ideologies_view)
]
jQuery(function($) {'use strict',

	//#main-slider
	$(function(){
		$('#main-slider.carousel').carousel({
			interval: 8000
		});
	});


	// accordian
	$('.accordion-toggle').on('click', function(){
		$(this).closest('.panel-group').children().each(function(){
		$(this).find('>.panel-heading').removeClass('active');
		 });

	 	$(this).closest('.panel-heading').toggleClass('active');
	});

	//Initiat WOW JS
	new WOW().init();

	// portfolio filter
	$(window).load(function(){'use strict';
		var $portfolio_selectors = $('.portfolio-filter >li>a');
		var $portfolio = $('.portfolio-items');
		$portfolio.isotope({
			itemSelector : '.portfolio-item',
			layoutMode : 'fitRows'
		});
		
		$portfolio_selectors.on('click', function(){
			$portfolio_selectors.removeClass('active');
			$(this).addClass('active');
			var selector = $(this).attr('data-filter');
			$portfolio.isotope({ filter: selector });
			return false;
		});
	});

	// Contact form
	var form = $('#main-contact-form');
	form.submit(function(event){
		event.preventDefault();
		var form_status = $('<div class="form_status"></div>');
		$.ajax({
			url: $(this).attr('action'),

			beforeSend: function(){
				form.prepend( form_status.html('<p><i class="fa fa-spinner fa-spin"></i> Email is sending...</p>').fadeIn() );
			}
		}).done(function(data){
			form_status.html('<p class="text-success">' + data.message + '</p>').delay(3000).fadeOut();
		});
	});

	
	//goto top
	$('.gototop').click(function(event) {
		event.preventDefault();
		$('html, body').animate({
			scrollTop: $("body").offset().top
		}, 500);
	});	

	//Pretty Photo
	$("a[rel^='prettyPhoto']").prettyPhoto({
		social_tools: false
	});	
});

$(document).ready(function(e){
    $('.search-panel .dropdown-menu').find('a').click(function(e) {
		e.preventDefault();
		var param = $(this).attr("href").replace("#","");
		var concept = $(this).text();
		$('.search-panel span#search_concept').text(concept);
		$('.input-group #search_param').val(param);
	});
});

$("#dropdown-food1 li").click(function () {
    var name = $(this).text();
    
    $("#hiddenSubcategory1").val(name);
    $("#menu1food").text(name);
    //alert($("#hiddenSubcategory").val())
    
 });

$("#dropdown-food2 li").click(function () {
    var name = $(this).text();
    
    $("#hiddenFilter1").val(name);
    $("#menu2food").text(name);
    //alert($("#hiddenFilter").val())
    
 });

$("#dropdown-clothes1 li").click(function () {
    var name = $(this).text();
    
    $("#hiddenSubcategory2").val(name);
    $("#menu1clothes").text(name);
    //alert($("#hiddenSubcategory").val())
    
 });

$("#dropdown-clothes2 li").click(function () {
    var name = $(this).text();
    
    $("#hiddenFilter2").val(name);
    $("#menu2clothes").text(name);
    //alert($("#hiddenFilter").val())
    
 });

$("#dropdown-electronics1 li").click(function () {
    var name = $(this).text();
    
    $("#hiddenSubcategory3").val(name);
    $("#menu1electronics").text(name);
    //alert($("#hiddenSubcategory").val())
    
 });

$("#dropdown-electronics2 li").click(function () {
    var name = $(this).text();
    
    $("#hiddenFilter3").val(name);
    $("#menu2electronics").text(name);
    //alert($("#hiddenFilter").val())
    
 });

$("#dropdown-home1 li").click(function () {
    var name = $(this).text();
    
    $("#hiddenSubcategory4").val(name);
    $("#menu1home").text(name);
    
 });

$("#dropdown-home2 li").click(function () {
    var name = $(this).text();
    
    $("#hiddenFilter4").val(name);
    $("#menu2home").text(name);
    
 });

$("#nav-tabs li").click(function () {
    $("#hiddenSubcategory1").val("All Subcategories");
    $("#hiddenSubcategory2").val("All Subcategories");
    $("#hiddenSubcategory3").val("All Subcategories");
    $("#hiddenSubcategory4").val("All Subcategories");
    $("#hiddenFilter1").val("Alphabetic Ascending");
    $("#hiddenFilter2").val("Alphabetic Ascending");
    $("#hiddenFilter3").val("Alphabetic Ascending");
    $("#hiddenFilter4").val("Alphabetic Ascending");
    $("#menu1food").text("All Subcategories");
    $("#menu2food").text("Alphabetic Ascending");
    $("#menu1clothes").text("All Subcategories");
    $("#menu2clothes").text("Alphabetic Ascending");
    $("#menu1electronics").text("All Subcategories");
    $("#menu2electronics").text("Alphabetic Ascending");
    $("#menu1home").text("All Subcategories");
    $("#menu2home").text("Alphabetic Ascending");
    $("#fromvalue1").val("");
    $("#tovalue1").val("");
    $("#fromvalue2").val("");
    $("#tovalue2").val("");
    $("#fromvalue3").val("");
    $("#tovalue3").val("");
    $("#fromvalue4").val("");
    $("#tovalue4").val("");
 });

jQuery(document).ready(function($) {
    $(".clickable-row").click(function() {
        window.document.location = $(this).data("href");
    });
});
import os
import re
import shutil
import time
import requests
import sys
from multiprocessing import Process

import google_logos
import wikipedia


def download_image(content, prod_name, prod_file, prod_link):

    USER_AGENT = 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.109 Safari/537.36'
    ACCEPT_HTML = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'
    ACCEPT_IMAGE = 'image/webp,image/*,*/*;q=0.8'
    ACCEPT_ENCODING = 'gzip, deflate, sdch'
    ACCEPT_ENCODING_2 = 'gzip, deflate'
    ACCEPT_LANGUAGE = 'ro-RO,ro;q=0.8,en-US;q=0.6,en;q=0.4,de;q=0.2,es;q=0.2'
    CONNECTION = 'keep-alive'
    HOST = 'www.6pm.com'

    headers = {
        'User-Agent': USER_AGENT,
        'Accept': ACCEPT_HTML,
        'Accept-Encoding': ACCEPT_ENCODING,
        'Accept-Language': ACCEPT_LANGUAGE,
        'Referer': 'http://www.hsn.com/shop/electronics/ec',
        'Host': 'www.hsn.com'
    }

    product_headers = {
        'User-Agent': USER_AGENT,
        'Accept': ACCEPT_HTML,
        'Accept-Encoding': ACCEPT_ENCODING,
        'Accept-Language': ACCEPT_LANGUAGE,
        'Host': 'www.hsn.com'
    }

    img_headers = {
        'User-Agent': USER_AGENT,
        'Accept': ACCEPT_IMAGE,
        'Accept-Encoding': ACCEPT_ENCODING
    }

    prod_dir = r'D:\CrawledData\Products\Electronics'
    brands_dir = r'D:\CrawledData\Brands2'

    illegal_characters = {
        '/': '-',
        '\\': '-',
        '*': '.',
        ':': '-',
        '"': "'",
        '?': '.',
        '<': '.',
        '>': '.',
        '|': '-'
    }

    image = re.search('<a class="product-image" href="([^"]+)"', content)
    if not image:
        try:
            print('Wtf, no image ?! {}'.format(prod_name))
        except:
            pass
    else:
        img_headers['Referer'] = prod_link
        img_response = requests.get('http:' + image.group(1), headers=img_headers)
        extension = image.group(1).rsplit('.', 1)[1].strip()
        if not extension or len(extension) > 4:
            extension = 'noextension'
        prod_image = prod_file + '.{}'.format(extension)
        with open(prod_image, 'wb') as handle:
            for chunk in img_response.iter_content(1024):
                handle.write(chunk)



def main():

    USER_AGENT = 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.109 Safari/537.36'
    ACCEPT_HTML = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'
    ACCEPT_IMAGE = 'image/webp,image/*,*/*;q=0.8'
    ACCEPT_ENCODING = 'gzip, deflate, sdch'
    ACCEPT_ENCODING_2 = 'gzip, deflate'
    ACCEPT_LANGUAGE = 'ro-RO,ro;q=0.8,en-US;q=0.6,en;q=0.4,de;q=0.2,es;q=0.2'
    CONNECTION = 'keep-alive'
    HOST = 'www.6pm.com'

    headers = {
        'User-Agent': USER_AGENT,
        'Accept': ACCEPT_HTML,
        'Accept-Encoding': ACCEPT_ENCODING,
        'Accept-Language': ACCEPT_LANGUAGE,
        'Referer': 'http://www.hsn.com/shop/electronics/ec',
        'Host': 'www.hsn.com'
    }

    product_headers = {
        'User-Agent': USER_AGENT,
        'Accept': ACCEPT_HTML,
        'Accept-Encoding': ACCEPT_ENCODING,
        'Accept-Language': ACCEPT_LANGUAGE,
        'Host': 'www.hsn.com'
    }

    img_headers = {
        'User-Agent': USER_AGENT,
        'Accept': ACCEPT_IMAGE,
        'Accept-Encoding': ACCEPT_ENCODING
    }

    prod_dir = r'D:\CrawledData\Products\Electronics'
    brands_dir = r'D:\CrawledData\Brands2'

    illegal_characters = {
        '/': '-',
        '\\': '-',
        '*': '.',
        ':': '-',
        '"': "'",
        '?': '.',
        '<': '.',
        '>': '.',
        '|': '-'
    }

    categories = [
        ('Video Games & Entertainment', 'http://www.hsn.com/shop/video-games-and-entertainment/ec0570'),
        ('Video Games & Entertainment', 'http://www.hsn.com/shop/video-games-and-entertainment/ec0570?view=all&sort=&skip=60&take=60&page=2&scroll=false'),
        ('Video Games & Entertainment', 'http://www.hsn.com/shop/video-games-and-entertainment/ec0570?view=all&sort=&skip=120&take=60&page=3&scroll=false'),
        ('Video Games & Entertainment', 'http://www.hsn.com/shop/video-games-and-entertainment/ec0570?view=all&sort=&skip=180&take=60&page=4&scroll=false'),
        ('Video Games & Entertainment', 'http://www.hsn.com/shop/video-games-and-entertainment/ec0570?view=all&sort=&skip=240&take=60&page=5&scroll=false'),
        ('Video Games & Entertainment', 'http://www.hsn.com/shop/video-games-and-entertainment/ec0570?view=all&sort=&skip=300&take=60&page=6&scroll=false'),
        ('Video Games & Entertainment', 'http://www.hsn.com/shop/video-games-and-entertainment/ec0570?view=all&sort=&skip=360&take=60&page=7&scroll=false'),
        ('Video Games & Entertainment', 'http://www.hsn.com/shop/video-games-and-entertainment/ec0570?view=all&sort=&skip=420&take=60&page=8&scroll=false'),
        ('Video Games & Entertainment', 'http://www.hsn.com/shop/video-games-and-entertainment/ec0570?view=all&sort=&skip=480&take=60&page=9&scroll=false'),
        ('Video Games & Entertainment', 'http://www.hsn.com/shop/video-games-and-entertainment/ec0570?view=all&sort=&skip=540&take=60&page=10&scroll=false'),
        ('Video Games & Entertainment', 'http://www.hsn.com/shop/video-games-and-entertainment/ec0570?view=all&sort=&skip=600&take=60&page=11&scroll=false'),
        ('Video Games & Entertainment', 'http://www.hsn.com/shop/video-games-and-entertainment/ec0570?view=all&sort=&skip=660&take=60&page=12&scroll=false'),

        ('Computers & Office', 'http://www.hsn.com/shop/computers-and-office/ec0027?view=all'),
        ('Computers & Office', 'http://www.hsn.com/shop/computers-and-office/ec0027?view=all&sort=&skip=58&take=58&page=2&scroll=false'),
        ('Computers & Office', 'http://www.hsn.com/shop/computers-and-office/ec0027?view=all&sort=&skip=116&take=58&page=3&scroll=false'),
        ('Computers & Office', 'http://www.hsn.com/shop/computers-and-office/ec0027?view=all&sort=&skip=174&take=47&page=4&scroll=false'),

        ('TVs & Home Theater', 'http://www.hsn.com/shop/tvs-and-home-theater/ec0073?view=all'),
        ('TVs & Home Theater', 'http://www.hsn.com/shop/tvs-and-home-theater/ec0073?view=all&sort=&skip=58&take=58&page=2&scroll=false'),
        ('TVs & Home Theater', 'http://www.hsn.com/shop/tvs-and-home-theater/ec0073?view=all&sort=&skip=116&take=12&page=3&scroll=false'),

        ('Phones', 'http://www.hsn.com/shop/phones/ec0542?view=all'),
        ('Phones', 'http://www.hsn.com/shop/phones/ec0542?view=all&sort=&skip=58&take=46&page=2&scroll=false'),

        ('Tablets', 'http://www.hsn.com/shop/tablets/ec0476'),

        ('Wearable Tech', 'http://www.hsn.com/shop/wearable-tech/ec0544'),

        ('Cameras & Camcorders', 'http://www.hsn.com/shop/cameras-and-camcorders/ec0405?view=all'),

        ('Audio', 'http://www.hsn.com/shop/audio/ec0202'),
        ('Audio', 'http://www.hsn.com/shop/audio/ec0202?view=all&sort=&skip=60&take=60&page=2&scroll=false'),
        ('Audio', 'http://www.hsn.com/shop/audio/ec0202?view=all&sort=&skip=120&take=37&page=3&scroll=false'),

        ('Smart Home', 'http://www.hsn.com/shop/smart-home/ec0447'),

        ('Car Electronics', 'http://www.hsn.com/shop/car-electronics/ec0427'),

        ('Portable Chargers', 'http://www.hsn.com/shop/portable-chargers/ec0484'),

        ('Accessories', 'http://www.hsn.com/shop/accessories/ec0498'),
        ('Accessories', 'http://www.hsn.com/shop/accessories/ec0498?view=all&sort=&skip=60&take=60&page=2&scroll=false'),
        ('Accessories', 'http://www.hsn.com/shop/accessories/ec0498?view=all&sort=&skip=120&take=60&page=3&scroll=false'),
        ('Accessories', 'http://www.hsn.com/shop/accessories/ec0498?view=all&sort=&skip=180&take=60&page=4&scroll=false'),
        ('Accessories', 'http://www.hsn.com/shop/accessories/ec0498?view=all&sort=&skip=240&take=60&page=5&scroll=false')
    ]

    prod_counter = 0
    brand_counter = 0
    for categ, link in categories:
        print('[info] Parsing category: {} - {}'.format(categ, link))
        categ_dir = os.path.join(prod_dir, categ)
        if not os.path.isdir(categ_dir):
            os.mkdir(categ_dir)
        response = requests.get(link, headers=headers).text
        product_headers['Referer'] = link
        products = re.findall('<div class="item product-item(?:.|\s)+?<a href="([^"]+)">([^<]+)<(?:.|\s)+? price">([^<]+)<', response)
        for product in products:
            try:
                print('[info] Parsing product: {} - {}'.format(product[1], 'http://www.hsn.com' + product[0]))
            except:
                try:
                    print('[info] Parsing product: ? - {}'.format('http://www.hsn.com' + product[0]))
                except:
                    try:
                        print('[info] Parsing product: {} - ?'.format(product[1]))
                    except:
                        print('[info] Parsing product: ? - ?')
            # getting product data
            info = {}
            prod_link = 'http://www.hsn.com' + product[0]
            prod_name = product[1]
            prod_price = int(float(product[2].replace('$', '').replace(',', '').split('-')[0].strip()))
            product_page = requests.get(prod_link, headers=product_headers).text
            description = re.search('<div class="content copy">(?:\s*<div>\s*)?((?:.|\s)*?)<\/div>', product_page)
            if description:
                description = description.group(1)
                replaceable = re.search('<[^>]+>.*Good to Know.*<[^>]+>', description)
                if replaceable:
                    description = description.replace(replaceable.group(0), '')
                description = description.strip()
            else:
                description = 'No description available for this product'
            info['description'] = description
            # description = re.search('<div class="content copy">(?:.|\s)*?(?:<br \/>\s*)(?:<\/strong>)?((?:.|\s)+?)(?:(?:<br \/>)|(?:<\/div>))', product_page)
            # if not description:
            #     info['description'] = 'No description available for this product'
            # else:
            #     info['description'] = description.group(1)
            brand = re.search('<span itemprop="brand" class="hidden">([^<]+)<\/span>', product_page)
            if not brand:
                try:
                    print('No brand for product: {}'.format(prod_name))
                except:
                    pass
                continue
            else:
                brand = brand.group(1)

            # writing product info file
            prod_file = os.path.join(categ_dir, 'product_{:05d}'.format(prod_counter))
            prod_counter += 1
            with open(prod_file, 'w') as handle:
                handle.write('Brand: {}\n'.format(brand))
                handle.write('Product: {}\n'.format(prod_name))
                handle.write('Filter: {}\n'.format(prod_price))
                handle.write('Info: {}\n'.format(info))

            # getting product image
            proc = Process(target=download_image, args=(product_page, prod_name, prod_file, prod_link))
            proc.start()
            proc.join(timeout=20)
            if proc.is_alive():
                print('Product image download killed !')
                proc.terminate()
                with open(prod_file + '.nophoto', 'w') as handle:
                     pass

            # treating product brand
            temp = brand
            for c in brand:
                if illegal_characters.get(c):
                    temp = temp.replace(c, illegal_characters[c])
            temp = temp.strip()
            brand_path = os.path.join(brands_dir, temp)
            if os.path.isdir(brand_path) and len(os.listdir(brand_path)) >= 3:
                # check if this brand has this category in its .categories file
                brand_listing = os.listdir(brand_path)
                for brand_file_x in brand_listing:
                    if brand_file_x.endswith('.categories'):
                        full_path_x = os.path.join(brand_path, brand_file_x)
                        with open(full_path_x, 'r') as handle:
                            content = handle.read().split('\n')
                        if 'Electronics' not in content:
                            print('Brand with multiple categories ! {}'.format(brand))
                            with open(full_path_x, 'a') as handle:
                                handle.write('Electronics\n')
                continue
            elif not os.path.isdir(brand_path):
                os.mkdir(brand_path)
            brand_name = os.path.join(brand_path, 'brand_{:05d}.name'.format(brand_counter))
            with open(brand_name, 'wb') as handle:
                handle.write((brand + '\n').encode(errors='replace'))
            brand_description = os.path.join(brand_path, 'brand_{:05d}.description'.format(brand_counter))
            description = wikipedia.search_term(brand)
            if not description:
                description = 'No description available for this company. You can supply a description by contacting us.'
            description = description + '\n'
            with open(brand_description, 'wb') as handle:
                handle.write(description.encode(errors='replace'))
            brand_categ = os.path.join(brand_path, 'brand_{:05d}.categories'.format(brand_counter))
            with open(brand_categ, 'w') as handle:
                handle.write('Electronics\n')
            brand_logo = os.path.join(brand_path, 'brand_{:05d}'.format(brand_counter))
            proc = Process(target=google_logos.download_icon, args=(brand_logo, brand))
            proc.start()
            proc.join(timeout=20)
            if proc.is_alive():
                print('Brand logo downloading killed !')
                proc.terminate()
            brand_counter += 1


if __name__ == '__main__':
    main()

import os
import re
import shutil
import time
import requests
import sys
from multiprocessing import Process

import google_logos
import wikipedia


def download_image(content, prod_name, prod_file, prod_link):

    USER_AGENT = 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.109 Safari/537.36'
    ACCEPT_HTML = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'
    ACCEPT_IMAGE = 'image/webp,image/*,*/*;q=0.8'
    ACCEPT_ENCODING = 'gzip, deflate, sdch'
    ACCEPT_ENCODING_2 = 'gzip, deflate'
    ACCEPT_LANGUAGE = 'ro-RO,ro;q=0.8,en-US;q=0.6,en;q=0.4,de;q=0.2,es;q=0.2'
    CONNECTION = 'keep-alive'

    headers = {
        'User-Agent': USER_AGENT,
        'Accept': ACCEPT_HTML,
        'Accept-Encoding': ACCEPT_ENCODING,
        'Accept-Language': ACCEPT_LANGUAGE,
        'Referer': 'http://www.hsn.com/shop/home/ho',
        'Host': 'www.hsn.com'
    }

    product_headers = {
        'User-Agent': USER_AGENT,
        'Accept': ACCEPT_HTML,
        'Accept-Encoding': ACCEPT_ENCODING,
        'Accept-Language': ACCEPT_LANGUAGE,
        'Host': 'www.hsn.com'
    }

    img_headers = {
        'User-Agent': USER_AGENT,
        'Accept': ACCEPT_IMAGE,
        'Accept-Encoding': ACCEPT_ENCODING
    }

    prod_dir = r'D:\CrawledData\Products\Home & Garden'
    brands_dir = r'D:\CrawledData\Brands2'
    brands_check_dir = r'D:\CrawledData\Brands3'

    illegal_characters = {
        '/': '-',
        '\\': '-',
        '*': '.',
        ':': '-',
        '"': "'",
        '?': '.',
        '<': '.',
        '>': '.',
        '|': '-'
    }

    image = re.search('<a class="product-image" href="([^"]+)"', content)
    if not image:
        try:
            print('Wtf, no image ?! {}'.format(prod_name))
        except:
            pass
    else:
        img_headers['Referer'] = prod_link
        img_response = requests.get('http:' + image.group(1), headers=img_headers)
        extension = image.group(1).rsplit('.', 1)[1].strip()
        if not extension or len(extension) > 4:
            extension = 'noextension'
        prod_image = prod_file + '.{}'.format(extension)
        with open(prod_image, 'wb') as handle:
            for chunk in img_response.iter_content(1024):
                handle.write(chunk)



def main():

    USER_AGENT = 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.109 Safari/537.36'
    ACCEPT_HTML = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'
    ACCEPT_IMAGE = 'image/webp,image/*,*/*;q=0.8'
    ACCEPT_ENCODING = 'gzip, deflate, sdch'
    ACCEPT_ENCODING_2 = 'gzip, deflate'
    ACCEPT_LANGUAGE = 'ro-RO,ro;q=0.8,en-US;q=0.6,en;q=0.4,de;q=0.2,es;q=0.2'
    CONNECTION = 'keep-alive'

    headers = {
        'User-Agent': USER_AGENT,
        'Accept': ACCEPT_HTML,
        'Accept-Encoding': ACCEPT_ENCODING,
        'Accept-Language': ACCEPT_LANGUAGE,
        'Referer': 'http://www.hsn.com/shop/home/ho',
        'Host': 'www.hsn.com'
    }

    product_headers = {
        'User-Agent': USER_AGENT,
        'Accept': ACCEPT_HTML,
        'Accept-Encoding': ACCEPT_ENCODING,
        'Accept-Language': ACCEPT_LANGUAGE,
        'Host': 'www.hsn.com'
    }

    img_headers = {
        'User-Agent': USER_AGENT,
        'Accept': ACCEPT_IMAGE,
        'Accept-Encoding': ACCEPT_ENCODING
    }

    prod_dir = r'D:\CrawledData\Products\Home & Garden'
    brands_dir = r'D:\CrawledData\Brands2'
    brands_check_dir = r'D:\CrawledData\Brands3'

    illegal_characters = {
        '/': '-',
        '\\': '-',
        '*': '.',
        ':': '-',
        '"': "'",
        '?': '.',
        '<': '.',
        '>': '.',
        '|': '-'
    }

    categories = [
        # ('Bed & Bath', 'http://www.hsn.com/shop/bed-and-bath/ho0001?view=all'),
        # ('Bed & Bath', 'http://www.hsn.com/shop/bed-and-bath/ho0001?view=all&sort=&skip=58&take=58&page=2&scroll=false'),
        # ('Bed & Bath', 'http://www.hsn.com/shop/bed-and-bath/ho0001?view=all&sort=&skip=116&take=58&page=3&scroll=false'),
        # ('Bed & Bath', 'http://www.hsn.com/shop/bed-and-bath/ho0001?view=all&sort=&skip=174&take=59&page=4&scroll=false'),
        # ('Bed & Bath', 'http://www.hsn.com/shop/bed-and-bath/ho0001?view=all&sort=&skip=233&take=58&page=5&scroll=false'),
        # ('Bed & Bath', 'http://www.hsn.com/shop/bed-and-bath/ho0001?view=all&sort=&skip=291&take=58&page=6&scroll=false'),
        # ('Bed & Bath', 'http://www.hsn.com/shop/bed-and-bath/ho0001?view=all&sort=&skip=349&take=59&page=7&scroll=false'),
        # ('Bed & Bath', 'http://www.hsn.com/shop/bed-and-bath/ho0001?view=all&sort=&skip=408&take=58&page=8&scroll=false'),
        # ('Bed & Bath', 'http://www.hsn.com/shop/bed-and-bath/ho0001?view=all&sort=&skip=466&take=58&page=9&scroll=false'),
        # ('Bed & Bath', 'http://www.hsn.com/shop/bed-and-bath/ho0001?view=all&sort=&skip=524&take=58&page=10&scroll=false'),
        # ('Bed & Bath', 'http://www.hsn.com/shop/bed-and-bath/ho0001?view=all&sort=&skip=582&take=59&page=11&scroll=false'),
        # ('Bed & Bath', 'http://www.hsn.com/shop/bed-and-bath/ho0001?view=all&sort=&skip=641&take=58&page=12&scroll=false'),
        # ('Bed & Bath', 'http://www.hsn.com/shop/bed-and-bath/ho0001?view=all&sort=&skip=699&take=58&page=13&scroll=false'),
        # ('Bed & Bath', 'http://www.hsn.com/shop/bed-and-bath/ho0001?view=all&sort=&skip=757&take=59&page=14&scroll=false'),
        # ('Bed & Bath', 'http://www.hsn.com/shop/bed-and-bath/ho0001?view=all&sort=&skip=816&take=58&page=15&scroll=false'),
        # ('Bed & Bath', 'http://www.hsn.com/shop/bed-and-bath/ho0001?view=all&sort=&skip=874&take=58&page=16&scroll=false'),
        # ('Bed & Bath', 'http://www.hsn.com/shop/bed-and-bath/ho0001?view=all&sort=&skip=932&take=58&page=17&scroll=false'),
        # ('Bed & Bath', 'http://www.hsn.com/shop/bed-and-bath/ho0001?view=all&sort=&skip=990&take=59&page=18&scroll=false'),
        # ('Bed & Bath', 'http://www.hsn.com/shop/bed-and-bath/ho0001?view=all&sort=&skip=1049&take=35&page=19&scroll=false'),

        # ('Cleaning & Laundry', 'http://www.hsn.com/shop/cleaning-and-laundry/ho0440'),
        # ('Cleaning & Laundry', 'http://www.hsn.com/shop/cleaning-and-laundry/ho0440?view=all&sort=&skip=58&take=58&page=2&scroll=false'),
        # ('Cleaning & Laundry', 'http://www.hsn.com/shop/cleaning-and-laundry/ho0440?view=all&sort=&skip=116&take=58&page=3&scroll=false'),
        # ('Cleaning & Laundry', 'http://www.hsn.com/shop/cleaning-and-laundry/ho0440?view=all&sort=&skip=174&take=36&page=4&scroll=false'),

        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=60&take=60&page=2&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=120&take=60&page=3&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=180&take=60&page=4&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=240&take=60&page=5&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=300&take=60&page=6&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=360&take=60&page=7&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=420&take=60&page=8&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=480&take=60&page=9&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=540&take=60&page=10&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=600&take=60&page=11&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=660&take=60&page=12&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=720&take=60&page=13&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=780&take=60&page=14&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=840&take=60&page=15&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=900&take=60&page=16&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=960&take=60&page=17&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=1020&take=60&page=18&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=1080&take=60&page=19&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=1140&take=60&page=20&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=1200&take=60&page=21&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=1260&take=60&page=22&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=1320&take=60&page=23&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=1380&take=60&page=24&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=1440&take=60&page=25&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=1500&take=60&page=26&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=1560&take=60&page=27&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=1620&take=60&page=28&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=1680&take=60&page=29&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=1740&take=60&page=30&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=1800&take=60&page=31&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=1860&take=60&page=32&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=1920&take=60&page=33&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=1980&take=60&page=34&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=2040&take=60&page=35&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=2100&take=60&page=36&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=2160&take=60&page=37&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=2220&take=60&page=38&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=2280&take=60&page=39&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=2340&take=60&page=40&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=2400&take=60&page=41&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=2460&take=60&page=42&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=2520&take=60&page=43&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=2580&take=60&page=44&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=2640&take=60&page=45&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=2700&take=60&page=46&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=2760&take=60&page=47&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=2820&take=60&page=48&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=2880&take=60&page=49&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=2940&take=60&page=50&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=3000&take=60&page=51&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=3060&take=60&page=52&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=3120&take=60&page=53&scroll=false'),
        # ('Decor', 'http://www.hsn.com/shop/decor/ho0045?view=all&sort=&skip=3180&take=22&page=54&scroll=false'),

        # ('Furniture', 'http://www.hsn.com/shop/furniture/ho0115?view=all'),
        # ('Furniture', 'http://www.hsn.com/shop/furniture/ho0115?view=all&sort=&skip=60&take=60&page=2&scroll=false'),
        # ('Furniture', 'http://www.hsn.com/shop/furniture/ho0115?view=all&sort=&skip=120&take=60&page=3&scroll=false'),
        # ('Furniture', 'http://www.hsn.com/shop/furniture/ho0115?view=all&sort=&skip=180&take=60&page=4&scroll=false'),
        # ('Furniture', 'http://www.hsn.com/shop/furniture/ho0115?view=all&sort=&skip=240&take=60&page=5&scroll=false'),
        # ('Furniture', 'http://www.hsn.com/shop/furniture/ho0115?view=all&sort=&skip=300&take=60&page=6&scroll=false'),
        # ('Furniture', 'http://www.hsn.com/shop/furniture/ho0115?view=all&sort=&skip=360&take=60&page=7&scroll=false'),
        # ('Furniture', 'http://www.hsn.com/shop/furniture/ho0115?view=all&sort=&skip=420&take=60&page=8&scroll=false'),
        # ('Furniture', 'http://www.hsn.com/shop/furniture/ho0115?view=all&sort=&skip=480&take=60&page=9&scroll=false'),
        # ('Furniture', 'http://www.hsn.com/shop/furniture/ho0115?view=all&sort=&skip=540&take=60&page=10&scroll=false'),
        # ('Furniture', 'http://www.hsn.com/shop/furniture/ho0115?view=all&sort=&skip=600&take=60&page=11&scroll=false'),
        # ('Furniture', 'http://www.hsn.com/shop/furniture/ho0115?view=all&sort=&skip=660&take=60&page=12&scroll=false'),
        # ('Furniture', 'http://www.hsn.com/shop/furniture/ho0115?view=all&sort=&skip=720&take=60&page=13&scroll=false'),
        # ('Furniture', 'http://www.hsn.com/shop/furniture/ho0115?view=all&sort=&skip=780&take=60&page=14&scroll=false'),
        # ('Furniture', 'http://www.hsn.com/shop/furniture/ho0115?view=all&sort=&skip=840&take=60&page=15&scroll=false'),
        # ('Furniture', 'http://www.hsn.com/shop/furniture/ho0115?view=all&sort=&skip=900&take=60&page=16&scroll=false'),
        # ('Furniture', 'http://www.hsn.com/shop/furniture/ho0115?view=all&sort=&skip=960&take=60&page=17&scroll=false'),
        # ('Furniture', 'http://www.hsn.com/shop/furniture/ho0115?view=all&sort=&skip=1020&take=60&page=18&scroll=false'),
        # ('Furniture', 'http://www.hsn.com/shop/furniture/ho0115?view=all&sort=&skip=1080&take=60&page=19&scroll=false'),
        # ('Furniture', 'http://www.hsn.com/shop/furniture/ho0115?view=all&sort=&skip=1140&take=60&page=20&scroll=false'),
        # ('Furniture', 'http://www.hsn.com/shop/furniture/ho0115?view=all&sort=&skip=1200&take=60&page=21&scroll=false'),
        # ('Furniture', 'http://www.hsn.com/shop/furniture/ho0115?view=all&sort=&skip=1260&take=60&page=22&scroll=false'),
        # ('Furniture', 'http://www.hsn.com/shop/furniture/ho0115?view=all&sort=&skip=1320&take=60&page=23&scroll=false'),
        # ('Furniture', 'http://www.hsn.com/shop/furniture/ho0115?view=all&sort=&skip=1380&take=60&page=24&scroll=false'),
        # ('Furniture', 'http://www.hsn.com/shop/furniture/ho0115?view=all&sort=&skip=1440&take=60&page=25&scroll=false'),
        # ('Furniture', 'http://www.hsn.com/shop/furniture/ho0115?view=all&sort=&skip=1500&take=60&page=26&scroll=false'),
        # ('Furniture', 'http://www.hsn.com/shop/furniture/ho0115?view=all&sort=&skip=1560&take=60&page=27&scroll=false'),
        # ('Furniture', 'http://www.hsn.com/shop/furniture/ho0115?view=all&sort=&skip=1620&take=60&page=28&scroll=false'),
        # ('Furniture', 'http://www.hsn.com/shop/furniture/ho0115?view=all&sort=&skip=1680&take=60&page=29&scroll=false'),
        # ('Furniture', 'http://www.hsn.com/shop/furniture/ho0115?view=all&sort=&skip=1740&take=60&page=30&scroll=false'),
        # ('Furniture', 'http://www.hsn.com/shop/furniture/ho0115?view=all&sort=&skip=1800&take=60&page=31&scroll=false'),
        # ('Furniture', 'http://www.hsn.com/shop/furniture/ho0115?view=all&sort=&skip=1860&take=60&page=32&scroll=false'),
        # ('Furniture', 'http://www.hsn.com/shop/furniture/ho0115?view=all&sort=&skip=1920&take=60&page=33&scroll=false'),
        # ('Furniture', 'http://www.hsn.com/shop/furniture/ho0115?view=all&sort=&skip=1980&take=7&page=34&scroll=false'),

        # ('Home Appliances', 'http://www.hsn.com/shop/home-appliances/ho0209'),
        # ('Home Appliances', 'http://www.hsn.com/shop/home-appliances/ho0209?view=all&sort=&skip=58&take=58&page=2&scroll=false'),
        # ('Home Appliances', 'http://www.hsn.com/shop/home-appliances/ho0209?view=all&sort=&skip=116&take=58&page=3&scroll=false'),
        # ('Home Appliances', 'http://www.hsn.com/shop/home-appliances/ho0209?view=all&sort=&skip=174&take=59&page=4&scroll=false'),
        # ('Home Appliances', 'http://www.hsn.com/shop/home-appliances/ho0209?view=all&sort=&skip=233&take=58&page=5&scroll=false'),
        # ('Home Appliances', 'http://www.hsn.com/shop/home-appliances/ho0209?view=all&sort=&skip=291&take=58&page=6&scroll=false'),
        # ('Home Appliances', 'http://www.hsn.com/shop/home-appliances/ho0209?view=all&sort=&skip=349&take=59&page=7&scroll=false'),
        # ('Home Appliances', 'http://www.hsn.com/shop/home-appliances/ho0209?view=all&sort=&skip=408&take=58&page=8&scroll=false'),
        # ('Home Appliances', 'http://www.hsn.com/shop/home-appliances/ho0209?view=all&sort=&skip=466&take=58&page=9&scroll=false'),
        # ('Home Appliances', 'http://www.hsn.com/shop/home-appliances/ho0209?view=all&sort=&skip=524&take=50&page=10&scroll=false'),

        # ('Home Improvements', 'http://www.hsn.com/shop/home-improvement/ho0223'),
        # ('Home Improvements', 'http://www.hsn.com/shop/home-improvement/ho0223?view=all&sort=&skip=58&take=58&page=2&scroll=false'),
        # ('Home Improvements', 'http://www.hsn.com/shop/home-improvement/ho0223?view=all&sort=&skip=116&take=58&page=3&scroll=false'),
        # ('Home Improvements', 'http://www.hsn.com/shop/home-improvement/ho0223?view=all&sort=&skip=174&take=59&page=4&scroll=false'),
        # ('Home Improvements', 'http://www.hsn.com/shop/home-improvement/ho0223?view=all&sort=&skip=233&take=29&page=5&scroll=false'),

        # ('Luggage & Travel', 'http://www.hsn.com/shop/luggage-and-travel/ho0235?view=all'),
        # ('Luggage & Travel', 'http://www.hsn.com/shop/luggage-and-travel/ho0235?view=all&sort=&skip=58&take=58&page=2&scroll=false'),
        # ('Luggage & Travel', 'http://www.hsn.com/shop/luggage-and-travel/ho0235?view=all&sort=&skip=116&take=58&page=3&scroll=false'),
        # ('Luggage & Travel', 'http://www.hsn.com/shop/luggage-and-travel/ho0235?view=all&sort=&skip=174&take=59&page=4&scroll=false'),
        # ('Luggage & Travel', 'http://www.hsn.com/shop/luggage-and-travel/ho0235?view=all&sort=&skip=233&take=52&page=5&scroll=false'),

        # ('Outdoor', 'http://www.hsn.com/shop/outdoor/ho0248'),
        # ('Outdoor', 'http://www.hsn.com/shop/outdoor/ho0248?view=all&sort=&skip=58&take=58&page=2&scroll=false'),
        # ('Outdoor', 'http://www.hsn.com/shop/outdoor/ho0248?view=all&sort=&skip=116&take=58&page=3&scroll=false'),
        # ('Outdoor', 'http://www.hsn.com/shop/outdoor/ho0248?view=all&sort=&skip=174&take=59&page=4&scroll=false'),
        # ('Outdoor', 'http://www.hsn.com/shop/outdoor/ho0248?view=all&sort=&skip=233&take=58&page=5&scroll=false'),
        # ('Outdoor', 'http://www.hsn.com/shop/outdoor/ho0248?view=all&sort=&skip=291&take=58&page=6&scroll=false'),
        # ('Outdoor', 'http://www.hsn.com/shop/outdoor/ho0248?view=all&sort=&skip=349&take=59&page=7&scroll=false'),
        # ('Outdoor', 'http://www.hsn.com/shop/outdoor/ho0248?view=all&sort=&skip=408&take=58&page=8&scroll=false'),
        # ('Outdoor', 'http://www.hsn.com/shop/outdoor/ho0248?view=all&sort=&skip=466&take=58&page=9&scroll=false'),
        # ('Outdoor', 'http://www.hsn.com/shop/outdoor/ho0248?view=all&sort=&skip=524&take=58&page=10&scroll=false'),
        # ('Outdoor', 'http://www.hsn.com/shop/outdoor/ho0248?view=all&sort=&skip=582&take=59&page=11&scroll=false'),
        # ('Outdoor', 'http://www.hsn.com/shop/outdoor/ho0248?view=all&sort=&skip=641&take=58&page=12&scroll=false'),
        # ('Outdoor', 'http://www.hsn.com/shop/outdoor/ho0248?view=all&sort=&skip=699&take=58&page=13&scroll=false'),
        # ('Outdoor', 'http://www.hsn.com/shop/outdoor/ho0248?view=all&sort=&skip=757&take=59&page=14&scroll=false'),
        # ('Outdoor', 'http://www.hsn.com/shop/outdoor/ho0248?view=all&sort=&skip=816&take=58&page=15&scroll=false'),
        # ('Outdoor', 'http://www.hsn.com/shop/outdoor/ho0248?view=all&sort=&skip=874&take=18&page=16&scroll=false'),

        # ('Pet Care', 'http://www.hsn.com/shop/pet-care/ho0288'),
        # ('Pet Care', 'http://www.hsn.com/shop/pet-care/ho0288?view=all&sort=&skip=60&take=60&page=2&scroll=false'),
        # ('Pet Care', 'http://www.hsn.com/shop/pet-care/ho0288?view=all&sort=&skip=120&take=60&page=3&scroll=false'),
        # ('Pet Care', 'http://www.hsn.com/shop/pet-care/ho0288?view=all&sort=&skip=180&take=60&page=4&scroll=false'),
        # ('Pet Care', 'http://www.hsn.com/shop/pet-care/ho0288?view=all&sort=&skip=240&take=60&page=5&scroll=false'),
        # ('Pet Care', 'http://www.hsn.com/shop/pet-care/ho0288?view=all&sort=&skip=300&take=33&page=6&scroll=false'),

        # ('Seasonal', 'http://www.hsn.com/shop/seasonal/ho0393'),
        # ('Seasonal', 'http://www.hsn.com/shop/seasonal/ho0393?view=all&sort=&skip=60&take=37&page=2&scroll=false'),

        # ('Storage & Organization', 'http://www.hsn.com/shop/storage-and-organization/ho0299'),
        # ('Storage & Organization', 'http://www.hsn.com/shop/storage-and-organization/ho0299?view=all&sort=&skip=58&take=58&page=2&scroll=false'),
        # ('Storage & Organization', 'http://www.hsn.com/shop/storage-and-organization/ho0299?view=all&sort=&skip=116&take=58&page=3&scroll=false'),
        # ('Storage & Organization', 'http://www.hsn.com/shop/storage-and-organization/ho0299?view=all&sort=&skip=174&take=59&page=4&scroll=false'),
        # ('Storage & Organization', 'http://www.hsn.com/shop/storage-and-organization/ho0299?view=all&sort=&skip=233&take=58&page=5&scroll=false'),
        # ('Storage & Organization', 'http://www.hsn.com/shop/storage-and-organization/ho0299?view=all&sort=&skip=291&take=58&page=6&scroll=false'),
        # ('Storage & Organization', 'http://www.hsn.com/shop/storage-and-organization/ho0299?view=all&sort=&skip=349&take=59&page=7&scroll=false'),
        # ('Storage & Organization', 'http://www.hsn.com/shop/storage-and-organization/ho0299?view=all&sort=&skip=408&take=58&page=8&scroll=false'),
        # ('Storage & Organization', 'http://www.hsn.com/shop/storage-and-organization/ho0299?view=all&sort=&skip=466&take=18&page=9&scroll=false'),

    ]

    prod_counter = 10000
    brand_counter = 0
    for categ, link in categories:
        print('[info] Parsing category: {} - {}'.format(categ, link))
        categ_dir = os.path.join(prod_dir, categ)
        if not os.path.isdir(categ_dir):
            os.mkdir(categ_dir)
        response = requests.get(link, headers=headers).text
        product_headers['Referer'] = link
        products = re.findall('<div class="item product-item(?:.|\s)+?<a href="([^"]+)">([^<]+)<(?:.|\s)+? price">([^<]+)<', response)
        for product in products:
            try:
                print('[info] Parsing product: {} - {}'.format(product[1], 'http://www.hsn.com' + product[0]))
            except:
                try:
                    print('[info] Parsing product: ? - {}'.format('http://www.hsn.com' + product[0]))
                except:
                    try:
                        print('[info] Parsing product: {} - ?'.format(product[1]))
                    except:
                        print('[info] Parsing product: ? - ?')
            # getting product data
            info = {}
            prod_link = 'http://www.hsn.com' + product[0]
            prod_name = product[1]
            prod_price = int(float(product[2].replace('$', '').replace(',', '').split('-')[0].strip()))
            product_page = requests.get(prod_link, headers=product_headers).text
            description = re.search('<div class="content copy">(?:\s*<div>\s*)?((?:.|\s)*?)<\/div>', product_page)
            if description:
                description = description.group(1)
                replaceable = re.search('<[^>]+>.*Good to Know.*<[^>]+>', description)
                if replaceable:
                    description = description.replace(replaceable.group(0), '')
                description = description.strip()
            else:
                description = 'No description available for this product'
            info['description'] = description
            # description = re.search('<div class="content copy">(?:.|\s)*?(?:<br \/>\s*)(?:<\/strong>)?((?:.|\s)+?)(?:(?:<br \/>)|(?:<\/div>))', product_page)
            # if not description:
            #     info['description'] = 'No description available for this product'
            # else:
            #     info['description'] = description.group(1)
            brand = re.search('<span itemprop="brand" class="hidden">([^<]+)<\/span>', product_page)
            if not brand:
                try:
                    print('No brand for product: {}'.format(prod_name))
                except:
                    pass
                continue
            else:
                brand = brand.group(1)
            brand = brand.replace('&#39;', "'").replace('&amp;', '&')
            replaceable = re.search('&#\d+;', brand)
            while replaceable:
                brand = brand.replace(replaceable.group(0), '')
                replaceable = re.search('&#\d+;', brand)

            prod_name = prod_name.replace('&#39;', "'").replace('&amp;', '&')
            replaceable = re.search('&#\d+;', prod_name)
            while replaceable:
                prod_name = prod_name.replace(replaceable.group(0), '')
                replaceable = re.search('&#\d+;', prod_name)

            # writing product info file
            prod_file = os.path.join(categ_dir, 'product_{:05d}'.format(prod_counter))
            prod_counter += 1
            with open(prod_file, 'w') as handle:
                handle.write('Brand: {}\n'.format(brand))
                handle.write('Product: {}\n'.format(prod_name))
                handle.write('Filter: {}\n'.format(prod_price))
                handle.write('Info: {}\n'.format(info))

            # getting product image
            proc = Process(target=download_image, args=(product_page, prod_name, prod_file, prod_link))
            proc.start()
            proc.join(timeout=20)
            if proc.is_alive():
                print('Product image download killed !')
                proc.terminate()
                with open(prod_file + '.nophoto', 'w') as handle:
                     pass

            # treating product brand
            temp = brand
            for c in brand:
                if illegal_characters.get(c):
                    temp = temp.replace(c, illegal_characters[c])
            temp = temp.strip()
            brand_path = os.path.join(brands_dir, temp)
            brand_check_path = os.path.join(brands_check_dir, temp)
            with open(brand_check_path, 'w') as handle:
                pass
            if os.path.isdir(brand_path) and len(os.listdir(brand_path)) >= 3:
                # check if this brand has this category in its .categories file
                brand_listing = os.listdir(brand_path)
                for brand_file_x in brand_listing:
                    if brand_file_x.endswith('.categories'):
                        full_path_x = os.path.join(brand_path, brand_file_x)
                        with open(full_path_x, 'r') as handle:
                            content = handle.read().split('\n')
                        if 'Home & Garden' not in content:
                            try:
                                print('Brand with multiple categories ! {}'.format(brand))
                            except:
                                print('Brand with multiple categories !')
                            with open(full_path_x, 'a') as handle:
                                handle.write('Home & Garden\n')
                continue
            elif not os.path.isdir(brand_path):
                os.mkdir(brand_path)
            brand_name = os.path.join(brand_path, 'brand_{:05d}.name'.format(brand_counter))
            with open(brand_name, 'wb') as handle:
                handle.write((brand + '\n').encode(errors='replace'))
            brand_description = os.path.join(brand_path, 'brand_{:05d}.description'.format(brand_counter))
            description = wikipedia.search_term(brand)
            if not description:
                description = 'No description available for this company. You can supply a description by contacting us.'
            description = description + '\n'
            with open(brand_description, 'wb') as handle:
                handle.write(description.encode(errors='replace'))
            brand_categ = os.path.join(brand_path, 'brand_{:05d}.categories'.format(brand_counter))
            with open(brand_categ, 'w') as handle:
                handle.write('Home & Garden\n')
            brand_logo = os.path.join(brand_path, 'brand_{:05d}'.format(brand_counter))
            proc = Process(target=google_logos.download_icon, args=(brand_logo, brand))
            proc.start()
            proc.join(timeout=20)
            if proc.is_alive():
                print('Brand logo downloading killed !')
                proc.terminate()
            brand_counter += 1


if __name__ == '__main__':
    main()

import os
import re
import shutil
import time
import requests
import sys


USER_AGENT = 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.109 Safari/537.36'
ACCEPT_HTML = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'
ACCEPT_IMAGE = 'image/webp,image/*,*/*;q=0.8'
ACCEPT_ENCODING = 'gzip, deflate, sdch'
ACCEPT_ENCODING_2 = 'gzip, deflate'
ACCEPT_LANGUAGE = 'ro-RO,ro;q=0.8,en-US;q=0.6,en;q=0.4,de;q=0.2,es;q=0.2'
CONNECTION = 'keep-alive'
HOST = 'www.6pm.com'

headers = {
    'User-Agent': USER_AGENT,
    'Accept': ACCEPT_HTML,
    'Referer': 'http://www.6pm.com/brands'
}

if len(sys.argv) == 2 and sys.argv[1] == 'custom':
    brands = [
        ('/element', 'Element'),
        ('/emporio-armani', 'Emporio Armani'),
        ('/levis', "Levi's"),
        ('/oneill', "O'Neill"),
        ('/osiris', 'Osiris'),
        ('/quiksilver', 'Quiksilver'),
        ('/ray-ban', 'Ray-Ban'),
        ('/versace', 'Versace'),
        ('/vans', 'Vans'),
        ('/diesel', 'Diesel'),
        ('/dolce-gabbana', 'Dolce & Gabbana'),
        ('/b/ecco/brand/54', 'ECCO'),
        ('/converse', 'Converse')
    ]
    brands = [('http://www.6pm.com' + item[0], item[1]) for item in brands]
else:
    exit()
    # getting main page with brands
    response = requests.get('http://www.6pm.com/brands', headers=headers)
    brands = re.findall(r'<a href="([^"]+)" class="to-brand">\s*<strong>([^<]+)<', response.text)
    brands = [('http://www.6pm.com' + item[0].strip(), item[1].replace('&#32;', ' ')) for item in brands]

prod_dir = r'D:\Products\Clothing & Accessories'
brands_dir = r'D:\Brands4'

product_counter = 1
brand_counter = 1
for brand in brands:
    # adding new crawled brands
    brand_path = os.path.join(brands_dir, brand[1])
    if not os.path.isdir(brand_path):
        os.mkdir(brand_path)

    # adding brand name
    brand_name = os.path.join(brand_path, 'brand_{:05d}.name'.format(brand_counter))
    with open(brand_name, 'w') as handle:
        handle.write(brand[1] + '\n')

    # adding brand category
    brand_name = os.path.join(brand_path, 'brand_{:05d}.categories'.format(brand_counter))
    with open(brand_name, 'w') as handle:
        handle.write('Clothing & Accessories\n')

    # adding brand description
    brand_description = os.path.join(brand_path, 'brand_{:05d}.description'.format(brand_counter))
    brand_counter += 1
    response = requests.get(brand[0], headers=headers)
    description_text = re.search(r'div id="brandInfo"(?:.|\s)+?<p>\s*<img[^>]+>\s*((?:.|\s)+?)<\/p>', response.text).group(1)
    replaceable = re.search(r'<a class="zph"[^>]+>([^<])+<\/a>', description_text)
    while replaceable:
        description_text = description_text.replace(replaceable.group(0), replaceable.group(1), 1)
        replaceable = re.search(r'<a class="zph"[^>]+>([^<])+<\/a>', description_text)
    with open (brand_description, 'wb') as handle:
        handle.write(description_text.replace('<br><br>', '').replace('<b>', '').replace('</b>', '').replace('<i>', '').replace('</i>', '').encode(errors='replace'))

    # adding brand logo
    brand_logo = os.path.join(brand_path, 'brand_{:05d}'.format(brand_counter))
    brand_logo_link = re.search(r'background:url\(([^)]+)\)', response.text).group(1)
    img_headers = {}
    for key in headers:
        img_headers[key] = headers[key]
    img_headers['Accept'] = ACCEPT_IMAGE
    img_headers['Referer'] = brand[0]
    extension = brand_logo_link.rsplit('.', 1)[-1]
    brand_logo = brand_logo + '.' + extension
    img_response = requests.get(brand_logo_link, headers=img_headers)
    with open(brand_logo, 'wb') as handle:
        for chunk in img_response.iter_content(1024):
            handle.write(chunk)
    
    # crawling women categories
    match = re.search(r'title="[^"]+?Women.s[^"]*(.|\s)+?(?:(?:title="[^"]+?Men.s)|(?:title="))', response.text)
    if match:
        women_html = match.group(0)
        women_categories = [('http://www.6pm.com' + x[0], "Women's " + x[1].strip()) for x in re.findall(r'<li>(?:.|\s)+?href="([^"]+)"[^>]+?>([^(]+)\((?:.|\s)+?<ul class="levelTwo">', women_html)]
    else:
        women_categories = []
    for category in women_categories:
        new_headers = {}
        for key in headers:
           new_headers[key] = headers[key]
        new_headers['Referer'] = brand[0]
        new_headers['Host'] = 'www.6pm.com'
        prod_response = requests.get(category[0], headers=new_headers)
        products = re.findall(r'<img src="([^"]+)".+?itemprop="image"(?:.|\s)+?class="productName"[^>]+>([^<]+)<(?:.|\s)+?class="price[^>]+>([^<]+)<', prod_response.text)
        product_path = os.path.join(prod_dir, category[1])
        if not os.path.isdir(product_path):
            os.mkdir(product_path)

        # detail_response = requests.get()

        for item in products:
            # writing product info
            prod_file = os.path.join(product_path, 'product_{:05d}'.format(product_counter))
            product_counter += 1
            price = round(float(item[2].strip().replace('$', '').replace(',', '')))
            info_dict = {}
            with open(prod_file, 'w') as handle:
                handle.write('Brand: {}\n'.format(brand[1]))
                handle.write('Product: {}\n'.format(item[1].strip()))
                handle.write('Filter: {}\n'.format(price))
                handle.write('Info: {}\n'.format(info_dict))

            # downloading product icon
            img_headers = {}
            for key in headers:
                img_headers[key] = headers[key]
            img_headers['Accept'] = ACCEPT_IMAGE
            img_headers['Referer'] = category[0]
            img_response = requests.get(item[0], headers=img_headers)
            prod_icon = prod_file + '.' + item[0].rsplit('.', 1)[-1]
            with open(prod_icon, 'wb') as handle:
                for chunk in img_response.iter_content(1024):
                    handle.write(chunk)

    # crawling men categories
    match = re.search(r'title="[^"]+?Men.s[^"]*(.|\s)+?(?:title=")', response.text)
    if match:
        men_html = match.group(0)
        men_categories = [('http://www.6pm.com' + x[0], "Men's " + x[1].strip()) for x in re.findall(r'<li>(?:.|\s)+?href="([^"]+)"[^>]+?>([^(]+)\((?:.|\s)+?<ul class="levelTwo">', men_html)]
    else:
        men_categories = []            
    for category in men_categories:
        new_headers = {}
        for key in headers:
           new_headers[key] = headers[key]
        new_headers['Referer'] = brand[0]
        new_headers['Host'] = 'www.6pm.com'
        prod_response = requests.get(category[0], headers=new_headers)
        products = re.findall(r'<img src="([^"]+)".+?itemprop="image"(?:.|\s)+?class="productName"[^>]+>([^<]+)<(?:.|\s)+?class="price[^>]+>([^<]+)<', prod_response.text)
        product_path = os.path.join(prod_dir, category[1])
        if not os.path.isdir(product_path):
            os.mkdir(product_path)

        for item in products:
            # writing product info
            prod_file = os.path.join(product_path, 'product_{:05d}'.format(product_counter))
            product_counter += 1
            price = round(float(item[2].strip().replace('$', '').replace(',', '')))
            info_dict = {}
            with open(prod_file, 'w') as handle:
                handle.write('Brand: {}\n'.format(brand[1]))
                handle.write('Product: {}\n'.format(item[1].strip()))
                handle.write('Filter: {}\n'.format(price))
                handle.write('Info: {}\n'.format(info_dict))

            # downloading product icon
            img_headers = {}
            for key in headers:
                img_headers[key] = headers[key]
            img_headers['Accept'] = ACCEPT_IMAGE
            img_headers['Referer'] = category[0]
            img_response = requests.get(item[0], headers=img_headers)
            prod_icon = prod_file + '.' + item[0].rsplit('.', 1)[-1]
            with open(prod_icon, 'wb') as handle:
                for chunk in img_response.iter_content(1024):
                    handle.write(chunk)
    
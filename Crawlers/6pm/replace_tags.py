import os

brands_dir = r'D:\Brands'

for item in os.listdir(brands_dir):
    brand_path = os.path.join(brands_dir, item)
    for name in os.listdir(brand_path):
        if not name.endswith('.description'):
            continue
        brand_file = os.path.join(brand_path, name)
        with open(brand_file, 'rb') as handle:
            content = handle.read().decode()
        content = content.replace('<br>', '')
        with open(brand_file, 'wb') as handle:
            handle.write(content.encode(errors='replace'))
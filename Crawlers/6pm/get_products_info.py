import os
import re
import shutil
import time
import requests
import sys
from urllib.parse import urlencode


USER_AGENT = 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.109 Safari/537.36'
ACCEPT_HTML = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'
ACCEPT_IMAGE = 'image/webp,image/*,*/*;q=0.8'
ACCEPT_ENCODING = 'gzip, deflate, sdch'
ACCEPT_ENCODING_2 = 'gzip, deflate'
ACCEPT_LANGUAGE = 'ro-RO,ro;q=0.8,en-US;q=0.6,en;q=0.4,de;q=0.2,es;q=0.2'
CONNECTION = 'keep-alive'
HOST = 'www.6pm.com'

headers = {
    'User-Agent': USER_AGENT,
    'Accept': ACCEPT_HTML,
    'Accept-Encoding': ACCEPT_ENCODING,
    'Referer': 'http://www.6pm.com/',
    'Host': HOST
}

PROD_DIRS = r'D:\CrawledData\Products\Clothing & Accessories'

for item in os.listdir(PROD_DIRS):
    subdir = os.path.join(PROD_DIRS, item)
    for prod_file in os.listdir(subdir):
        if '.' in prod_file:
            continue
        print(prod_file)
        prod_path = os.path.join(subdir, prod_file)
        with open(prod_path, 'r') as handle:
            content = handle.read()
        content = [line.strip() for line in content.split('\n')]
        prod_name = content[1].replace('Product: ', '')
        prod_brand = content[0].replace('Brand: ', '')
        term = prod_name + ' ' + prod_brand
        query = {'term': term}
        print('http://www.6pm.com/search?department=&' + urlencode(query))
        search_response = requests.get('http://www.6pm.com/search?department=&' + urlencode(query), headers=headers)
        response_text = search_response.text
        product = re.search(r'<a href="([^"]+)"[^>]+>\s*<img[^>]+>\s*<span class="brandName"', response_text)
        if product:
            copy_headers = {}
            for key in headers:
                copy_headers[key] = headers[key]
            copy_headers['Referer'] = search_response.url
            prod_page = requests.get('http://www.6pm.com' + product.group(1), copy_headers)
            response_text = prod_page.text
        description = re.search(r'<div class="description" itemprop="description">((?:.|\s)+?)<\/div>', response_text)
        if description:
            description = description.group(1)
        else:
            description = 'No description available for this product'
        info = {'description': description}
        with open(prod_path + '.parsed', 'w') as handle:
            handle.write(content[0] + '\n')
            handle.write(content[1] + '\n')
            handle.write(content[2] + '\n')
            handle.write('Info: {}\n'.format(info))
        os.remove(prod_path)
        os.rename(prod_path + '.parsed', prod_path)

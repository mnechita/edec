import os
import re
import shutil
import time
import requests
import sys
from urllib.parse import urlencode
from html import unescape

USER_AGENT = 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.109 Safari/537.36'
ACCEPT_HTML = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'
ACCEPT_ENCODING = 'gzip, deflate, sdch'
ACCEPT_LANGUAGE = 'ro-RO,ro;q=0.8,en-US;q=0.6,en;q=0.4,de;q=0.2,es;q=0.2'
CONNECTION = 'keep-alive'
HOST = 'www.en.wikipedia.com'


def search_term(search_this):
    headers = {
        'User-Agent': USER_AGENT,
        'Accept': ACCEPT_HTML,
        'Accept-Encoding': ACCEPT_ENCODING,
        'Referer': 'https://en.wikipedia.org/wiki/Searching'
    }

    search_link = 'https://en.wikipedia.org/w/index.php?'

    # if len(sys.argv) != 2:
    #     exit()

    search_data = {
        'title': 'Special:Search',
        'go': 'Go'
    }
    search_data['search'] = search_this

    data = urlencode(search_data)
    link = search_link + data
    response = requests.get(link, headers=headers)

    print(response.url)

    if 'index.php' in response.url:
        return None
    else:
        match = re.search(r'(?:(?:<\/div>)|(?:<\/table>))\s*<p>((?:.|\s)+?)(?:(?:<div id=)|(?:<h2))', response.text)
        if match:
            content = match.group(1)
            matches = re.search('(?:<[^>]+>)|(?:\[[^\]]+\])|(?:&[^;]+;)', content)
            while matches:
                content = content.replace(matches.group(0), '', 1)
                matches = re.search('(?:<[^>]+>)|(?:\[[^\]]+\])|(?:&[^;]+;)', content)
            with open('result.txt', 'wb') as handle:
                handle.write(content.encode(errors='replace'))
            return content
        else:
            print('Expression did not match on: {}'.format(response.url))
            return None

search_term(sys.argv[1])
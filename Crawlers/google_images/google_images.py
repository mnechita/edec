import os
import re
import shutil
import time
import requests
import sys
from urllib.parse import urlencode
from html import unescape

USER_AGENT = 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.109 Safari/537.36'
ACCEPT_HTML = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'
ACCEPT_IMAGE = 'image/webp,image/*,*/*;q=0.8'
ACCEPT_ENCODING = 'gzip, deflate, sdch'
ACCEPT_LANGUAGE = 'ro-RO,ro;q=0.8,en-US;q=0.6,en;q=0.4,de;q=0.2,es;q=0.2'
CONNECTION = 'keep-alive'
HOST = 'www.en.wikipedia.com'

headers = {
    'User-Agent': USER_AGENT,
    'Accept': ACCEPT_HTML,
    'Accept-Encoding': ACCEPT_ENCODING,
    'Referer': 'https://en.wikipedia.org/wiki/Searching'
}

img_headers = {
    'User-Agent': USER_AGENT,
    'Accept': ACCEPT_IMAGE,
    'Accept-Encoding': ACCEPT_ENCODING
}

search_link = 'https://www.google.ro/search?q=python+requests+search+google&espv=2&source=lnms&tbm=isch'

def download_icon(icon_path, product, brand):
    values = {'q': '{} {}'.format(product, brand)}
    response = requests.get('https://www.google.ro/search?espv=2&source=lnms&tbm=isch&{}'.format(urlencode(values)), headers=headers)
    first_image = re.search(r'Rezultate(?:.|\s)+?"ou":"(https?:[^"]+)"', response.text)
    if first_image:
        first_image = first_image.group(1)
    else:
        return 
    try:
        img_response = requests.get(first_image, headers=img_headers)
    except:
        return
    extension = first_image.rsplit('.', 1)[-1]
    if '/' in extension or len(extension) > 4:
        print('Error in extension: {} - from product: {} - brand: {}'.format(extension, product, brand))
        extension = 'noextension'
    if extension == 'noextension':
        extension = img_response.url.rsplit('.', 1)[-1]
        if '/' in extension or len(extension) > 4:
            print('Error [second try] in extension: {} - from product: {} - brand: {}'.format(extension, product, brand))
            extension = 'noextension'
    if extension == 'noextension':
        extension = img_response.url.rsplit('.', 1)[-1].split('?', 1)[0]
        if '/' in extension or len(extension) > 4:
            print('Error [third try] in extension: {} - from product: {} - brand: {}'.format(extension, product, brand))
            extension = 'noextension'
    extension = extension.lower()
    icon_path = icon_path + '.' + extension
    with open(icon_path, 'wb') as handle:
        for chunk in img_response.iter_content(1024):
            handle.write(chunk)

download_icon('test_icon', sys.argv[1], sys.argv[2])
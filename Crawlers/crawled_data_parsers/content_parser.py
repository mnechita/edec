import os

PARSED_DIRS = [
    r'D:\CrawledData\Products\Clothing & Accessories',
    r'D:\CrawledData\Products\Electronics',
    #r'D:\CrawledData\Products\Food & Drinks',
    r'D:\CrawledData\Products\Home & Garden'
]

subdirs = []
for item in PARSED_DIRS:
	subdirs += [os.path.join(item, x) for x in os.listdir(item)]

for x in subdirs:
	for item in os.listdir(x):
		if '.' in item:
			continue
		full_path = os.path.join(x, item)
		with open(full_path, 'r') as handle:
			content = handle.read()
		if r'\xa0' not in content:
			continue
		print('Changing: {}'.format(full_path))
		content = content.replace(r'\xa0', ' ')

		with open(full_path + '.tmp', 'w') as handle:
			handle.write(content)
		os.remove(full_path)
		os.rename(full_path + '.tmp', full_path)
		
import os

# PARSE_DIR = r'D:\CrawledData\Products\Food & Drinks'
PARSE_DIR = r'D:\CrawledData\Brands'

bytes = {
    b'GIF': '.gif',
    b'PNG': '.png',
    b'JFIF': '.jpg',
    b'<?xml': '.svg'
}

for item in os.listdir(PARSE_DIR):
    subdir = os.path.join(PARSE_DIR, item)
    for entry in os.listdir(subdir):
        full_path = os.path.join(subdir, entry)
        if entry.endswith('.ashx'):
            print('Renaming: {}'.format(full_path))
            os.rename(full_path, full_path.replace('.ashx', '.png'))
            full_path = full_path.replace('.ashx', '.png')
        if entry.lower() != entry:
            full_path = os.path.join(subdir, entry.lower())
            os.rename(os.path.join(subdir, entry), full_path)
        if not entry.endswith('.noextension'):
            continue

        with open(full_path, 'rb') as handle:
            content = handle.read(10)
        flag = 0
        for ftype in bytes:
            if ftype in content:
                os.rename(full_path, full_path.replace('.noextension', bytes[ftype]))
                flag = 1
                break
        if flag:
            continue
        print('Removing: {}'.format(full_path))
        os.remove(full_path)
        with open(full_path.replace('.noextension', '.nophoto'), 'w'):
            pass
    
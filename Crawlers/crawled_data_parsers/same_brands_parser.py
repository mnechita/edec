import os

dir1 = r'D:\CrawledData\Brands2'
dir2 = r'D:\CrawledData\Brands'

first = [item.strip().lower().replace(' ', '') for item in os.listdir(dir1)]
second = [item.strip().lower().replace(' ', '') for item in os.listdir(dir2)]

for item in first:
    if item in second:
        print(item)
import os
import google_images
import google_logos
import wikipedia
import signal
from multiprocessing import Process

foods_dir = r'D:\Products\Food & Drinks'
brands_dir = r'D:\Brands'

illegal_characters = {
    '/': '-',
    '\\': '-',
    '*': '.',
    ':': '-',
    '"': "'",
    '?': '.',
    '<': '.',
    '>': '.',
    '|': '-'
}

def main():
    brand_counter = 1000
    for item in os.listdir(foods_dir):
        subcateg = os.path.join(foods_dir, item)
        listing = os.listdir(subcateg)
        print('[info] Parsing subcategory: {}'.format(item))
        for prod in listing:
            # see if product needs processing
            if '.' in prod:
                continue
            process_count = 0
            for xxx in listing:
                if xxx.startswith(prod):
                    process_count += 1
            if process_count >= 2:
                continue
            print('Passed: {}'.format(prod))

        	# parsing product (obtaining product image)
            prod_path = os.path.join(subcateg, prod)
            with open(prod_path, 'rb') as handle:
                content = handle.read().decode(errors='replace').split('\n')[:-2]
            content = [item.strip() for item in content]
            brand = content[0].replace('Brand: ', '')
            prod_name = content[1].replace('Product: ', '')
            # print('Here 1')
            # status = google_images.download_icon(prod_path, prod_name, brand)
            proc = Process(target=google_images.download_icon, args=(prod_path, prod_name, brand))
            proc.start()
            proc.join(timeout=20)
            # print('Here 2')
            if proc.is_alive():
                print('Product icon downloading killed !')
                proc.terminate()
                status = 1
            else:
                status = proc.exitcode
            if status:
                with open(prod_path + '.nophoto', 'w') as handle:
                     pass

            # parsing brand
            temp = brand
            for c in brand:
                if illegal_characters.get(c):
                    temp = temp.replace(c, illegal_characters[c])
            brand_path = os.path.join(brands_dir, temp)
            if os.path.isdir(brand_path) and len(os.listdir(brand_path)) >= 3:
                # check if this brand has this category in its .categories file
                brand_listing = os.listdir(brand_path)
                for brand_file_x in brand_listing:
                    if brand_file_x.endswith('.categories'):
                        full_path_x = os.path.join(brand_path, brand_file_x)
                        with open(full_path_x, 'r') as handle:
                            content = handle.read().split('\n')
                        if 'Food & Drinks' not in content:
                            print('Brand with multiple categories ! {}'.format(brand))
                            with open(full_path_x, 'w') as handle:
                                handle.write('Food & Drinks\n')
                continue
            elif not os.path.isdir(brand_path):
            	os.mkdir(brand_path)
            print('New brand: {}'.format(brand))
            brand_name = os.path.join(brand_path, 'brand_{:05d}.name'.format(brand_counter))
            with open(brand_name, 'wb') as handle:
            	handle.write((brand + '\n').encode(errors='replace'))
            brand_description = os.path.join(brand_path, 'brand_{:05d}.description'.format(brand_counter))
            # print('Here 3')
            description = wikipedia.search_term(brand)
            # print('Here 4')
            if not description:
            	description = 'No description available for this company. You can supply a description by contacting us.'
            description = description + '\n'
            with open(brand_description, 'wb') as handle:
            	handle.write(description.encode(errors='replace'))
            brand_categ = os.path.join(brand_path, 'brand_{:05d}.categories'.format(brand_counter))
            with open(brand_categ, 'w') as handle:
            	handle.write('Food & Drinks\n')
            brand_logo = os.path.join(brand_path, 'brand_{:05d}'.format(brand_counter))
            # print('Here 5')
            # google_logos.download_icon(brand_logo, brand)
            proc = Process(target=google_logos.download_icon, args=(brand_logo, brand))
            proc.start()
            proc.join(timeout=20)
            # print('Here 6')
            if proc.is_alive():
                print('Brand logo downloading killed !')
                proc.terminate()
            brand_counter += 1


if __name__ == '__main__':
    main()
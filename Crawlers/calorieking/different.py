import os

PROD_DIR = r'D:\XPathCrawlers\CalorieKingProducts'

all_cars = ['calories', 'kilojoules']

for item in os.listdir(PROD_DIR):
    item_path = os.path.join(PROD_DIR, item)
    with open(item_path, 'r') as handle:
        content = handle.read()
        content = content.split('\n')[5:]
        for elem in content:
            if not elem:
                continue
            car = elem.split('-')[0].strip().replace('.', '').replace(' ', '_').lower()
            if car not in all_cars:
                all_cars.append(car)
                print(item_path, car)
print(all_cars)
import os
import requests
import lxml.html
from lxml import etree

USER_AGENT = 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.109 Safari/537.36'
ACCEPT_HTML = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'
ACCEPT_ENCODING = 'gzip, deflate, sdch'
ACCEPT_ENCODING_2 = 'gzip, deflate'
ACCEPT_LANGUAGE = 'ro-RO,ro;q=0.8,en-US;q=0.6,en;q=0.4,de;q=0.2,es;q=0.2'
CONNECTION = 'keep-alive'
HOST = 'www.calorieking.com'

# new session
session = requests.Session()

# getting main page with food categories
headers = {
    'User-Agent': USER_AGENT,
    'Accept': ACCEPT_HTML,
    'Accept-Encoding': ACCEPT_ENCODING,
    'Accept-Language': ACCEPT_LANGUAGE,
    'Connection': CONNECTION,
    'Host': HOST,
    'Upgrade-Insecure-Requests': '1'
}
response = session.get('http://www.calorieking.com/foods/', headers=headers)
root = lxml.html.fromstring(response.text)
subcategories = root.find_class('subcategory')

# getting brands and products each food category
headers = {
    'User-Agent': USER_AGENT,
    'Accept': ACCEPT_HTML,
    'Accept-Encoding': ACCEPT_ENCODING,
    'Accept-Language': ACCEPT_LANGUAGE,
    'Connection': CONNECTION,
    'Host': HOST,
    'Upgrade-Insecure-Requests': '1',
    'Referer': 'http://www.calorieking.com/foods/'
}
headers_prod_info = {
    'User-Agent': USER_AGENT,
    'Accept': ACCEPT_HTML,
    'Accept-Encoding': ACCEPT_ENCODING_2,
    'Accept-Language': ACCEPT_LANGUAGE,
    'Connection': CONNECTION,
    'Host': HOST,
    'Origin': 'http://www.calorieking.com',
    'Content-Type': 'application/x-www-form-urlencoded',
    'Upgrade-Insecure-Requests': '1',
}
counter = 0
info_dir = r'D:\XPathCrawlers\CalorieKingProducts'
for item in subcategories:
    categ = item.xpath('a[@href]')[0]
    categ_name = categ.text
    print('Parsing category: {}'.format(categ_name))
    response = session.get(categ.attrib['href'], headers=headers)
    new_root = lxml.html.fromstring(response.text)
    brands = new_root.find_class('result')
    for brand in brands:
        brand_name = brand.xpath('h3')[0].text
        if brand_name.startswith('- '):
            continue
        products = brand.xpath('div/ul/li/a[@href]')
        print('\nProducts for brand: {}'.format(brand_name))
        for product in products:
            print(' - Product: {}'.format(product.text))
            counter += 1
            prod_info_file = os.path.join(info_dir, 'product_{:05d}'.format(counter))
            handle = open(prod_info_file, 'w')
            handle.write('Categ: {}\n'.format(categ_name))
            handle.write('Brand: {}\n'.format(brand_name))
            handle.write('Product: {}\n'.format(product.text))
            product_link = product.attrib['href']
            headers_prod_info['Referer'] = product_link
            product_page = session.get(product_link, headers=headers_prod_info)
            units_root = lxml.html.fromstring(product_page.text)
            units = units_root.xpath('//div[@id="serving-controls"]/select/option[text()="mL" or text()="g"]/@value')
            u_name = units_root.xpath('//div[@id="serving-controls"]/select/option[text()="mL" or text()="g"]/text()')
            if not units:
                units = units_root.xpath('//div[@id="serving-controls"]/select/option[@value="0"]/@value')
                u_name = units_root.xpath('//div[@id="serving-controls"]/select/option[@value="0"]/text()')
            units = units[0]
            u_name = u_name[0]
            if u_name in ['g', 'mL']:
                amount = 100
            else:
                amount = 1
            post_data = 'amount={}&units={}&recalc='.format(amount, units)
            handle.write('Info for {} x {}:\n'.format(amount, u_name))
            response_info = session.post(product_link, headers=headers_prod_info, data=post_data)
            info_root = lxml.html.fromstring(response_info.text)
            entries = info_root.xpath('//table/tr[@class!="daily-value-header" and count(td/span)=2]')
            for ent in entries:
                data_prod = ent.xpath('td/span')
                handle.write('Calories: {} - Kilojoules: {}\n'.format(data_prod[0].text, data_prod[1].text))
            entries = info_root.xpath('//table/tr[@class and count(td)=3]')
            for ent in entries:
                data_prod = ent.xpath('td')
                if data_prod[2].text.strip():
                    handle.write('{} - {} - {}\n'.format(data_prod[0].text, data_prod[1].text, data_prod[2].text))
                else:
                    handle.write('{} - {}\n'.format(data_prod[0].text, data_prod[1].text, data_prod[2].text))


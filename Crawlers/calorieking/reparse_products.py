import os

prod_dir = r'D:\XPathCrawlers\CalorieKingProducts'
result_dir = r'D:\Products\Food & Drinks'

for item in os.listdir(prod_dir):
    full_path = os.path.join(prod_dir, item)
    with open(full_path, 'r') as handle:
        lines = handle.read()
    lines = lines.split('\n')
    category = lines[0].replace('Categ: ', '').replace('/', ' or ').replace(':', ' -')
    categ_dir = os.path.join(result_dir, category)
    if not os.path.isdir(categ_dir):
        os.mkdir(categ_dir)
    brand = lines[1]
    product = lines[2]
    info_for = lines[3].split(':')[0].replace('Info for ', '')
    calories = lines[4].split('-')[0].strip().replace('Calories: ', '')
    info_dict = {'info_for': info_for}
    lines = lines[:-1]
    for x in lines[5:]:
        tokens = [xxx.strip() for xxx in x.split(' - ')]
        name = tokens[0].lower().replace(' ', '_').replace('.', '')
        if 'mg' in tokens[1]:
            value = int(float(tokens[1].replace('< ', '').replace('mg', ''))/1000)
        elif 'g' in tokens[1]:
            value = int(float(tokens[1].replace('< ', '').replace('g', '')))
        else:
            print('WTF!? {}'.format(tokens[1]))
            continue
        percentual = None
        if len(tokens) == 3:
            percentual = int(tokens[2].replace('< ', '').replace('%', ''))
        info_dict[name] = (value, percentual)
    info_path = os.path.join(categ_dir, item)
    with open(info_path, 'w') as handle:
        handle.write(brand + '\n')
        handle.write(product + '\n')
        handle.write('Filter: {}\n'.format(calories))
        handle.write('Info: {}\n'.format(info_dict) + '\n')
    
    
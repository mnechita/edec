set serveroutput on;
clear screen;

CREATE OR REPLACE PACKAGE csv_exporting
AS
  default_directory VARCHAR2(260) := 'D:\Work\sgbd\export';
  
  PROCEDURE create_table_csv (
    p_table_name  VARCHAR2,
    p_directory   VARCHAR2 := default_directory
  );
  
  PROCEDURE create_full_csv (
    p_directory  VARCHAR2 := default_directory
  );
  
  PROCEDURE update_table_from_csv (
    p_table_name  VARCHAR2,
    p_directory   VARCHAR2 := default_directory
  );
END csv_exporting;
/

CREATE OR REPLACE PACKAGE BODY csv_exporting
AS
  PROCEDURE create_table_csv (
    p_table_name  VARCHAR2,
    p_directory   VARCHAR2 := default_directory
  ) AS
    TYPE      t_string_table IS TABLE OF VARCHAR2(3000);
    CURSOR    c_columns IS SELECT column_name FROM all_tab_cols WHERE table_name = p_table_name ORDER BY column_id;
    c_cln         sys_refcursor;
    v_query       VARCHAR2(3000);
    v_results     t_string_table;
    v_file_handle UTL_FILE.FILE_TYPE;
  BEGIN
    EXECUTE IMMEDIATE 'CREATE OR REPLACE DIRECTORY export_dir AS ''' || p_directory || '''';
    v_file_handle := UTL_FILE.FOPEN('EXPORT_DIR', p_table_name || '.csv', 'w');
    v_query := 'SELECT ';
    FOR v_line IN c_columns LOOP
      v_query := v_query || 'DECODE(a.' || v_line.column_name || ', NULL, ''NULL'', TO_CHAR('''''''' || a.' || v_line.column_name || ' || '''''''')) || '','' || ';
    END LOOP;
    v_query := SUBSTR(v_query, 0, LENGTH(v_query) - LENGTH(' || '','' || ')) || ' FROM ' || p_table_name || ' a';
    
    OPEN c_cln FOR v_query;
    LOOP
      FETCH c_cln BULK COLLECT INTO v_results;
      FOR i IN 1..v_results.count LOOP
        UTL_FILE.PUT_LINE(v_file_handle, v_results(i));
      END LOOP;
      EXIT WHEN c_cln%NOTFOUND;
    END LOOP;
    CLOSE c_cln;
    UTL_FILE.FCLOSE(v_file_handle);
  END create_table_csv;
  
  PROCEDURE create_full_csv (
    p_directory  VARCHAR2 := default_directory
  ) AS
  BEGIN
    FOR oline IN (SELECT object_name FROM user_objects WHERE object_type = 'TABLE' AND object_name LIKE 'EDEC%' AND object_name NOT LIKE 'EDEC_METRICS%' ORDER BY last_ddl_time) LOOP
      create_table_csv(oline.object_name, p_directory);
    END LOOP;
  END create_full_csv;
  
  PROCEDURE update_table_from_csv (
    p_table_name  VARCHAR2,
    p_directory   VARCHAR2 := default_directory
  ) AS
    v_file_handle     UTL_FILE.FILE_TYPE;
    v_arguments       VARCHAR2(3000);
  BEGIN
    EXECUTE IMMEDIATE 'CREATE OR REPLACE DIRECTORY import_dir AS ''' || p_directory || '''';
    v_file_handle := UTL_FILE.FOPEN('IMPORT_DIR', p_table_name || '.csv', 'r');
    LOOP
      BEGIN
        UTL_FILE.GET_LINE(v_file_handle, v_arguments);
        DBMS_OUTPUT.PUT_LINE('INSERT INTO ' || p_table_name || ' VALUES(' || v_arguments || ')');
        EXECUTE IMMEDIATE 'INSERT INTO ' || p_table_name || ' VALUES(' || v_arguments || ')';
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          EXIT;
      END;
    END LOOP;
    UTL_FILE.FCLOSE(v_file_handle);
  END update_table_from_csv;    

END csv_exporting;
/

--BEGIN
--  csv_exporting.create_full_csv;
--END;
--/

--BEGIN
--  csv_exporting.create_table_csv('EDEC_COMPANY');
--  csv_exporting.create_table_csv('EDEC_CATEGORY');
--END;
--/

--INSERT INTO edec_category VALUES (NULL, 'ada');

--INSERT INTO edec_company VALUES (NULL, 'adfasdfsadadffa', 'aasfsafdfasdfasasfasf');
--DELETE FROM edec_category;

--DESC edec_trait;
--DESC edec_category;
--SELECt * FROM edec_category;
----INSERT INTO edec_category VALUES (NULL, 'adsfsadfdasassdfasfsadfasff');
--INSERT INTO edec_trait VALUES (NULL, 'fadsf', 'asdfsafdsafafaf', 1);
--INSERT INTO edec_trait VALUES (NULL, 'fadsf', 'asdfsafdsafafaf', 1);
--INSERT INTO edec_trait VALUES (NULL, 'fadsf', 'asdfsafdsafafaf', 2);
--INSERT INTO edec_trait VALUES (NULL, 'fadsf', 'asdfsafdsafafaf', 3);
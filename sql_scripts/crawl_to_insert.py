# from sys import argv
# from os.path import join, exists, basename
# from os import listdir, mkdir
# from shutil import copyfile
#
# from time import sleep
# import traceback
# import json
# import csv
# import re
#
# products_folder = r'..\Crawlers\CrawledData\Products'
# brands_folder = r'..\Crawlers\CrawledData\Brands'
# static_brands_img = r'D:\work\tw\project\edec\server\static\img\company'
# static_products_img = r'D:\work\tw\project\edec\server\static\img\product'
# countries_csv = r'countries-20140629.csv'
# cities_csv = r'worldcitiespop.txt'
#
#
# def parse_brands():
#     index = 1
#     brands_ids = {}
#     with open('_brands.pseudosql', 'wb') as fd:
#         for brand_name in listdir(brands_folder):
#             brand_dir = join(brands_folder, brand_name)
#             brand_name = ''
#             brand_description = ''
#             extension = ''
#             for element in listdir(brand_dir):
#                 if element.endswith('.description'):
#                     with open(join(brand_dir, element), 'rb') as fd2:
#                         brand_description = fd2.read().replace('\n', r'\n').replace('\'', '\'\'')
#                     continue
#                 if element.endswith('.name'):
#                     with open(join(brand_dir, element), 'rb') as fd2:
#                         brand_name = fd2.readline().strip().replace('\'', '\'\'')
#                     continue
#                 if element.endswith('.categories') or element.endswith('.nophoto'):
#                     continue
#
#                 image_path = join(brand_dir, element)
#                 image_basename = basename(image_path)
#                 extension = image_basename.split('.')[-1] if '.' in image_basename else 'unk'
#                 dst_img_dir = join(static_brands_img, '%02d' % (index % 100))
#                 if not exists(dst_img_dir):
#                     mkdir(dst_img_dir)
#                 dst_img_path = join(dst_img_dir, '%d.%s' % (index, extension))
#                 if not exists(dst_img_path):
#                     copyfile(image_path, dst_img_path)
#
#             if not brand_name:
#                 continue
#             fd.write('{}\n{}\n{}\n{}\n'.format(index, brand_name, brand_description, extension))
#             brands_ids[brand_name] = index
#             index += 1
#     with open('__brands.json', 'wb') as fd:
#         json.dump(brands_ids, fd)
#
#
# def parse_products(company_ids, category_ids, subcategory_ids):
#     index = 1
#     with open('_products.pseudosql', 'wb') as fd:
#         for category in listdir(products_folder):
#             cleaned_category = category.replace('\'', '\'\'')
#             category_dir = join(products_folder, category)
#             for subcategory in listdir(category_dir):
#                 cleaned_subcategory = subcategory.replace('\'', '\'\'')
#                 subcategory_dir = join(category_dir, subcategory)
#                 elements = listdir(subcategory_dir)
#                 for element in filter(lambda a:re.match(r'^product_\d{5}$', a), elements):
#                     product_name = ''
#                     product_brand = ''
#                     product_token = None
#                     product_info = ''
#
#                     with open(join(subcategory_dir, element), 'r+') as fd2:
#                         content = filter(lambda a:a.strip(), fd2.readlines())
#                         if len(content) != 4:
#                             continue
#
#                         product_brand = content[0][7:].strip().replace('&#39;', '\'').replace('\'','\'\'').replace('\xae', '').replace('\xe9', '')
#                         if product_brand == 'Liora Mann':
#                             product_brand = 'Liora Manne'
#                         product_name = content[1][9:].strip().replace('\'','\'\'')
#                         product_token = content[2][8:].strip().replace('\'','\'\'')
#
#                         cleaned = {}
#                         for k, v in eval(content[3][6:].strip()).items():
#                             if isinstance(k, str):
#                                 k = k.decode(errors='replace')
#                             if isinstance(v, str):
#                                 v = v.decode(errors='replace')
#                             cleaned[k] = v
#                         product_info = json.dumps(cleaned).replace('\'','\'\'')
#
#                     if product_brand not in company_ids:
#                         print 'Could not find this prodcut\'s company: ' + join(subcategory_dir, element)
#                         continue
#
#                     if not product_name:
#                         print 'Could not get this product\'s name: ' + join(subcategory_dir, element)
#                         continue
#
#                     if not product_token:
#                         print 'Could not get this product\'s token: ' + join(subcategory_dir, element)
#                         continue
#
#                     if not product_info:
#                         print 'Could not get this product\'s info: ' + join(subcategory_dir, element)
#                         continue
#
#                     image_path = ''
#                     for image_file in elements:
#                         if image_file.startswith(element) and image_file != element:
#                             image_path = join(subcategory_dir, image_file)
#                             break
#
#                     if image_path:
#                         image_basename = basename(image_path)
#                         extension = image_basename.split('.')[-1] if '.' in image_basename else 'unk'
#                         dst_img_dir = join(static_products_img, '%02d' % (index%100))
#                         if not exists(dst_img_dir):
#                             mkdir(dst_img_dir)
#                         dst_img_path = join(dst_img_dir, '%d.%s' % (index, extension))
#                         if not exists(dst_img_path):
#                             copyfile(image_path, dst_img_path)
#                     else:
#                         extension = ''
#
#                     fd.write('{}\n{}\n{}\n{}\n{}\n{}\n{}\n{}\n'.format(index, product_name, product_info, product_token, category_ids[cleaned_category], company_ids[product_brand], subcategory_ids[cleaned_subcategory], extension))
#                     index += 1
#
# def parse_categories():
#     index = 1
#     category_ids = {}
#     with open('_categories.pseudosql', 'wb') as fd:
#         for category in listdir(products_folder):
#             if category.startswith('Food'):
#                 index = 1
#             elif category.startswith('Cloth'):
#                 index = 2
#             elif category.startswith('Ele'):
#                 index = 3
#             elif category.startswith('Hom'):
#                 index = 4
#             else:
#                 continue
#             category = category.replace('\'', '\'\'')
#             fd.write('{}\n{}\n'.format(index, category))
#             category_ids[category] = index
#             index += 1
#     with open('__categories.json', 'wb') as fd:
#         json.dump(category_ids, fd)
#
#
# def parse_subcategories(category_ids):
#     index = 1
#     subcategory_ids = {}
#     with open('_subcategories.pseudosql', 'wb') as fd:
#         for category in listdir(products_folder):
#             cleaned_category = category.replace('\'', '\'\'')
#             category_dir = join(products_folder, category)
#             for subcategory in listdir(category_dir):
#                 subcategory = subcategory.replace('\'', '\'\'')
#                 if cleaned_category not in category_ids:
#                     print 'Nu gasesc Categoria pentru Subcategoria ' + join(category_dir, subcategory)
#
#                 fd.write('{}\n{}\n{}\n'.format(index, subcategory, category_ids[cleaned_category]))
#                 subcategory_ids[subcategory] = index
#                 index += 1
#     with open('__subcategories.json', 'wb') as fd:
#         json.dump(subcategory_ids, fd)
#
#
# def parse_countries():
#     country_ids = {}
#     index = 1
#     with open(countries_csv, 'rb') as fdr, open('_countries.pseudosql', 'wb') as fdw:
#         for entry in list(csv.reader(fdr))[1:]:
#             country_code = entry[0].upper().replace('\'','\'\'')
#             country_name = entry[1].replace('\'', '\'\'')
#
#             fdw.write('{}\n{}\n{}\n'.format(index, country_code, country_name))
#             country_ids[country_code] = index
#             index += 1
#     with open('__countries.json', 'wb') as fd:
#         json.dump(country_ids, fd)
#
#
# def parse_cities(country_ids):
#     index = 1
#     with open(cities_csv, 'rb') as fdr, open('_cities.pseudosql', 'wb') as fdw:
#         for entry in list(csv.reader(fdr))[1:]:
#             city_country_code = entry[0].upper().replace('\'','\'\'')
#             city_name = entry[1].title().replace('\'','\'\'')
#             city_locale_name = entry[2].replace('\'','\'\'')
#             city_region = entry[3]
#             city_population = entry[4]
#             city_latitude = entry[5]
#             city_longitude = entry[6]
#
#             if city_country_code not in country_ids:
#                 continue
#
#             fdw.write('{}\n{}\n{}\n{}\n{}\n{}\n{}\n'.format(index, city_name, city_latitude, city_longitude, country_ids[city_country_code], city_locale_name, city_population))
#             index += 1
#
#             # print 'Code: ' + city_country_code
#             # print 'Name: ' + city_name
#             # print 'LocaleName: ' + city_locale_name
#             # print 'Region: ' + city_region
#             # print 'Population: ' + city_population
#             # print 'Latitude: ' + city_latitude
#             # print 'Longitude: ' + city_longitude
#             # sleep(1)
#
# if __name__ == '__main__':
#     if len(argv) == 2 and argv[1] == 'brands':
#         parse_brands()
#         exit(0)
#
#     if len(argv) == 2 and argv[1] == 'categories':
#         parse_categories()
#         exit(0)
#
#     if len(argv) == 2 and argv[1] == 'countries':
#         parse_countries()
#         exit(0)
#
#     if len(argv) == 3 and argv[1] == 'cities':
#         try:
#             with open(argv[2], 'rb') as fd1:
#                 country_ids = json.load(fd1)
#                 parse_cities(country_ids)
#         except:
#             traceback.print_exc()
#         else:
#             exit(0)
#
#     if len(argv) == 3 and argv[1] == 'subcategories':
#         try:
#             with open(argv[2], 'rb') as fd1:
#                 category_ids = json.load(fd1)
#                 parse_subcategories(category_ids)
#         except:
#             traceback.print_exc()
#         else:
#             exit(0)
#
#
#     if len(argv) == 5 and argv[1] == 'products':
#         try:
#             with open(argv[2], 'rb') as fd1, open(argv[3], 'rb') as fd2, open(argv[4], 'rb') as fd3:
#                 brands_ids = json.load(fd1)
#                 category_ids = json.load(fd2)
#                 subcategory_ids = json.load(fd3)
#                 parse_products(brands_ids, category_ids, subcategory_ids)
#         except:
#             traceback.print_exc()
#         else:
#             exit(0)
#
#     print '\nUsage: \n' \
#           'crawl_to_insert.py countries\n' \
#           'crawl_to_insert.py cities <country_json_id_path>\n' \
#           'crawl_to_insert.py brands\n' \
#           'crawl_to_insert.py categories\n' \
#           'crawl_to_insert.py subcategories <category_json_id_path>\n' \
#           'crawl_to_insert.py products <brand_json_id_path> <category_json_id_path> <subcategory_json_id_path>\n'

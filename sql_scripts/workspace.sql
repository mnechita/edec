--select NVL(b.score, 0) from (
--  select id product_id
--  from product_product
--  where id = 72000
--) a
--left outer join (
--  select positive_score - negative_score as score, product_id
--  from metrics_userproduct
--  where user_id = 500 and product_id = 72000
--) b
--on a.product_id = b.product_id;

select * from product_product pp
left outer join metrics_userproduct up
on pp.id = up.product_id
order by NVL(up.positive_score - up.negative_score, 0) desc;

select * from metrics_userproduct;

clear screen;
desc edec_account_userprofile;
set pagesize 200;
set linesize 200;

select * from user_objects;

select * from edec_account_country;
select * from edec_account_city;
select * from edec_account_userprofile;

select * from product_category;
select * from company_company;
select * from product_product;

select * from product_trait;
select * from company_ideology;

select * from company_approval;
select * from company_compliance;

select * from product_producttrait;
select * from product_preference;

select * from metrics_useruser;
select * from metrics_usercompany;
select * from metrics_userproduct;

select * from metrics_companycompany;
select * from metrics_productproduct;

-- selecteaza produse si trait-uri pentru a vedea natural legaturi
select p.id, p.name, t.id, t.name
from product_product p
join product_producttrait pt on p.id = pt.product_id
join product_trait t on t.id = pt.trait_id
where p.id = 21970 or p.id = 21315
order by p.id;

-- selecteaza produsele in ordinea asemanarii cu produsul cu id-ul 21970
select 21970 prod1, p.id prod2, nvl(score, 0) from product_product p
left outer join metrics_productproduct pp on p.id = pp.product1_id and pp.product2_id = 21970 or p.id = pp.product2_id and pp.product1_id = 21970
order by nvl(score, 0) desc;
--order by pp.score desc;


-- selecteaza userii in ordinea preferintei pentru produsul cu id-ul 21970
select u.id, u.first_name, nvl(pp.positive_score - pp.negative_score, 0) score, pp.negative_score, pp.positive_score
from edec_account_userprofile u
left outer join metrics_userproduct pp on pp.user_id = u.id and pp.product_id = 21970
order by nvl(pp.positive_score - pp.negative_score, 0) desc;

select unique score from metrics_productproduct order by score desc;
select * from metrics_companycompany order by score desc;
select * from metrics_usercompany order by positive_score - negative_score desc;
select * from metrics_userproduct order by positive_score - negative_score asc;
select * from metrics_useruser order by approval_score + preference_score - abs(approval_score - preference_score)/2 desc;

create or replace procedure add_user
as
  v_idx         integer;
  v_city_id     edec_account_city.id%TYPE;
  v_country_id  edec_account_country.id%TYPE;
begin
  select count(*) + 1 into v_idx from edec_account_userprofile;
  insert into auth_user(id, is_superuser, is_staff, is_active, date_joined)
  values (v_idx, 1, 1, 1, systimestamp);
--  select id, country_id into v_city_id, v_country_id from (select id, country_id from edec_account_city order by dbms_random.value) where rownum < 2;
  select id, country_id into v_city_id, v_country_id from edec_account_city sample(1) where rownum < 2;
  insert into edec_account_userprofile(id, auth_id, birth_date, gender, income, city_id, country_id)
  values (NULL, v_idx, to_date(trunc(dbms_random.value(to_char(date '1950-01-01', 'J'), to_char(date '1989-12-31', 'J'))), 'J'), mod(v_idx, 2), dbms_random.value(10, 100), v_city_id, v_country_id);
end;
/

create or replace procedure add_country
as
  v_idx     integer;
begin
  select count(*) + 1 into v_idx from edec_account_country;
  insert into edec_account_country(id, code, name)
  values (NULL, MOD(v_idx, 100), 'Country' || v_idx);
end;
/

create or replace procedure add_city
as
  v_idx     integer;
  v_c_id    edec_account_country.id%TYPE;
begin
  select count(*) + 1 into v_idx from edec_account_city;
  select id into v_c_id from (select id from edec_account_country order by dbms_random.value) where rownum < 2;
  insert into edec_account_city(id, name, country_id, latitude, longitude)
  values (NULL, 'City' || v_idx, v_c_id, dbms_random.value(-90, 90), dbms_random.value(-180, 180));
end;
/

create or replace procedure add_category
as
  v_idx     integer;
begin
  select count(*) + 1 into v_idx from product_category;
  insert into product_category(id, name)
  values (NULL, 'Category' || v_idx);
end;
/

create or replace procedure add_subcategory
as
  v_idx     integer;
  v_c_id    product_category.id%TYPE;
begin
  select count(*) + 1 into v_idx from product_subcategory;
  select id into v_c_id from (select id from product_category order by dbms_random.value) where rownum < 2;
  insert into product_subcategory(id, name, category_id)
  values (NULL, 'Subcategory' || v_idx, v_c_id);
end;
/

create or replace procedure add_company
as
  v_idx     integer;
begin
  select count(*) + 1 into v_idx from company_company;
  insert into company_company(id, name, description)
  values (NULL, 'Company' || v_idx, 'Descriere pentru compania ' || v_idx || '.');
end;
/

create or replace procedure add_product
as
  v_idx         integer;
  v_cat_id      product_category.id%TYPE;
  v_comp_id     company_company.id%TYPE;
  v_subcat_id   product_subcategory.id%TYPE;
begin
  select count(*) + 1 into v_idx from product_product;
  select id into v_cat_id from (select id from product_category order by dbms_random.value) where rownum < 2;
  select id into v_comp_id from (select id from company_company order by dbms_random.value) where rownum < 2;
  select id into v_subcat_id from (select id from product_subcategory where category_id = v_cat_id order by dbms_random.value) where rownum < 2;
  insert into product_product(id, name, description, category_id, company_id, subcategory_id, token)
  values (NULL, 'Product' || v_idx, 'Descriere stufoasa si interesanta care reuseste sa atraga clienti pentru produsul ' || v_idx || '.', v_cat_id, v_comp_id, v_subcat_id, dbms_random.value * 1000);
end;
/

create or replace procedure add_trait
as
  v_idx     integer;
  v_cat_id  product_category.id%TYPE;
begin
  select count(*) + 1 into v_idx from product_trait;
  select id into v_cat_id from (select id from product_category order by dbms_random.value) where rownum < 2;
  insert into product_trait(id, name, description, category_id)
  values (NULL, 'Trait' || v_idx, 'Descriere pentru trait-ul ' || v_idx || '.', v_cat_id);
end;
/

create or replace procedure add_ideology
as
  v_idx     integer;
begin
  select count(*) + 1 into v_idx from company_ideology;
  insert into company_ideology(id, name, description)
  values (NULL, 'Ideologia' || v_idx, 'Descriere pentru ideologia ' || v_idx || '.');
end;
/

create or replace procedure add_producttrait
as
  v_p_id      product_product.id%type;
  v_t_id      product_trait.id%type;
  v_c_id      product_category.id%type;
  v_flag      integer;
  v_infinity  integer  := 9000;
begin
  for j in 1..v_infinity loop
--    select id, category_id into v_p_id, v_c_id from (select id, category_id from product_product order by dbms_random.value) where rownum < 2;
    select id, category_id into v_p_id, v_c_id from (select id, category_id from product_product where mod(id, 1000) = 0 order by dbms_random.value) where rownum < 2;
--    v_p_id := 12511;
    select id into v_t_id from (select id from product_trait where category_id = v_c_id order by dbms_random.value) where rownum < 2;
    select count(*) into v_flag from product_producttrait where product_id = v_p_id and trait_id = v_t_id;
    continue when v_flag > 0;
    insert into product_producttrait(id, product_id, trait_id) values (NULL, v_p_id, v_t_id);
--    dbms_output.put_line('Linked product ' || v_p_id || ' with trait ' || v_t_id);
    exit;
  end loop;
end;
/

create or replace procedure add_compliance
as
  v_c_id      company_company.id%type;
  v_i_id      company_ideology.id%type;
  v_flag      integer;
  v_infinity  integer  := 9000;
begin
  for j in 1..v_infinity loop
--    select id into v_c_id from (select id from company_company order by dbms_random.value) where rownum < 2;
    select id into v_c_id from (select id from company_company where mod(id, 100) = 0 order by dbms_random.value) where rownum < 2;
--    v_c_id := 1155;
    select id into v_i_id from (select id from company_ideology order by dbms_random.value) where rownum < 2;
    select count(*) into v_flag from company_compliance where company_id = v_c_id and ideology_id = v_i_id;
    continue when v_flag > 0;
    insert into company_compliance(id, company_id, ideology_id) values (NULL, v_c_id, v_i_id);
--    dbms_output.put_line('Linked company ' || v_c_id || ' with ideology ' || v_i_id);
    exit;
  end loop;
end;
/

create or replace procedure add_approval
as
  v_u_id      edec_account_userprofile.id%type;
  v_i_id      company_ideology.id%type;
  v_approve   integer;
  v_flag      integer;
  v_infinity  integer  := 9000;
begin
  for j in 1..v_infinity loop
    select id into v_u_id from (select id from edec_account_userprofile order by dbms_random.value) where rownum < 2;
    select id into v_i_id from (select id from company_ideology order by dbms_random.value) where rownum < 2;
    v_approve := round(dbms_random.value);
    select count(*) into v_flag from company_approval where user_id = v_u_id and ideology_id = v_i_id;
    continue when v_flag > 0;
    insert into company_approval(id, user_id, ideology_id, approval)
    values (NULL, v_u_id, v_i_id, v_approve);
    exit;
  end loop;
end;
/

create or replace procedure add_preference
as
  v_u_id      edec_account_userprofile.id%type;
  v_t_id      product_trait.id%type;
  v_prefer    integer;
  v_flag      integer;
  v_infinity  integer  := 9000;
begin
  for j in 1..v_infinity loop
    select id into v_u_id from (select id from edec_account_userprofile order by dbms_random.value) where rownum < 2;
    select id into v_t_id from (select id from product_trait order by dbms_random.value) where rownum < 2;
    v_prefer := round(dbms_random.value);
    select count(*) into v_flag from product_preference where user_id = v_u_id and trait_id = v_t_id;
    continue when v_flag > 0;
    insert into product_preference(id, user_id, trait_id, preference)
    values (NULL, v_u_id, v_t_id, v_prefer);
    exit;
  end loop;
end;
/

create or replace procedure clear_tables
as
  TYPE t_table_names IS TABLE OF VARCHAR2(50);
  v_tables    t_table_names;
  v_tmp       integer;
begin
  v_tables := t_table_names(
    'metrics_useruser',
    'metrics_usercompany',
    'metrics_userproduct',
    'metrics_companycompany',
    'metrics_productproduct',
    'company_approval',
    'company_compliance',
    'product_preference',
    'product_productTRAIT',
    'edec_account_userprofile',
    'product_product',
    'company_company',
    'company_ideology',
    'product_trait',
    'product_subcategory',
    'product_category'
  );
  for i in 1..v_tables.count loop
    dbms_output.put_line(v_tables(i));
    loop
      dbms_output.put('.');
      execute immediate 'delete from ' || v_tables(i) || ' where rownum < 100000';
      execute immediate 'select count(*) from ' || v_tables(i) into v_tmp;
      commit;
      exit when v_tmp = 0;
    end loop;
    commit;
  end loop;
end;
/

create or replace procedure tables_overview
as
  table_name    user_objects.object_name%TYPE;
  table_size    integer;
begin
  for line in (select object_name from user_objects where object_type = 'TABLE') loop
    table_name := line.object_name;
    execute immediate 'select count(*) from ' || line.object_name into table_size;
    dbms_output.put_line(rpad(table_name, 26) || TO_CHAR(table_size, '999,999,999'));
  end loop;
end;
/

select id from product_product where description like '%egg%' and description like '%protein%';
--select id from product_product where contains(description, '%egg%', 1) and contains(description, '%protein%', 1);
select id from product_product where lower(description) like '%egg%' and lower(description) like '%protein%';
select * from product_preference where rownum < 2;

select * from edec_account_userprofile;
select * from product_product where id = 1137;

begin
--  clear_tables;
--  tables_overview;
  for i in 1..15 loop
--    add_country;
    add_city;
--    add_category;
--    add_subcategory;
--    add_company;
--    add_product;
--    add_trait;
--    add_producttrait;
--    add_user;
--    add_ideology;
--    add_compliance;
--    add_approval;
--    add_preference;
    if mod(i, 5) = 0 then
      commit;
    end if;
  end loop;
  commit;
end;
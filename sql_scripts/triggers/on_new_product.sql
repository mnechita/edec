create or replace procedure on_new_product(p_id in product_product.id%TYPE, p_category_id in product_product.category_id%TYPE)
as
  cursor c_produse is
    select id from product_product
    where id != p_id and category_id = p_category_id;
  type ids is table of product_product.id%TYPE;
  id_products ids;

  cursor c_useri is
    select id from edec_account_userprofile;
  type iduseri is table of edec_account_userprofile.id%TYPE;
  id_useri iduseri;
begin
  open c_produse;
  loop
    fetch c_produse bulk collect into id_products limit 10000;
    for i in 1..id_products.count
    loop
      insert into metrics_productproduct(id, product1_id, product2_id, score) values (NULL, id_products(i), p_id, 0);
    end loop;
    exit when c_produse%NOTFOUND;
  end loop;
  close c_produse;

  open c_useri;
  loop
    fetch c_useri bulk collect into id_useri limit 10000;
    for i in 1..id_useri.count
    loop
      insert into metrics_userproduct(id, product_id, user_id, negative_score, positive_score) values (NULL, p_id, id_useri(i), 0, 0);
    end loop;
    exit when c_useri%NOTFOUND;
  end loop;
  close c_useri;
end;
/

--create or replace TRIGGER "PRODUCT_PRODUCT_TR"
--BEFORE INSERT ON "PRODUCT_PRODUCT"
--FOR EACH ROW
--WHEN (new."ID" IS NULL)
--    BEGIN
--        SELECT "PRODUCT_PRODUCT_SQ".nextval
--        INTO :new."ID" FROM dual;
--        on_new_product(:new."ID", :new."CATEGORY_ID");
--    END;
--/


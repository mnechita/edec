CREATE OR REPLACE PROCEDURE on_new_approval (
  p_user_id   company_approval.user_id%TYPE,
  p_idea_id   company_approval.ideology_id%type,
  p_approval  company_approval.approval%TYPE
) AS
  TYPE t_companies      IS TABLE OF company_company.id%TYPE;
  TYPE t_users          IS TABLE OF edec_account_userprofile.id%TYPE;
  CURSOR c_company      IS SELECT id FROM company_company;
  CURSOR c_user         IS SELECT id FROM edec_account_userprofile WHERE id != p_user_id;
  v_bucket              t_companies;
  v_bucket_users        t_users;
  v_user_approvals      INTEGER;
  v_common_approvals    INTEGER;
  v_company_total       INTEGER;
  v_score               INTEGER;
  v_tmp                 INTEGER;
  v_tmp2                INTEGER;
  v_score2              INTEGER;
  v_other_user_total    INTEGER;
  v_lower_user_id       INTEGER;
  v_greater_user_id     INTEGER;
BEGIN
  SELECT COUNT(*) + 1 INTO v_user_approvals
  FROM company_approval
  WHERE user_id = p_user_id AND approval = p_approval;
  OPEN c_company;
  LOOP
    FETCH c_company BULK COLLECT INTO v_bucket LIMIT 10000;
    FOR i IN 1..v_bucket.count LOOP
      SELECT COUNT(*) INTO v_company_total
      FROM company_compliance WHERE company_id = v_bucket(i);
      CONTINUE WHEN v_company_total = 0;

      SELECT COUNT(*) INTO v_common_approvals
      FROM company_approval ap
      JOIN company_compliance co ON ap.ideology_id = co.ideology_id
      WHERE ap.approval = p_approval AND ap.user_id = p_user_id AND co.company_id = v_bucket(i);

      SELECT COUNT(*) INTO v_tmp
      FROM company_compliance WHERE company_id = v_bucket(i) AND ideology_id = p_idea_id;
      v_common_approvals := v_common_approvals + v_tmp;

      v_score := 2000 * v_common_approvals * (3 * v_company_total + 2 * v_user_approvals) / (v_user_approvals * v_company_total);
      CONTINUE WHEN v_score = 0;

      IF p_approval = 1 THEN
        SELECT COUNT(*) INTO v_tmp
        FROM metrics_usercompany
        WHERE user_id = p_user_id AND company_id = v_bucket(i);

        IF v_tmp > 0 THEN
          UPDATE metrics_usercompany SET positive_score = v_score
          WHERE user_id = p_user_id AND company_id = v_bucket(i);
        ELSE
          INSERT INTO metrics_usercompany(id, user_id, company_id, negative_score, positive_score)
          VALUES (NULL, p_user_id, v_bucket(i), 0, v_score);
        END IF;
      ELSE
        SELECT COUNT(*) INTO v_tmp
        FROM metrics_usercompany
        WHERE user_id = p_user_id AND company_id = v_bucket(i);

        IF v_tmp > 0 THEN
          UPDATE metrics_usercompany SET negative_score = v_score
          WHERE user_id = p_user_id AND company_id = v_bucket(i);
        ELSE
          INSERT INTO metrics_usercompany(id, user_id, company_id, negative_score, positive_score)
          VALUES (NULL, p_user_id, v_bucket(i), v_score, 0);
        END IF;
      END IF;
    END LOOP;
    EXIT WHEN c_company%NOTFOUND;
  END LOOP;
  CLOSE c_company;

  SELECT COUNT(*) + 1 INTO v_user_approvals
  FROM company_approval
  WHERE user_id = p_user_id;
  OPEN c_user;
  LOOP
    FETCH c_user BULK COLLECT INTO v_bucket_users LIMIT 10000;
    FOR i IN 1..v_bucket_users.count LOOP
      SELECT COUNT(*) INTO v_other_user_total
      FROM company_approval WHERE user_id = v_bucket_users(i);
      CONTINUE WHEN v_other_user_total = 0;

      SELECT COUNT(*) INTO v_common_approvals
      FROM company_approval ap1
      JOIN company_approval ap2 ON ap1.ideology_id = ap2.ideology_id
      WHERE p_user_id = ap1.user_id AND ap2.user_id = v_bucket_users(i) AND ap1.approval = ap2.approval;

      SELECT COUNT(*) INTO v_tmp
      FROM company_approval WHERE user_id = v_bucket_users(i) AND approval = p_approval AND ideology_id = p_idea_id;
      v_common_approvals := v_common_approvals + v_tmp;

      v_score := 5000 * v_common_approvals * (v_user_approvals + v_other_user_total) / (v_user_approvals * v_other_user_total);

      -- cu toate ca folosesc aceeasi variabila, semantica nu va mai fi aceeasi (v_common_approvals va fi de fapt v_different_approvals)
      SELECT COUNT(*) INTO v_common_approvals
      FROM company_approval ap1
      JOIN company_approval ap2 ON ap1.ideology_id = ap2.ideology_id
      WHERE p_user_id = ap1.user_id AND ap2.user_id = v_bucket_users(i) AND ap1.approval != ap2.approval;

      SELECT COUNT(*) INTO v_tmp
      FROM company_approval WHERE user_id = v_bucket_users(i) AND approval != p_approval AND ideology_id = p_idea_id;
      v_common_approvals := v_common_approvals + v_tmp;

      v_score := v_score - 5000 * v_common_approvals * (v_user_approvals + v_other_user_total) / (v_user_approvals * v_other_user_total);

      IF p_user_id < v_bucket_users(i) THEN
        v_lower_user_id := p_user_id;
        v_greater_user_id := v_bucket_users(i);
      ELSE
        v_lower_user_id := v_bucket_users(i);
        v_greater_user_id := p_user_id;
      END IF;

      IF v_score = 0 THEN
        BEGIN
          SELECT score, preference_score INTO v_tmp, v_tmp2
          FROM metrics_useruser
          WHERE user1_id = v_lower_user_id AND user2_id = v_greater_user_id;
        EXCEPTION
          WHEN no_data_found THEN
            CONTINUE;
        END;

        IF v_tmp = 0 AND v_tmp2 = 0 THEN
          DELETE FROM metrics_useruser
          WHERE user1_id = v_lower_user_id AND user2_id = v_greater_user_id;
        ELSE
          UPDATE metrics_useruser SET approval_score = 0
          WHERE user1_id = v_lower_user_id AND user2_id = v_greater_user_id;
        END IF;

        CONTINUE;
      END IF;

      SELECT COUNT(*) INTO v_tmp
      FROM metrics_useruser
      WHERE user1_id = v_lower_user_id AND user2_id = v_greater_user_id;

      IF v_tmp = 0 THEN
        INSERT INTO metrics_useruser(id, user1_id, user2_id, score, approval_score, preference_score)
        VALUES (NULL, v_lower_user_id, v_greater_user_id, 0, v_score, 0);
      ELSE
        UPDATE metrics_useruser SET approval_score = v_score
        WHERE user1_id = v_lower_user_id AND user2_id = v_greater_user_id;
      END IF;
    END LOOP;
    EXIT WHEN c_user%NOTFOUND;
  END LOOP;
  CLOSE c_user;
END;
/

create or replace TRIGGER "COMPANY_APPROVAL_TR"
BEFORE INSERT ON "COMPANY_APPROVAL"
FOR EACH ROW
WHEN (new."ID" IS NULL)
    BEGIN
        SELECT "COMPANY_APPROVAL_SQ".nextval
        INTO :new."ID" FROM dual;
        on_new_approval(:new.user_id, :new.ideology_id, :new.approval);
    END;
/

CREATE OR REPLACE TRIGGER company_approval_tr2
before update on company_approval
for each row
begin
  on_new_approval(:new.user_id, :new.ideology_id, :new.approval);
end;
/
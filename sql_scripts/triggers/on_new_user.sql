create or replace procedure on_new_user(p_id_user in edec_account_userprofile.id%TYPE)
as
  cursor c_produse    is select id from product_product;
  cursor c_users      is select id from edec_account_userprofile;
  cursor c_companies  is select id from company_company;

  type ids            is table of product_product.id%TYPE;
  type t_user_vec     is table of edec_account_userprofile.id%type;
  type t_company_vec  is table of company_company.id%type;

  id_products     ids;
  v_id_users      t_user_vec;
  v_id_companies  t_company_vec;
begin
  open c_produse;
  loop
    fetch c_produse bulk collect into id_products limit 10000;
    for i in 1..id_products.count
    loop
      insert into metrics_userproduct(id, product_id, user_id, negative_score, positive_score) values (NULL, id_products(i), p_id_user, 0, 0);
    end loop;
    exit when c_produse%NOTFOUND;
  end loop;
  close c_produse;

  open c_users;
  loop
    fetch c_users bulk collect into v_id_users limit 10000;
    for i in 1..v_id_users.count loop
      insert into metrics_useruser(id, user1_id, user2_id, score, approval_score, preference_score) values (NULL, v_id_users(i), p_id_user, 0, 0, 0);
    end loop;
    exit when c_users%NOTFOUND;
  end loop;
  close c_users;

  open c_companies;
  loop
    fetch c_companies bulk collect into v_id_companies limit 10000;
    for i in 1..v_id_companies.count loop
      insert into metrics_usercompany(id, company_id, user_id, negative_score, positive_score) values (NULL, v_id_companies(i), p_id_user, 0, 0);
    end loop;
    exit when c_companies%NOTFOUND;
  end loop;
  close c_companies;
end;
/

--create or replace TRIGGER "edec_account_userprofile_TR"
--BEFORE INSERT ON "edec_account_userprofile"
--FOR EACH ROW
--WHEN (new."ID" IS NULL)
--    BEGIN
--        SELECT "edec_account_userprofile_SQ".nextval
--        INTO :new."ID" FROM dual;
--        on_update_user(p_id, p_birth_date, p_gender, p_income, p_city_id, p_country_id);
----        dbms_output.put_line('on_new_user');
----        on_new_user(:new."ID");
--    END;
--/
CREATE OR REPLACE PROCEDURE on_new_company (
  p_id  company_company.id%TYPE
) AS
  TYPE      t_companies IS TABLE OF company_company.id%TYPE;
  TYPE      t_users     IS TABLE OF edec_account_userprofile.id%TYPE;
  CURSOR    c_company   IS SELECT id FROM company_company;
  CURSOR    c_user      IS SELECT id FROM edec_account_userprofile;
  v_bucket  t_companies;
  v_user_bucket t_users;
BEGIN
  OPEN c_company;
  LOOP
    FETCH c_company BULK COLLECT INTO v_bucket LIMIT 10000;
    FOR i IN 1..v_bucket.count LOOP
      INSERT INTO metrics_companycompany(id, company1_id, company2_id, score) VALUES (NULL, v_bucket(i), p_id, 0);
    END LOOP;
    EXIT WHEN c_company%NOTFOUND;
  END LOOP;
  CLOSE c_company;

  OPEN c_user;
  LOOP
    FETCH c_user BULK COLLECT INTO v_user_bucket LIMIT 10000;
    FOR i IN 1..v_user_bucket.count LOOP
      INSERT INTO metrics_usercompany(id, company_id, user_id, negative_score, positive_score) VALUES (NULL, p_id, v_user_bucket(i), 0, 0);
    END LOOP;
    EXIT WHEN c_user%NOTFOUND;
  END LOOP;
  CLOSE c_user;
END;
/

--create or replace TRIGGER "COMPANY_COMPANY_TR"
--BEFORE INSERT ON "COMPANY_COMPANY"
--FOR EACH ROW
--WHEN (new."ID" IS NULL)
--    BEGIN
--        SELECT "COMPANY_COMPANY_SQ".nextval
--        INTO :new."ID" FROM dual;
----        on_new_company(:new."ID");
--    END;
--/
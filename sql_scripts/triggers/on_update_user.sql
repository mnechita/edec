CREATE OR REPLACE FUNCTION geographic_distance (
  p_lat1  NUMBER,
  p_long1 NUMBER,
  p_lat2  NUMBER,
  p_long2 NUMBER
) RETURN NUMBER AS
BEGIN
  RETURN sdo_geom.sdo_distance(sdo_geometry(2001, 4326, null, sdo_elem_info_array(1, 1, 1), sdo_ordinate_array(p_long1, p_lat1)),
                        sdo_geometry(2001, 4326, null, sdo_elem_info_array(1, 1, 1), sdo_ordinate_array(p_long2, p_lat2)),
                        1, 'unit=KM');
END;
/

CREATE OR REPLACE PROCEDURE on_update_user (
  p_id          edec_account_userprofile.id%TYPE,
  p_birth_date  edec_account_userprofile.birth_date%TYPE,
  p_gender      edec_account_userprofile.gender%TYPE,
  p_income      edec_account_userprofile.income%TYPE,
  p_city_id     edec_account_userprofile.city_id%TYPE,
  p_country_id  edec_account_userprofile.country_id%TYPE
) AS
  c_maximum_distance        CONSTANT NUMBER := 1000;     -- in kilometers
  c_maximum_generation_span CONSTANT NUMBER := 30 * 365;  -- in days
  c_maximum_income_dif      CONSTANT NUMBER := 10000;
  c_coef_age                CONSTANT NUMBER := 30;
  c_coef_gender             CONSTANT NUMBER := 5;
  c_coef_income             CONSTANT NUMBER := 15;
  c_coef_location           CONSTANT NUMBER := 50;

  CURSOR c_users      IS SELECT * FROM edec_account_userprofile WHERE id != p_id;
  TYPE t_users        IS TABLE OF edec_account_userprofile%ROWTYPE;

  v_bucket_users      t_users;
  v_distance          NUMBER;
  v_user              edec_account_userprofile%ROWTYPE;
  v_flag_age          INTEGER;
  v_flag_gender       INTEGER;
  v_flag_income       INTEGER;
  v_flag_location     INTEGER;
  v_score             INTEGER;
  v_tmp               INTEGER;
  v_lower_id          edec_account_userprofile.id%TYPE;
  v_greater_id        edec_account_userprofile.id%TYPE;
  v_city1_lat         edec_account_city.latitude%TYPE;
  v_city2_lat         edec_account_city.latitude%TYPE;
  v_city1_long        edec_account_city.longitude%TYPE;
  v_city2_long        edec_account_city.longitude%TYPE;
BEGIN
  OPEN c_users;
  LOOP
    FETCH c_users BULK COLLECT INTO v_bucket_users LIMIT 10000;
    FOR i in 1..v_bucket_users.count LOOP
      v_user            := v_bucket_users(i);
      v_score           := 0;
      v_flag_age        := 0;
      v_flag_gender     := 0;
      v_flag_income     := 0;
      v_flag_location   := 0;

      IF p_birth_date IS NOT NULL AND v_user.birth_date IS NOT NULL THEN
        v_flag_age := 1;
        v_tmp := ABS(v_user.birth_date - p_birth_date);
        IF v_tmp > c_maximum_generation_span THEN
          v_tmp := c_maximum_generation_span;
        END IF;

        v_score := v_score + c_coef_age * (10000 - v_tmp * 10000 / c_maximum_generation_span);
        dbms_output.put_line('Age:' || (10000 - v_tmp * 10000 / c_maximum_generation_span));
      END IF;

      IF p_gender IS NOT NULL AND v_user.gender IS NOT NULL THEN
        v_flag_gender := 1;
        IF p_gender = v_user.gender THEN
          v_tmp := 1;
        ELSE
          v_tmp := 0;
        END IF;

        v_score := v_score + c_coef_gender * v_tmp * 10000;
        dbms_output.put_line('Gender:' || (v_tmp * 10000));
      END IF;

      IF p_income IS NOT NULL AND v_user.income IS NOT NULL THEN
        v_flag_income := 1;
        v_tmp := ABS(p_income - v_user.income);
        IF v_tmp > c_maximum_income_dif THEN
          v_tmp := c_maximum_income_dif;
        END IF;

        v_score := v_score + c_coef_income * v_tmp * 10000 / c_maximum_income_dif;
        dbms_output.put_line('Income:' || (v_tmp * 10000));
      END IF;

      IF p_city_id IS NOT NULL AND v_user.city_id IS NOT NULL THEN
        v_flag_location := 1;
        SELECT latitude, longitude INTO v_city1_lat, v_city1_long FROM edec_account_city WHERE id = p_city_id;
        SELECT latitude, longitude INTO v_city2_lat, v_city2_long FROM edec_account_city WHERE id = v_user.city_id;
        v_tmp := geographic_distance(v_city1_lat, v_city1_long, v_city2_lat, v_city2_long);
        IF v_tmp > c_maximum_distance THEN
          v_tmp := c_maximum_distance;
        END IF;

        v_score := v_score + c_coef_location * (10000 - v_tmp * 10000 / c_maximum_distance);
        dbms_output.put_line('Location:' || (10000 - v_tmp * 10000 / c_maximum_distance));
      ELSIF p_city_id IS NOT NULL AND v_user.country_id IS NOT NULL THEN
        v_flag_location := 1;
        SELECT latitude, longitude INTO v_city1_lat, v_city1_long FROM edec_account_city WHERE id = p_city_id;
        SELECT latitude, longitude INTO v_city2_lat, v_city2_long FROM edec_account_city WHERE country_id = v_user.country_id AND rownum < 2;
        v_tmp := geographic_distance(v_city1_lat, v_city1_long, v_city2_lat, v_city2_long);
        IF v_tmp > c_maximum_distance THEN
          v_tmp := c_maximum_distance;
        END IF;

        v_score := v_score + c_coef_location * (10000 - v_tmp * 10000 / c_maximum_distance);
      ELSIF p_country_id IS NOT NULL AND v_user.city_id IS NOT NULL THEN
        v_flag_location := 1;
        SELECT latitude, longitude INTO v_city1_lat, v_city1_long FROM edec_account_city WHERE country_id = p_country_id AND rownum < 2;
        SELECT latitude, longitude INTO v_city2_lat, v_city2_long FROM edec_account_city WHERE id = v_user.city_id;
        v_tmp := geographic_distance(v_city1_lat, v_city1_long, v_city2_lat, v_city2_long);
        IF v_tmp > c_maximum_distance THEN
          v_tmp := c_maximum_distance;
        END IF;

        v_score := v_score + c_coef_location * (10000 - v_tmp * 10000 / c_maximum_distance);
      ELSIF p_country_id IS NOT NULL AND v_user.country_id IS NOT NULL THEN
        v_flag_location := 1;
        SELECT latitude, longitude INTO v_city1_lat, v_city1_long FROM edec_account_city WHERE country_id = p_country_id AND rownum < 2;
        SELECT latitude, longitude INTO v_city2_lat, v_city2_long FROM edec_account_city WHERE country_id = v_user.country_id AND rownum < 2;
        v_tmp := geographic_distance(v_city1_lat, v_city1_long, v_city2_lat, v_city2_long);
        IF v_tmp > c_maximum_distance THEN
          v_tmp := c_maximum_distance;
        END IF;

        v_score := v_score + c_coef_location * (10000 - v_tmp * 10000 / c_maximum_distance);
      END IF;

      v_tmp := v_flag_age * c_coef_age + v_flag_gender * c_coef_gender + v_flag_income * c_coef_income + v_flag_location * c_coef_location;
      CONTINUE WHEN v_tmp = 0;
      v_score := v_score / v_tmp;
      CONTINUE WHEN v_score = 0;

      IF p_id > v_user.id THEN
        v_lower_id := v_user.id;
        v_greater_id := p_id;
      ELSE
        v_lower_id := p_id;
        v_greater_id := v_user.id;
      END IF;

      SELECT COUNT(*) INTO v_tmp
      FROM metrics_useruser
      WHERE user1_id = v_lower_id AND user2_id = v_greater_id;

      IF v_tmp = 0 THEN
        INSERT INTO metrics_useruser(id, user1_id, user2_id, score, approval_score, preference_score)
        VALUES (NULL, v_lower_id, v_greater_id, v_score, 0, 0);
      ELSE
        UPDATE metrics_useruser SET score = v_score
        WHERE user1_id = v_lower_id AND user2_id = v_greater_id;
      END IF;
    END LOOP;
    EXIT WHEN c_users%NOTFOUND;
  END LOOP;
  CLOSE c_users;
END;
/

create or replace TRIGGER "EDEC_ACCOUNT_USERPROFILE_TR"
BEFORE INSERT ON "EDEC_ACCOUNT_USERPROFILE"
FOR EACH ROW
WHEN (new."ID" IS NULL)
    BEGIN
        SELECT "EDEC_ACCOUNT_USERPROFILE_SQ".nextval
        INTO :new."ID" FROM dual;
        on_update_user(:new.id, :new.birth_date, :new.gender, :new.income, :new.city_id, :new.country_id);
    END;
/

CREATE OR REPLACE TRIGGER EDEC_ACCOUNT_USERPROFILE_TR2
BEFORE UPDATE ON "EDEC_ACCOUNT_USERPROFILE"
FOR EACH ROW
BEGIN
  on_update_user(:new.id, :new.birth_date, :new.gender, :new.income, :new.city_id, :new.country_id);
END;
/

--CREATE OR REPLACE TRIGGER compound_user_trigger
--FOR INSERT ON edec_account_userprofile
--COMPOUND TRIGGER
--
--  p_id          edec_account_userprofile.id%TYPE;
--  p_birth_date  edec_account_userprofile.birth_date%TYPE;
--  p_gender      edec_account_userprofile.gender%TYPE;
--  p_income      edec_account_userprofile.income%TYPE;
--  p_city_id     edec_account_userprofile.city_id%TYPE;
--  p_country_id  edec_account_userprofile.country_id%TYPE;
--
--  BEFORE EACH ROW IS
--  BEGIN
----    dbms_output.put_line('Before insert compound');
--    p_id := :new.id;
--    p_birth_date := :new.birth_date;
--    p_gender := :new.gender;
--    p_income := :new.income;
--    p_city_id := :new.city_id;
--    p_country_id := :new.country_id;
--  END BEFORE EACH ROW;
--
--  AFTER STATEMENT IS
--  BEGIN
----    dbms_output.put_line('After insert compund');
--    on_update_user(p_id, p_birth_date, p_gender, p_income, p_city_id, p_country_id);
--  END AFTER STATEMENT;
--
--END compound_user_trigger;
--/
CREATE OR REPLACE PROCEDURE on_delete_preference(
  p_id_trait    IN product_trait.id%TYPE,
  p_id_user     IN edec_account_userprofile.id%TYPE,
  p_id_category IN product_trait.category_id%TYPE,
  p_preference    IN product_preference.preference%TYPE)
AS
  CURSOR c_produse IS SELECT id FROM product_product WHERE category_id = p_id_category;
  CURSOR c_useri IS SELECT id FROM edec_account_userprofile WHERE id != p_id_user;
  TYPE ids IS TABLE OF product_product.id%TYPE;
  TYPE ids_2 IS TABLE OF edec_account_userprofile.id%TYPE;
  id_users ids_2;
  id_products ids;
  v_total_1   INTEGER;
  v_total_2   INTEGER;
  v_comun     INTEGER;
  v_temporar  INTEGER;
  v_tmp       INTEGER;
  v_tmp2      INTEGER;
  v_score     INTEGER;

  v_comun_invers    INTEGER;
  v_temporar_invers INTEGER;
  v_lower_user_id   edec_account_userprofile.id%TYPE;
  v_greater_user_id edec_account_userprofile.id%TYPE;
BEGIN
  SELECT count(*) INTO v_total_1 FROM product_preference pref
  JOIN product_trait trait ON pref.trait_id = trait.id
  WHERE pref.user_id = p_id_user AND pref.preference = p_preference AND trait.category_id = p_id_category;

  OPEN c_produse;
  LOOP
    FETCH c_produse BULK COLLECT INTO id_products LIMIT 10000;
    FOR i IN 1..id_products.count LOOP
      IF v_total_1 = 0 THEN
        IF p_preference = 1 THEN
          BEGIN
            SELECT negative_score INTO v_tmp
            FROM metrics_userproduct
            WHERE product_id = id_products(i) and user_id = p_id_user;
          EXCEPTION
            WHEN no_data_found THEN
              CONTINUE;
          END;

          IF v_tmp = 0 THEN
            DELETE FROM metrics_userproduct
            WHERE product_id = id_products(i) AND user_id = p_id_user;
          ELSE
            UPDATE metrics_userproduct SET positive_score = 0
            WHERE product_id = id_products(i) AND user_id = p_id_user;
          END IF;
        ELSE
          BEGIN
            SELECT positive_score INTO v_tmp
            FROM metrics_userproduct
            WHERE product_id = id_products(i) and user_id = p_id_user;
          EXCEPTION
            WHEN no_data_found THEN
              CONTINUE;
          END;

          IF v_tmp = 0 THEN
            DELETE FROM metrics_userproduct
            WHERE product_id = id_products(i) AND user_id = p_id_user;
          ELSE
            UPDATE metrics_userproduct SET negative_score = 0
            WHERE product_id = id_products(i) AND user_id = p_id_user;
          END IF;
        END IF;

        CONTINUE;
      END IF;

      SELECT COUNT(*) INTO v_total_2
      FROM product_producttrait
      WHERE product_id = id_products(i);
      CONTINUE WHEN v_total_2 = 0;

      SELECT COUNT(*) INTO v_comun FROM product_preference pref
      JOIN product_producttrait prodt ON pref.trait_id = prodt.trait_id
      WHERE prodt.product_id = id_products(i) AND pref.user_id = p_id_user AND pref.preference = p_preference;

      v_score := (v_comun * (v_total_2 * 3 + v_total_1 * 2) * 2000) / (v_total_1 * v_total_2);
      IF v_score = 0 THEN
        IF p_preference = 1 THEN
          BEGIN
            SELECT negative_score INTO v_tmp
            FROM metrics_userproduct
            WHERE product_id = id_products(i) and user_id = p_id_user;
          EXCEPTION
            WHEN no_data_found THEN
              CONTINUE;
          END;

          IF v_tmp = 0 THEN
            DELETE FROM metrics_userproduct
            WHERE product_id = id_products(i) AND user_id = p_id_user;
          ELSE
            UPDATE metrics_userproduct SET positive_score = 0
            WHERE product_id = id_products(i) AND user_id = p_id_user;
          END IF;
        ELSE
          BEGIN
            SELECT positive_score INTO v_tmp
            FROM metrics_userproduct
            WHERE product_id = id_products(i) and user_id = p_id_user;
          EXCEPTION
            WHEN no_data_found THEN
              CONTINUE;
          END;

          IF v_tmp = 0 THEN
            DELETE FROM metrics_userproduct
            WHERE product_id = id_products(i) AND user_id = p_id_user;
          ELSE
            UPDATE metrics_userproduct SET negative_score = 0
            WHERE product_id = id_products(i) AND user_id = p_id_user;
          END IF;
        END IF;

        CONTINUE;
      END IF;

      IF p_preference = 1 THEN
        SELECT COUNT(*) INTO v_tmp
        FROM metrics_userproduct
        WHERE product_id = id_products(i) AND user_id = p_id_user;

        IF v_tmp = 0 THEN
          INSERT INTO metrics_userproduct(id, user_id, product_id, negative_score, positive_score)
          VALUES (NULL, p_id_user, id_products(i), 0, v_score);
        ELSE
          UPDATE metrics_userproduct SET positive_score = v_score
          WHERE product_id = id_products(i) AND user_id = p_id_user;
        END IF;
      ELSE
        SELECT COUNT(*) INTO v_tmp
        FROM metrics_userproduct
        WHERE product_id = id_products(i) AND user_id = p_id_user;

        IF v_tmp = 0 THEN
          INSERT INTO metrics_userproduct(id, user_id, product_id, negative_score, positive_score)
          VALUES (NULL, p_id_user, id_products(i), v_score, 0);
        ELSE
          UPDATE metrics_userproduct SET negative_score = v_score
          WHERE product_id = id_products(i) AND user_id = p_id_user;
        END IF;
      END IF;
    END LOOP;
    EXIT WHEN c_produse%NOTFOUND;
  END LOOP;
  CLOSE c_produse;

  --delete and update metrics user user
  SELECT COUNT(*) INTO v_total_1
  FROM product_preference
  WHERE user_id = p_id_user;
  OPEN c_useri;
  LOOP
    FETCH c_useri BULK COLLECT INTO id_users LIMIT 10000;
    FOR i IN 1..id_users.count LOOP
      IF v_total_1 = 0 THEN
        IF p_id_user < id_users(i) THEN
          v_lower_user_id := p_id_user;
          v_greater_user_id := id_users(i);
        ELSE
          v_lower_user_id := id_users(i);
          v_greater_user_id := p_id_user;
        END IF;

        BEGIN
          SELECT score, approval_score INTO v_tmp, v_tmp2
          FROM metrics_useruser
          WHERE user1_id = v_lower_user_id AND user2_id = v_greater_user_id;
        EXCEPTION
          WHEN no_data_found THEN
            CONTINUE;
        END;

        IF v_tmp = 0 AND v_tmp2 = 0 THEN
          DELETE FROM metrics_useruser
          WHERE user1_id = v_lower_user_id AND user2_id = v_greater_user_id;
        ELSE
          UPDATE metrics_useruser SET preference_score = 0
          WHERE user1_id = v_lower_user_id AND user2_id = v_greater_user_id;
        END IF;

        CONTINUE;
      END IF;

      SELECT COUNT(*) INTO v_total_2
      FROM product_preference
      WHERE user_id = id_users(i);
      CONTINUE WHEN v_total_2 = 0;

      SELECT COUNT(*) INTO v_comun
      FROM product_preference p1
      JOIN product_preference p2 ON p1.trait_id = p2.trait_id
      WHERE p1.user_id = p_id_user AND p2.user_id = id_users(i) AND p1.preference = p2.preference;

      SELECT COUNT(*) INTO v_comun_invers
      FROM product_preference p1
      JOIN product_preference p2 ON p1.trait_id = p2.trait_id
      WHERE p1.user_id = p_id_user AND p2.user_id = id_users(i) AND p1.preference != p2.preference;

      v_score := ((v_comun - v_comun_invers) * (v_total_1 + v_total_2) * 5000) / (v_total_1 * v_total_2);

      IF p_id_user < id_users(i) THEN
        v_lower_user_id := p_id_user;
        v_greater_user_id := id_users(i);
      ELSE
        v_lower_user_id := id_users(i);
        v_greater_user_id := p_id_user;
      END IF;

      IF v_score = 0 THEN
        BEGIN
          SELECT score, approval_score INTO v_tmp, v_tmp2
          FROM metrics_useruser
          WHERE user1_id = v_lower_user_id AND user2_id = v_greater_user_id;
        EXCEPTION
          WHEN no_data_found THEN
            CONTINUE;
        END;

        IF v_tmp = 0 AND v_tmp2 = 0 THEN
          DELETE FROM metrics_useruser
          WHERE user1_id = v_lower_user_id AND user2_id = v_greater_user_id;
        ELSE
          UPDATE metrics_useruser SET preference_score = 0
          WHERE user1_id = v_lower_user_id AND user2_id = v_greater_user_id;
        END IF;

        CONTINUE;
      END IF;

      SELECT COUNT(*) INTO v_tmp
      FROM metrics_useruser
      WHERE user1_id = v_lower_user_id AND user2_id = v_greater_user_id;

      IF v_tmp = 0 THEN
        INSERT INTO metrics_useruser(id, user1_id, user2_id, score, approval_score, preference_score)
        VALUES (NULL, v_lower_user_id, v_greater_user_id, 0, 0, v_score);
      ELSE
        UPDATE metrics_useruser SET preference_score = v_score
        WHERE user1_id = v_lower_user_id AND user2_id = v_greater_user_id;
      END IF;
    END LOOP;
    EXIT WHEN c_useri%NOTFOUND;
  END LOOP;
  CLOSE c_useri;
END;
/

-- merge numai pentru delete-uri de cate un row
CREATE OR REPLACE TRIGGER compound_delete_preference
FOR DELETE
ON product_preference
COMPOUND TRIGGER

  p_id product_preference.id%TYPE;
  p_preference product_preference.preference%TYPE;
  p_trait_id product_preference.trait_id%TYPE;
  p_user_id product_preference.user_id%TYPE;
  p_id_category product_product.category_id%TYPE := 1;

  BEFORE EACH ROW IS
  BEGIN
    p_id := :old."ID";
    p_preference := :old."PREFERENCE";
    p_trait_id := :old."TRAIT_ID";
    p_user_id := :old."USER_ID";
    SELECT category_id INTO p_id_category FROM product_trait WHERE id = :old."TRAIT_ID";
  END BEFORE EACH ROW;

  AFTER STATEMENT IS
  BEGIN
    on_delete_preference(p_trait_id, p_user_id, p_id_category, p_preference);
  END AFTER STATEMENT;

END compound_delete_preference;
/
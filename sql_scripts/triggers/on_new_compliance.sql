CREATE OR REPLACE PROCEDURE on_new_compliance (
  p_company_id   company_company.id%TYPE,
  p_ideology_id  company_ideology.id%TYPE
) AS
  TYPE    t_companies IS TABLE OF company_company.id%TYPE;
  TYPE    t_users     IS TABLE OF edec_account_userprofile.id%TYPE;
  CURSOR  c_companies IS SELECT id FROM company_company WHERE p_company_id != id;
  CURSOR  c_users     IS SELECT id FROM edec_account_userprofile;
  v_company_bucket        t_companies;
  v_users_bucket          t_users;
  v_company_total         INTEGER;
  v_user_total            INTEGER;
  v_user_approval         INTEGER;
  v_current_company_total INTEGER;
  v_other_company_total   INTEGER;
  v_common_ideologies     INTEGER;
  v_lower_id              INTEGER;
  v_greater_id            INTEGER;
  v_tmp                   INTEGER;
  v_score                 INTEGER;
BEGIN
  SELECT COUNT(*) + 1 INTO v_current_company_total
  FROM company_compliance
  WHERE company_id = p_company_id;
  OPEN c_companies;
  LOOP
    FETCH c_companies BULK COLLECT INTO v_company_bucket LIMIT 10000;
    FOR i IN 1..v_company_bucket.count LOOP
      SELECT COUNT(*) INTO v_other_company_total
      FROM company_compliance
      WHERE company_id = v_company_bucket(i);
      CONTINUE WHEN v_other_company_total = 0;

      SELECT COUNT(*) INTO v_common_ideologies
      FROM company_compliance c1
      JOIN company_compliance c2 ON c1.ideology_id = c2.ideology_id
      WHERE c1.company_id = p_company_id AND c2.company_id = v_company_bucket(i);

      SELECT COUNT(*) INTO v_tmp
      FROM company_compliance
      WHERE company_id = v_company_bucket(i) AND ideology_id = p_ideology_id;
      v_common_ideologies := v_common_ideologies + v_tmp;

      v_score := 5000 * v_common_ideologies * (v_current_company_total + v_other_company_total) / (v_current_company_total * v_other_company_total);
      CONTINUE WHEN v_score = 0;

      IF v_company_bucket(i) < p_company_id THEN
        v_lower_id := v_company_bucket(i);
        v_greater_id := p_company_id;
      ELSE
        v_lower_id := p_company_id;
        v_greater_id := v_company_bucket(i);
      END IF;

      SELECT COUNT(*) INTO v_tmp
      FROM metrics_companycompany
      WHERE company1_id = v_lower_id AND company2_id = v_greater_id;

      IF v_tmp = 0 THEN
        INSERT INTO metrics_companycompany(id, company1_id, company2_id, score)
        VALUES (NULL, v_lower_id, v_greater_id, v_score);
      ELSE
        UPDATE metrics_companycompany SET score = v_score
        WHERE company1_id = v_lower_id AND company2_id = v_greater_id;
      END IF;
    END LOOP;
    EXIT WHEN c_companies%NOTFOUND;
  END LOOP;

  SELECT COUNT(*) + 1 INTO v_company_total
  FROM company_compliance
  WHERE company_id = p_company_id;
  OPEN c_users;
  LOOP
    FETCH c_users BULK COLLECT INTO v_users_bucket LIMIT 10000;
    FOR i IN 1..v_users_bucket.count LOOP
      -- facem mai intai update la positive_score
      SELECT COUNT(*) INTO v_user_total
      FROM company_approval
      WHERE user_id = v_users_bucket(i) AND approval = 1;

      IF v_user_total != 0 THEN
        SELECT COUNT(*) INTO v_common_ideologies
        FROM company_approval a
        JOIN company_compliance c ON a.ideology_id = c.ideology_id
        WHERE a.user_id = v_users_bucket(i) AND c.company_id = p_company_id AND a.approval = 1;

        SELECT COUNT(*) INTO v_tmp
        FROM company_approval
        WHERE user_id = v_users_bucket(i) AND ideology_id = p_ideology_id AND approval = 1;
        v_common_ideologies := v_common_ideologies + v_tmp;

        v_score := 2000 * v_common_ideologies * (3 * v_company_total + 2 * v_user_total) / (v_user_total * v_company_total);
        IF v_score != 0 THEN
          SELECT COUNT(*) INTO v_tmp
          FROM metrics_usercompany
          WHERE user_id = v_users_bucket(i) AND company_id = p_company_id;

          IF v_tmp = 0 THEN
            INSERT INTO metrics_usercompany(id, user_id, company_id, negative_score, positive_score)
            VALUES (NULL, v_users_bucket(i), p_company_id, 0, v_score);
          ELSE
            UPDATE metrics_usercompany SET positive_score = v_score
            WHERE user_id = v_users_bucket(i) AND company_id = p_company_id;
          END IF;
        END IF;
      END IF;

      SELECT COUNT(*) INTO v_user_total
      FROM company_approval
      WHERE user_id = v_users_bucket(i) AND approval = 0;

      IF v_user_total != 0 THEN
        SELECT COUNT(*) INTO v_common_ideologies
        FROM company_approval a
        JOIN company_compliance c ON a.ideology_id = c.ideology_id
        WHERE a.user_id = v_users_bucket(i) AND c.company_id = p_company_id AND a.approval = 0;

        SELECT COUNT(*) INTO v_tmp
        FROM company_approval
        WHERE user_id = v_users_bucket(i) AND ideology_id = p_ideology_id AND approval = 0;
        v_common_ideologies := v_common_ideologies + v_tmp;

        v_score := 2000 * v_common_ideologies * (3 * v_company_total + 2 * v_user_total) / (v_user_total * v_company_total);
        IF v_score != 0 THEN
          SELECT COUNT(*) INTO v_tmp
          FROM metrics_usercompany
          WHERE user_id = v_users_bucket(i) AND company_id = p_company_id;

          IF v_tmp = 0 THEN
            INSERT INTO metrics_usercompany(id, user_id, company_id, negative_score, positive_score)
            VALUES (NULL, v_users_bucket(i), p_company_id, v_score, 0);
          ELSE
            UPDATE metrics_usercompany SET negative_score = v_score
            WHERE user_id = v_users_bucket(i) AND company_id = p_company_id;
          END IF;
        END IF;
      END IF;
    END LOOP;
    EXIT WHEN c_users%NOTFOUND;
  END LOOP;
  CLOSE c_users;
END;
/

create or replace TRIGGER "COMPANY_COMPLIANCE_TR"
BEFORE INSERT ON "COMPANY_COMPLIANCE"
FOR EACH ROW
WHEN (new."ID" IS NULL)
    BEGIN
        SELECT "COMPANY_COMPLIANCE_SQ".nextval
        INTO :new."ID" FROM dual;
        on_new_compliance(:new.company_id, :new.ideology_id);
    END;
/

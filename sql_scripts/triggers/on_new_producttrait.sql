CREATE OR REPLACE PROCEDURE on_new_product_trait(
  p_id_trait      IN product_trait.id%TYPE,
  p_id_product    IN product_product.id%TYPE,
  p_id_category   IN product_product.category_id%TYPE)
AS
  CURSOR c_produse  IS SELECT id FROM product_product WHERE id != p_id_product AND category_id = p_id_category;
  CURSOR c_users    IS SELECT id FROM edec_account_userprofile;
  TYPE t_users      IS TABLE OF edec_account_userprofile.id%TYPE;
  TYPE ids          IS TABLE OF product_product.id%TYPE;
  v_users_bucket    t_users;
  id_products       ids;
  v_user_total      INTEGER;
  v_product_total   INTEGER;
  v_common_traits   INTEGER;
  v_tmp             INTEGER;
  v_total_1         INTEGER;
  v_total_2         INTEGER;
  v_comun           INTEGER;
  v_temporar        INTEGER;
  v_score           INTEGER;
  v_lower_id        INTEGER;
  v_greater_id      INTEGER;
BEGIN
  SELECT COUNT(*) + 1 INTO v_total_1
  FROM product_producttrait
  WHERE product_id = p_id_product;
  OPEN c_produse;
  LOOP
    FETCH c_produse BULK COLLECT INTO id_products LIMIT 10000;
    FOR i IN 1..id_products.count LOOP
      SELECT COUNT(*) INTO v_total_2
      FROM product_producttrait
      WHERE product_id = id_products(i);
      CONTINUE WHEN v_total_2 = 0;

      SELECT COUNT(*) INTO v_comun
      FROM product_producttrait pt1
      JOIN product_producttrait pt2 ON pt1.trait_id = pt2.trait_id
      WHERE pt1.product_id = p_id_product AND pt2.product_id = id_products(i);

      SELECT COUNT(*) INTO v_temporar
      FROM product_producttrait
      WHERE product_id = id_products(i) AND trait_id = p_id_trait;
      v_comun := v_comun + v_temporar;

      v_score := (v_comun * (v_total_1 + v_total_2) * 5000) / (v_total_1 * v_total_2);
      CONTINUE WHEN v_score = 0;

      IF p_id_product < id_products(i) THEN
        v_lower_id := p_id_product;
        v_greater_id := id_products(i);
      ELSE
        v_lower_id := id_products(i);
        v_greater_id := p_id_product;
      END IF;

      SELECT COUNT(*) INTO v_tmp
      FROM metrics_productproduct
      WHERE product1_id = v_lower_id and product2_id = v_greater_id;

      IF v_tmp = 0 THEN
        INSERT INTO metrics_productproduct(id, product1_id, product2_id, score)
        VALUES (NULL, v_lower_id, v_greater_id, v_score);
      ELSE
        UPDATE metrics_productproduct SET score = v_score
        WHERE product1_id = v_lower_id and product2_id = v_greater_id;
      END IF;
    END LOOP;
    EXIT WHEN c_produse%NOTFOUND;
  END LOOP;
  CLOSE c_produse;

  SELECT COUNT(*) + 1 INTO v_product_total
  FROM product_producttrait
  WHERE product_id = p_id_product;
  OPEN c_users;
  LOOP
    FETCH c_users BULK COLLECT INTO v_users_bucket LIMIT 10000;
    FOR i IN 1..v_users_bucket.count LOOP
      SELECT COUNT(*) INTO v_user_total
      FROM product_preference
      WHERE user_id = v_users_bucket(i) AND preference = 1;

      IF v_user_total != 0 THEN
        SELECT COUNT(*) INTO v_common_traits
        FROM product_preference p
        JOIN product_producttrait pt ON p.trait_id = pt.trait_id
        WHERE p.user_id = v_users_bucket(i) AND pt.product_id = p_id_product AND p.preference = 1;

        SELECT COUNT(*) INTO v_tmp
        FROM product_preference
        WHERE user_id = v_users_bucket(i) AND trait_id = p_id_trait AND preference = 1;
        v_common_traits := v_common_traits + v_tmp;

        v_score := 2000 * v_common_traits * (3 * v_product_total + 2 * v_user_total) / (v_user_total * v_product_total);
        IF v_score != 0 THEN
          SELECT COUNT(*) INTO v_tmp
          FROM metrics_userproduct
          WHERE user_id = v_users_bucket(i) AND product_id = p_id_product;

          IF v_tmp = 0 THEN
            INSERT INTO metrics_userproduct(id, user_id, product_id, negative_score, positive_score)
            VALUES (NULL, v_users_bucket(i), p_id_product, 0, v_score);
          ELSE
            UPDATE metrics_userproduct SET positive_score = v_score
            WHERE user_id = v_users_bucket(i) AND product_id = p_id_product;
          END IF;
        END IF;
      END IF;

      SELECT COUNT(*) INTO v_user_total
      FROM product_preference
      WHERE user_id = v_users_bucket(i) AND preference = 0;
      IF v_user_total != 0 THEN
        SELECT COUNT(*) INTO v_common_traits
        FROM product_preference p
        JOIN product_producttrait pt ON p.trait_id = pt.trait_id
        WHERE p.user_id = v_users_bucket(i) AND pt.product_id = p_id_product AND p.preference = 0;

        SELECT COUNT(*) INTO v_tmp
        FROM product_preference
        WHERE user_id = v_users_bucket(i) AND trait_id = p_id_trait AND preference = 0;
        v_common_traits := v_common_traits + v_tmp;

        v_score := 2000 * v_common_traits * (3 * v_product_total + 2 * v_user_total) / (v_user_total * v_product_total);
        IF v_score != 0 THEN
          SELECT COUNT(*) INTO v_tmp
          FROM metrics_userproduct
          WHERE user_id = v_users_bucket(i) AND product_id = p_id_product;

          IF v_tmp = 0 THEN
            INSERT INTO metrics_userproduct(id, user_id, product_id, negative_score, positive_score)
            VALUES (NULL, v_users_bucket(i), p_id_product, v_score, 0);
          ELSE
            UPDATE metrics_userproduct SET negative_score = v_score
            WHERE user_id = v_users_bucket(i) AND product_id = p_id_product;
          END IF;
        END IF;
      END IF;
    END LOOP;
    EXIT WHEN c_users%NOTFOUND;
  END LOOP;
  CLOSE c_users;
END;
/

create or replace TRIGGER "PRODUCT_PRODUCTTRAIT_TR"
BEFORE INSERT ON "PRODUCT_PRODUCTTRAIT"
FOR EACH ROW
WHEN (new."ID" IS NULL)
    DECLARE
      p_id_category product_product.category_id%TYPE;
    BEGIN
        SELECT "PRODUCT_PRODUCTTRAIT_SQ".nextval
        INTO :new."ID" FROM dual;

        SELECT category_id INTO p_id_category
        FROM product_product
        WHERE id = :new."PRODUCT_ID";

        on_new_product_trait(:new."TRAIT_ID", :new."PRODUCT_ID", p_id_category);
    END;
/
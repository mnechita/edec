CREATE OR REPLACE PROCEDURE on_new_preference(
  p_id_trait    IN product_trait.id%TYPE,
  p_id_user     IN edec_account_userprofile.id%TYPE,
  p_id_category IN product_trait.category_id%TYPE,
  p_preference    IN product_preference.preference%TYPE)
AS
  CURSOR c_produse  IS SELECT id FROM product_product WHERE category_id = p_id_category;
  CURSOR c_useri    IS SELECT id FROM edec_account_userprofile WHERE id != p_id_user;
  TYPE ids          IS TABLE OF product_product.id%TYPE;
  TYPE ids_2        IS TABLE OF edec_account_userprofile.id%TYPE;

  id_products ids;
  id_users    ids_2;
  v_total_1   INTEGER;
  v_total_2   INTEGER;
  v_comun     INTEGER;
  v_temporar  INTEGER;
  v_tmp       INTEGER;
  v_tmp2      INTEGER;
  v_score     INTEGER;

  v_comun_invers      INTEGER;
  v_temporar_invers   INTEGER;
  v_lower_user_id     edec_account_userprofile.id%TYPE;
  v_greater_user_id   edec_account_userprofile.id%TYPE;
BEGIN
  -- modify metrics user product
  SELECT count(*) + 1 INTO v_total_1 FROM product_preference pref
  JOIN product_trait trait ON pref.trait_id = trait.id
  WHERE pref.user_id = p_id_user AND pref.preference = p_preference AND trait.category_id = p_id_category;

  OPEN c_produse;
  LOOP
    FETCH c_produse BULK COLLECT INTO id_products LIMIT 10000;
    FOR i IN 1..id_products.count LOOP
      SELECT COUNT(*) INTO v_total_2
      FROM product_producttrait
      WHERE product_id = id_products(i);
      CONTINUE WHEN v_total_2 = 0;

      SELECT COUNT(*) INTO v_comun FROM product_preference pref
      JOIN product_producttrait prodt ON pref.trait_id = prodt.trait_id
      WHERE prodt.product_id = id_products(i) AND pref.user_id = p_id_user AND pref.preference = p_preference;

      SELECT COUNT(*) INTO v_temporar FROM product_producttrait
      WHERE product_id = id_products(i) AND trait_id = p_id_trait;
      v_comun := v_comun + v_temporar;

      v_score := (v_comun * (v_total_2 * 3 + v_total_1 * 2) * 2000) / (v_total_1 * v_total_2);
      CONTINUE WHEN v_score = 0;

      IF p_preference = 1 THEN
        SELECT COUNT(*) INTO v_tmp
        FROM metrics_userproduct
        WHERE product_id = id_products(i) AND user_id = p_id_user;

        IF v_tmp = 0 THEN
          INSERT INTO metrics_userproduct(id, user_id, product_id, negative_score, positive_score)
          VALUES (NULL, p_id_user, id_products(i), 0, v_score);
        ELSE
          UPDATE metrics_userproduct SET positive_score = v_score
          WHERE product_id = id_products(i) AND user_id = p_id_user;
        END IF;
      ELSE
        SELECT COUNT(*) INTO v_tmp
        FROM metrics_userproduct
        WHERE product_id = id_products(i) AND user_id = p_id_user;

        IF v_tmp = 0 THEN
          INSERT INTO metrics_userproduct(id, user_id, product_id, negative_score, positive_score)
          VALUES (NULL, p_id_user, id_products(i), v_score, 0);
        ELSE
          UPDATE metrics_userproduct SET negative_score = v_score
          WHERE product_id = id_products(i) AND user_id = p_id_user;
        END IF;
      END IF;
    END LOOP;
    EXIT WHEN c_produse%NOTFOUND;
  END LOOP;
  CLOSE c_produse;

--   modify metrics user user
  SELECT COUNT(*) + 1 INTO v_total_1
  FROM product_preference
  WHERE user_id = p_id_user;
  OPEN c_useri;
  LOOP
    FETCH c_useri BULK COLLECT INTO id_users LIMIT 10000;
    FOR i IN 1..id_users.count LOOP
      SELECT COUNT(*) INTO v_total_2
      FROM product_preference
      WHERE user_id = id_users(i);
      CONTINUE WHEN v_total_2 = 0;

      SELECT COUNT(*) INTO v_comun
      FROM product_preference p1
      JOIN product_preference p2 ON p1.trait_id = p2.trait_id
      WHERE p1.user_id = p_id_user AND p2.user_id = id_users(i) AND p1.preference = p2.preference;

      SELECT COUNT(*) INTO v_tmp
      FROM product_preference
      WHERE user_id = id_users(i) AND trait_id = p_id_trait AND preference = p_preference;
      v_comun := v_comun + v_tmp;

      SELECT COUNT(*) INTO v_comun_invers
      FROM product_preference p1
      JOIN product_preference p2 ON p1.trait_id = p2.trait_id
      WHERE p1.user_id = p_id_user AND p2.user_id = id_users(i) AND p1.preference != p2.preference;

      SELECT COUNT(*) INTO v_tmp
      FROM product_preference
      WHERE user_id = id_users(i) AND trait_id = p_id_trait AND preference != p_preference;
      v_comun_invers := v_comun_invers + v_tmp;

      v_score := ((v_comun - v_comun_invers) * (v_total_1 + v_total_2) * 5000) / (v_total_1 * v_total_2);

      IF p_id_user < id_users(i) THEN
        v_lower_user_id := p_id_user;
        v_greater_user_id := id_users(i);
      ELSE
        v_lower_user_id := id_users(i);
        v_greater_user_id := p_id_user;
      END IF;

      IF v_score = 0 THEN
        BEGIN
          SELECT score, approval_score INTO v_tmp, v_tmp2
          FROM metrics_useruser
          WHERE user1_id = v_lower_user_id AND user2_id = v_greater_user_id;
        EXCEPTION
          WHEN no_data_found THEN
            CONTINUE;
        END;

        IF v_tmp = 0 AND v_tmp2 = 0 THEN
          DELETE FROM metrics_useruser
          WHERE user1_id = v_lower_user_id AND user2_id = v_greater_user_id;
        ELSE
          UPDATE metrics_useruser SET preference_score = 0
          WHERE user1_id = v_lower_user_id AND user2_id = v_greater_user_id;
        END IF;

        CONTINUE;
      END IF;

      SELECT COUNT(*) INTO v_tmp
      FROM metrics_useruser
      WHERE user1_id = v_lower_user_id AND user2_id = v_greater_user_id;

      IF v_tmp = 0 THEN
        INSERT INTO metrics_useruser(id, user1_id, user2_id, score, approval_score, preference_score)
        VALUES (NULL, v_lower_user_id, v_greater_user_id, 0, 0, v_score);
      ELSE
        UPDATE metrics_useruser SET preference_score = v_score
        WHERE user1_id = v_lower_user_id AND user2_id = v_greater_user_id;
      END IF;
    END LOOP;
    EXIT WHEN c_useri%NOTFOUND;
  END LOOP;
  CLOSE c_useri;
END;
/

create or replace TRIGGER "PRODUCT_PREFERENCE_TR"
BEFORE INSERT ON "PRODUCT_PREFERENCE"
FOR EACH ROW
WHEN (new."ID" IS NULL)
    DECLARE
        p_id_category product_product.category_id%TYPE;
    BEGIN
        SELECT "PRODUCT_PREFERENCE_SQ".nextval
        INTO :new."ID" FROM dual;
        select category_id into p_id_category from product_trait
        where id = :new."TRAIT_ID";
        on_new_preference(:new."TRAIT_ID", :new."USER_ID", p_id_category, :new."PREFERENCE");
    END;
/

CREATE OR REPLACE TRIGGER product_preference_tr2
BEFORE UPDATE ON product_preference
FOR EACH ROW
DECLARE
  p_id_category product_product.category_id%TYPE;
BEGIN
  SELECT category_id INTO p_id_category FROM product_trait
  WHERE id = :new.trait_id;
  on_new_preference(:new.trait_id, :new.user_id, p_id_category, :new.preference);
END;
/
jQuery(function($) {'use strict',

	//#main-slider
	$(function(){
		$('#main-slider.carousel').carousel({
			interval: 8000
		});
	});


	// accordian
	$('.accordion-toggle').on('click', function(){
		$(this).closest('.panel-group').children().each(function(){
		$(this).find('>.panel-heading').removeClass('active');
		 });

	 	$(this).closest('.panel-heading').toggleClass('active');
	});

	//Initiat WOW JS
	new WOW().init();

	// portfolio filter
	$(window).load(function(){'use strict';
		var $portfolio_selectors = $('.portfolio-filter >li>a');
		var $portfolio = $('.portfolio-items');
		$portfolio.isotope({
			itemSelector : '.portfolio-item',
			layoutMode : 'fitRows'
		});
		
		$portfolio_selectors.on('click', function(){
			$portfolio_selectors.removeClass('active');
			$(this).addClass('active');
			var selector = $(this).attr('data-filter');
			$portfolio.isotope({ filter: selector });
			return false;
		});
	});

	// Contact form
	var form = $('#main-contact-form');
	form.submit(function(event){
		event.preventDefault();
		var form_status = $('<div class="form_status"></div>');
		$.ajax({
			url: $(this).attr('action'),

			beforeSend: function(){
				form.prepend( form_status.html('<p><i class="fa fa-spinner fa-spin"></i> Email is sending...</p>').fadeIn() );
			}
		}).done(function(data){
			form_status.html('<p class="text-success">' + data.message + '</p>').delay(3000).fadeOut();
		});
	});

	
	//goto top
	$('.gototop').click(function(event) {
		event.preventDefault();
		$('html, body').animate({
			scrollTop: $("body").offset().top
		}, 500);
	});	

	//Pretty Photo
	$("a[rel^='prettyPhoto']").prettyPhoto({
		social_tools: false
	});	
});

$(document).ready(function(e){
    $('.search-panel .dropdown-menu').find('a').click(function(e) {
		e.preventDefault();
		var param = $(this).attr("href").replace("#","");
		var concept = $(this).text();
		$('.search-panel span#search_concept').text(concept);
		$('.input-group #search_param').val(param);
	});
});

$("#dropdown-food1 li").click(function () {
    var name = $(this).text();
    
    $("#hiddenSubcategory").val(name);
    $("#menu1food").text(name);
    //alert($("#hiddenSubcategory").val())
    
 });

$("#dropdown-food2 li").click(function () {
    var name = $(this).text();
    
    $("#hiddenFilter").val(name);
    $("#menu2food").text(name);
    //alert($("#hiddenFilter").val())
    
 });

$("#dropdown-clothes1 li").click(function () {
    var name = $(this).text();
    
    $("#hiddenSubcategory").val(name);
    $("#menu1clothes").text(name);
    //alert($("#hiddenSubcategory").val())
    
 });

$("#dropdown-clothes2 li").click(function () {
    var name = $(this).text();
    
    $("#hiddenFilter").val(name);
    $("#menu2clothes").text(name);
    //alert($("#hiddenFilter").val())
    
 });

$("#dropdown-electronics1 li").click(function () {
    var name = $(this).text();
    
    $("#hiddenSubcategory").val(name);
    $("#menu1electronics").text(name);
    //alert($("#hiddenSubcategory").val())
    
 });

$("#dropdown-electronics2 li").click(function () {
    var name = $(this).text();
    
    $("#hiddenFilter").val(name);
    $("#menu2electronics").text(name);
    //alert($("#hiddenFilter").val())
    
 });

$("#dropdown-home1 li").click(function () {
    var name = $(this).text();
    
    $("#hiddenSubcategory").val(name);
    $("#menu1home").text(name);
    //alert($("#hiddenSubcategory").val())
    
 });

$("#dropdown-home2 li").click(function () {
    var name = $(this).text();
    
    $("#hiddenFilter").val(name);
    $("#menu2home").text(name);
    //alert($("#hiddenFilter").val())
    
 });

$("#nav-tabs li").click(function () {
    $("#hiddenSubcategory").val("All Subcategories");
    $("#hiddenFilter").val("Alphabetic Asc");
    $("#menu1food").text("All Subcategories");
    $("#menu2food").text("Alphabetic Asc");
    $("#menu1clothes").text("All Subcategories");
    $("#menu2clothes").text("Alphabetic Asc");
    $("#menu1electronics").text("All Subcategories");
    $("#menu2electronics").text("Alphabetic Asc");
    $("#menu1home").text("All Subcategories");
    $("#menu2home").text("Alphabetic Asc");
    $("#fromvalue1").val("");
    $("#tovalue1").val("");
    $("#fromvalue2").val("");
    $("#tovalue2").val("");
    $("#fromvalue3").val("");
    $("#tovalue3").val("");
    $("#fromvalue4").val("");
    $("#tovalue4").val("");
    var h = $("a", this).attr('href');
    $("#hiddenCategory").val(h.substring(1));
    //alert($("#hiddenCategory").val())
    
 });






$("#dropdown-poside li").click(function () {

   var name = $(this).text();
   var string = '<li class="plan-feature">' + name + '<button class="remove-item btn btn-default btn-xs pull-right"><span class="glyphicon glyphicon-remove"></span></button>' + "</li>";
   
    $("#poside").append(string);
    $(this).remove();

    var ul = document.getElementById("dropdown-negide");
    var items = ul.getElementsByTagName("li");
    
    for (var i = 0; i < items.length; ++i) {
    	var nume = $(items[i]).text();
    	var n = name.localeCompare(nume);
    	if(n==0)
    	{
    		$(items[i]).remove();
    	}
    }
    
 });

$("#dropdown-negide li").click(function () {

   var name = $(this).text();
   var string = '<li class="plan-feature">' + name + '<button class="remove-item btn btn-default btn-xs pull-right"><span class="glyphicon glyphicon-remove"></span></button>' + "</li>";
    $("#negide").append(string);
    $(this).remove();

    var ul = document.getElementById("dropdown-poside");
    var items = ul.getElementsByTagName("li");
    
    for (var i = 0; i < items.length; ++i) {
    	var nume = $(items[i]).text();
    	var n = name.localeCompare(nume);
    	if(n==0)
    	{
    		$(items[i]).remove();
    	}
    }
    
 });

$('.todolist').on('click','.remove-item',function(){
	$(this).parent().remove();
	var name = '<li><a>' + $(this).parent().text() + '</li></a>';
	$("#dropdown-negide").append(name);
	$("#dropdown-poside").append(name);

});

$('.testpos').on('click','#dropdown-poside li',function(){
	var name = $(this).text();
   var string = '<li class="plan-feature">' + name + '<button class="remove-item btn btn-default btn-xs pull-right"><span class="glyphicon glyphicon-remove"></span></button>' + "</li>";
   
    $("#poside").append(string);
    $(this).remove();

    var ul = document.getElementById("dropdown-negide");
    var items = ul.getElementsByTagName("li");
    
    for (var i = 0; i < items.length; ++i) {
    	var nume = $(items[i]).text();
    	var n = name.localeCompare(nume);
    	if(n==0)
    	{
    		$(items[i]).remove();
    	}
    }
});

$('.testneg').on('click','#dropdown-negide li',function(){

	var name = $(this).text();
   var string = '<li class="plan-feature">' + name + '<button class="remove-item btn btn-default btn-xs pull-right"><span class="glyphicon glyphicon-remove"></span></button>' + "</li>";
    $("#negide").append(string);
    $(this).remove();

    var ul = document.getElementById("dropdown-poside");
    var items = ul.getElementsByTagName("li");
    
    for (var i = 0; i < items.length; ++i) {
    	var nume = $(items[i]).text();
    	var n = name.localeCompare(nume);
    	if(n==0)
    	{
    		$(items[i]).remove();
    	}
    }
	
});



$("#submitIde").click(function () {

	$("#formIde").submit(function(event) { 
        event.preventDefault(); 
        var everythingIsOkay = true;
      
      	var stringToSend= "";
        var ul = document.getElementById("poside");
        var items = ul.getElementsByTagName("li");
    
        for (var i = 0; i < items.length; ++i) {
    	    var nume = $(items[i]).text();
    	    stringToSend = stringToSend + nume + "#";
        }

        var ul = document.getElementById("negide");
        var items = ul.getElementsByTagName("li");
    
        for (var i = 0; i < items.length; ++i) {
    	    var nume = $(items[i]).text();
    	    stringToSend = stringToSend + nume + "#";
        }

        $("#hiddenIde").val(stringToSend);
        alert($("#hiddenIde").val());
      
        if(everythingIsOkay){
            document.forms["formIde"].submit(); 
        }
    });
});



$("#dropdown-posftra li").click(function () {

   var name = $(this).text();
   var string = '<li class="plan-feature">' + name + '<button class="remove-food btn btn-default btn-xs pull-right"><span class="glyphicon glyphicon-remove"></span></button>' + "</li>";
    $("#posftra").append(string);
    $(this).remove();

    var ul = document.getElementById("dropdown-negftra");
    var items = ul.getElementsByTagName("li");
    
    for (var i = 0; i < items.length; ++i) {
    	var nume = $(items[i]).text();
    	var n = name.localeCompare(nume);
    	if(n==0)
    	{
    		$(items[i]).remove();
    	}
    }
    
 });

$("#dropdown-negftra li").click(function () {

   var name = $(this).text();
   var string = '<li class="plan-feature">' + name + '<button class="remove-food btn btn-default btn-xs pull-right"><span class="glyphicon glyphicon-remove"></span></button>' + "</li>";
    $("#negftra").append(string);
    $(this).remove();

    var ul = document.getElementById("dropdown-posftra");
    var items = ul.getElementsByTagName("li");
    
    for (var i = 0; i < items.length; ++i) {
    	var nume = $(items[i]).text();
    	var n = name.localeCompare(nume);
    	if(n==0)
    	{
    		$(items[i]).remove();
    	}
    }
    
 });

$('.toFood').on('click','.remove-food',function(){
	$(this).parent().remove();
	var name = '<li><a>' + $(this).parent().text() + '</li></a>';
	$("#dropdown-negftra").append(name);
	$("#dropdown-posftra").append(name);

});

$('.testposf').on('click','#dropdown-posftra li',function(){
	var name = $(this).text();
   var string = '<li class="plan-feature">' + name + '<button class="remove-food btn btn-default btn-xs pull-right"><span class="glyphicon glyphicon-remove"></span></button>' + "</li>";
   
    $("#posftra").append(string);
    $(this).remove();

    var ul = document.getElementById("dropdown-negftra");
    var items = ul.getElementsByTagName("li");
    
    for (var i = 0; i < items.length; ++i) {
    	var nume = $(items[i]).text();
    	var n = name.localeCompare(nume);
    	if(n==0)
    	{
    		$(items[i]).remove();
    	}
    }
});

$('.testnegf').on('click','#dropdown-negftra li',function(){

	var name = $(this).text();
   var string = '<li class="plan-feature">' + name + '<button class="remove-food btn btn-default btn-xs pull-right"><span class="glyphicon glyphicon-remove"></span></button>' + "</li>";
    $("#negftra").append(string);
    $(this).remove();

    var ul = document.getElementById("dropdown-posftra");
    var items = ul.getElementsByTagName("li");
    
    for (var i = 0; i < items.length; ++i) {
    	var nume = $(items[i]).text();
    	var n = name.localeCompare(nume);
    	if(n==0)
    	{
    		$(items[i]).remove();
    	}
    }
	
});

$("#submitftra").click(function () {

	$("#formftra").submit(function(event) { 
        event.preventDefault(); 
        var everythingIsOkay = true;
      
      	var stringToSend= "";
        var ul = document.getElementById("posftra");
        var items = ul.getElementsByTagName("li");
    
        for (var i = 0; i < items.length; ++i) {
    	    var nume = $(items[i]).text();
    	    stringToSend = stringToSend + nume + "#";
        }

        var ul = document.getElementById("negftra");
        var items = ul.getElementsByTagName("li");
    
        for (var i = 0; i < items.length; ++i) {
    	    var nume = $(items[i]).text();
    	    stringToSend = stringToSend + nume + "#";
        }

        $("#hiddenftra").val(stringToSend);
        //alert($("#hiddenftra").val());
      
        if(everythingIsOkay){
            document.forms["formftra"].submit(); 
        }
    });
});


$("#dropdown-posctra li").click(function () {

   var name = $(this).text();
   var string = '<li class="plan-feature">' + name + '<button class="remove-clothes btn btn-default btn-xs pull-right"><span class="glyphicon glyphicon-remove"></span></button>' + "</li>";
    $("#posctra").append(string);
    $(this).remove();

    var ul = document.getElementById("dropdown-negctra");
    var items = ul.getElementsByTagName("li");
    
    for (var i = 0; i < items.length; ++i) {
    	var nume = $(items[i]).text();
    	var n = name.localeCompare(nume);
    	if(n==0)
    	{
    		$(items[i]).remove();
    	}
    }
    
 });

$("#dropdown-negctra li").click(function () {

   var name = $(this).text();
   
   var string = '<li class="plan-feature">' + name + '<button class="remove-clothes btn btn-default btn-xs pull-right"><span class="glyphicon glyphicon-remove"></span></button>' + "</li>";
    $("#negctra").append(string);
    
    $(this).remove();

    var ul = document.getElementById("dropdown-posctra");
    var items = ul.getElementsByTagName("li");
    
    for (var i = 0; i < items.length; ++i) {
    	var nume = $(items[i]).text();
    	var n = name.localeCompare(nume);
    	if(n==0)
    	{
    		$(items[i]).remove();
    	}
    }
    
 });

$('.toClothes').on('click','.remove-clothes',function(){
	$(this).parent().remove();
	var name = '<li><a>' + $(this).parent().text() + '</li></a>';
	$("#dropdown-negctra").append(name);
	$("#dropdown-posctra").append(name);

});

$('.testposc').on('click','#dropdown-posctra li',function(){
	var name = $(this).text();
   var string = '<li class="plan-feature">' + name + '<button class="remove-clothes btn btn-default btn-xs pull-right"><span class="glyphicon glyphicon-remove"></span></button>' + "</li>";
   
    $("#posctra").append(string);
    $(this).remove();

    var ul = document.getElementById("dropdown-negctra");
    var items = ul.getElementsByTagName("li");
    
    for (var i = 0; i < items.length; ++i) {
    	var nume = $(items[i]).text();
    	var n = name.localeCompare(nume);
    	if(n==0)
    	{
    		$(items[i]).remove();
    	}
    }
});

$('.testnegc').on('click','#dropdown-negctra li',function(){

	var name = $(this).text();
   var string = '<li class="plan-feature">' + name + '<button class="remove-clothes btn btn-default btn-xs pull-right"><span class="glyphicon glyphicon-remove"></span></button>' + "</li>";
    $("#negctra").append(string);
    $(this).remove();

    var ul = document.getElementById("dropdown-posctra");
    var items = ul.getElementsByTagName("li");
    
    for (var i = 0; i < items.length; ++i) {
    	var nume = $(items[i]).text();
    	var n = name.localeCompare(nume);
    	if(n==0)
    	{
    		$(items[i]).remove();
    	}
    }
	
});



$("#submitctra").click(function () {

	$("#formctra").submit(function(event) { 
        event.preventDefault(); 
        var everythingIsOkay = true;
      	
      	var stringToSend= "";
        var ul = document.getElementById("posctra");
        var items = ul.getElementsByTagName("li");
    
        for (var i = 0; i < items.length; ++i) {
    	    var nume = $(items[i]).text();
    	    stringToSend = stringToSend + nume + "#";
        }


        var ul = document.getElementById("negctra");
        var items = ul.getElementsByTagName("li");
    
        for (var i = 0; i < items.length; ++i) {
    	    var nume = $(items[i]).text();
    	    stringToSend = stringToSend + nume + "#";
        }
        alert(stringToSend);
        $("#hiddenctra").val(stringToSend);
        alert($("#hiddenctra").val());
      
        if(everythingIsOkay){
            document.forms["formctra"].submit(); 
        }
    });
});


$("#dropdown-posetra li").click(function () {

   var name = $(this).text();
   var string = '<li class="plan-feature">' + name + '<button class="remove-e btn btn-default btn-xs pull-right"><span class="glyphicon glyphicon-remove"></span></button>' + "</li>";
    $("#posetra").append(string);
    $(this).remove();

    var ul = document.getElementById("dropdown-negetra");
    var items = ul.getElementsByTagName("li");
    
    for (var i = 0; i < items.length; ++i) {
    	var nume = $(items[i]).text();
    	var n = name.localeCompare(nume);
    	if(n==0)
    	{
    		$(items[i]).remove();
    	}
    }
    
 });

$("#dropdown-negetra li").click(function () {

   var name = $(this).text();
   
   var string = '<li class="plan-feature">' + name + '<button class="remove-e btn btn-default btn-xs pull-right"><span class="glyphicon glyphicon-remove"></span></button>' + "</li>";
    $("#negetra").append(string);
    
    $(this).remove();

    var ul = document.getElementById("dropdown-posetra");
    var items = ul.getElementsByTagName("li");
    
    for (var i = 0; i < items.length; ++i) {
    	var nume = $(items[i]).text();
    	var n = name.localeCompare(nume);
    	if(n==0)
    	{
    		$(items[i]).remove();
    	}
    }
    
 });

$('.toElec').on('click','.remove-e',function(){
	$(this).parent().remove();
	var name = '<li><a>' + $(this).parent().text() + '</li></a>';
	$("#dropdown-negetra").append(name);
	$("#dropdown-posetra").append(name);

});

$('.testpose').on('click','#dropdown-posetra li',function(){
	var name = $(this).text();
   var string = '<li class="plan-feature">' + name + '<button class="remove-e btn btn-default btn-xs pull-right"><span class="glyphicon glyphicon-remove"></span></button>' + "</li>";
   
    $("#posetra").append(string);
    $(this).remove();

    var ul = document.getElementById("dropdown-negetra");
    var items = ul.getElementsByTagName("li");
    
    for (var i = 0; i < items.length; ++i) {
    	var nume = $(items[i]).text();
    	var n = name.localeCompare(nume);
    	if(n==0)
    	{
    		$(items[i]).remove();
    	}
    }
});

$('.testnege').on('click','#dropdown-negetra li',function(){

	var name = $(this).text();
   var string = '<li class="plan-feature">' + name + '<button class="remove-e btn btn-default btn-xs pull-right"><span class="glyphicon glyphicon-remove"></span></button>' + "</li>";
    $("#negetra").append(string);
    $(this).remove();

    var ul = document.getElementById("dropdown-posetra");
    var items = ul.getElementsByTagName("li");
    
    for (var i = 0; i < items.length; ++i) {
    	var nume = $(items[i]).text();
    	var n = name.localeCompare(nume);
    	if(n==0)
    	{
    		$(items[i]).remove();
    	}
    }
	
});



$("#submitetra").click(function () {

	$("#formetra").submit(function(event) { 
        event.preventDefault(); 
        var everythingIsOkay = true;
      	
      	var stringToSend= "";
        var ul = document.getElementById("posetra");
        var items = ul.getElementsByTagName("li");
    
        for (var i = 0; i < items.length; ++i) {
    	    var nume = $(items[i]).text();
    	    stringToSend = stringToSend + nume + "#";
        }


        var ul = document.getElementById("negetra");
        var items = ul.getElementsByTagName("li");
    
        for (var i = 0; i < items.length; ++i) {
    	    var nume = $(items[i]).text();
    	    stringToSend = stringToSend + nume + "#";
        }
        alert(stringToSend);
        $("#hiddenetra").val(stringToSend);
        alert($("#hiddenetra").val());
      
        if(everythingIsOkay){
            document.forms["formetra"].submit(); 
        }
    });
});

$("#dropdown-poshtra li").click(function () {

   var name = $(this).text();
   var string = '<li class="plan-feature">' + name + '<button class="remove-h btn btn-default btn-xs pull-right"><span class="glyphicon glyphicon-remove"></span></button>' + "</li>";
    $("#poshtra").append(string);
    $(this).remove();

    var ul = document.getElementById("dropdown-neghtra");
    var items = ul.getElementsByTagName("li");
    
    for (var i = 0; i < items.length; ++i) {
    	var nume = $(items[i]).text();
    	var n = name.localeCompare(nume);
    	if(n==0)
    	{
    		$(items[i]).remove();
    	}
    }
    
 });

$("#dropdown-neghtra li").click(function () {

   var name = $(this).text();
   
   var string = '<li class="plan-feature">' + name + '<button class="remove-h btn btn-default btn-xs pull-right"><span class="glyphicon glyphicon-remove"></span></button>' + "</li>";
    $("#neghtra").append(string);
    
    $(this).remove();

    var ul = document.getElementById("dropdown-poshtra");
    var items = ul.getElementsByTagName("li");
    
    for (var i = 0; i < items.length; ++i) {
    	var nume = $(items[i]).text();
    	var n = name.localeCompare(nume);
    	if(n==0)
    	{
    		$(items[i]).remove();
    	}
    }
    
 });

$('.toHome').on('click','.remove-h',function(){
	$(this).parent().remove();
	var name = '<li><a>' + $(this).parent().text() + '</li></a>';
	$("#dropdown-neghtra").append(name);
	$("#dropdown-poshtra").append(name);

});

$('.testposh').on('click','#dropdown-poshtra li',function(){
	var name = $(this).text();
   var string = '<li class="plan-feature">' + name + '<button class="remove-h btn btn-default btn-xs pull-right"><span class="glyphicon glyphicon-remove"></span></button>' + "</li>";
   
    $("#poshtra").append(string);
    $(this).remove();

    var ul = document.getElementById("dropdown-neghtra");
    var items = ul.getElementsByTagName("li");
    
    for (var i = 0; i < items.length; ++i) {
    	var nume = $(items[i]).text();
    	var n = name.localeCompare(nume);
    	if(n==0)
    	{
    		$(items[i]).remove();
    	}
    }
});

$('.testnegh').on('click','#dropdown-neghtra li',function(){

	var name = $(this).text();
   var string = '<li class="plan-feature">' + name + '<button class="remove-h btn btn-default btn-xs pull-right"><span class="glyphicon glyphicon-remove"></span></button>' + "</li>";
    $("#neghtra").append(string);
    $(this).remove();

    var ul = document.getElementById("dropdown-poshtra");
    var items = ul.getElementsByTagName("li");
    
    for (var i = 0; i < items.length; ++i) {
    	var nume = $(items[i]).text();
    	var n = name.localeCompare(nume);
    	if(n==0)
    	{
    		$(items[i]).remove();
    	}
    }
	
});



$("#submithtra").click(function () {

	$("#formhtra").submit(function(event) { 
        event.preventDefault(); 
        var everythingIsOkay = true;
      	
      	var stringToSend= "";
        var ul = document.getElementById("poshtra");
        var items = ul.getElementsByTagName("li");
    
        for (var i = 0; i < items.length; ++i) {
    	    var nume = $(items[i]).text();
    	    stringToSend = stringToSend + nume + "#";
        }


        var ul = document.getElementById("neghtra");
        var items = ul.getElementsByTagName("li");
    
        for (var i = 0; i < items.length; ++i) {
    	    var nume = $(items[i]).text();
    	    stringToSend = stringToSend + nume + "#";
        }
        alert(stringToSend);
        $("#hiddenhtra").val(stringToSend);
        alert($("#hiddenhtra").val());
      
        if(everythingIsOkay){
            document.forms["formhtra"].submit(); 
        }
    });
});


$("#dropdown-prtrait li").click(function () {
    var name = $(this).text();
    
    $("#hiddenProdTrait").val(name);
    $("#prtrait").text(name);
    //alert($("#hiddenFilter").val())
    document.forms["productForm"].submit();
    
 });

$("#dropdown-compide li").click(function () {
    var name = $(this).text();
    
    $("#hiddenCompIde").val(name);
    $("#compide").text(name);
    //alert($("#hiddenCompIde").val())
    document.forms["companyForm"].submit();
    
 });


$("#dropdown-company1 li").click(function () {
    var name = $(this).text();
    
    $("#hiddenCategoryCompany").val(name);
    $("#menu1company").text(name);
    //alert($("#hiddenCategoryCompany").val())
    
 });

$("#dropdown-company2 li").click(function () {
    var name = $(this).text();
    
    $("#hiddenFilterCompany").val(name);
    $("#menu2company").text(name);
    //alert($("#hiddenFilter").val())
    
 });
